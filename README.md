Portfolio
=========

Welcome to my portoflio page!

This repository contains all the work I conducted while an undergrauate at university. 

First year
----------
### Huffman code 
This Huffman coding project was written in C#.
### Pyhton code
A small collection of programming projects written in Python.
### Containers
A collection of containers (Array, Linked List, etc) written in C#.
### Unity Game
A game developed in Unity called Ted vs Zombies written in C#.
### Dungeon Quest
A text adventure game written in Pyhton. 
Not available for viewing. Download available upon request. 
### Geometry Blaster
A game developed in the Unreal Engine using C++ and Blueprint scripting, based on the popular Geometry Wars series of games. 
Not available for viewing. Download available upon request. 

Second year
-----------
### Networking Application
A networked chat application written in C++.
### 3D Rendering Application 
Features a collection of 3D rendering samples written in C++ and GLSL.
### Game Engine
A 2D game engine written in C++.
### Spaceship Wars
A 2D game featuring AI techniques written in C++.
### Artificial Intelligence 
A collection of AI projects and algorithms written in C++.
### Racing Bot
An AI racing bot developed in Torcs using a Behavior Tree, written in C++.
### Guardians
An Unreal Engine project featuring a third-party plugin that can read an RFID tag, written in C++.
Not available for viewing. Download available upon request. 

Third year
----------
### Football Champions
A multiplayer football game made in the Unreal Engine using C++.
### Project ReBorn
A Resident Evil clone made in the Unreal Engine using C++.
### ECS Project
A Entity Component System or ECS developed for use wihtin Unreal Engine 5. 
Not available for viewing. Download available upon request. 