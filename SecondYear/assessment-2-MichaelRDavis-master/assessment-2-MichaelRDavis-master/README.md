# Networking Application
## Bibliography
* Glazer, Madhav, J.G. S.M., (2016) *Multiplayer Game Programming*. United States of America. Addison-Wesley
* Glazer, Madhav, J.G. S.M., (2017) *Multiplayer Book*. Available at: [Link](https://github.com/MultiplayerBook/MultiplayerBook)
