#include "SocketAddress.h"
#include "Memory.h"

SocketAddress::SocketAddress()
{
	GetAsSocketAddress()->sin_family = AF_INET;
	GetAsSocketAddress()->sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	GetAsSocketAddress()->sin_port = htons(0);
}

SocketAddress::SocketAddress(uint32_t address, uint16_t port)
{
	GetAsSocketAddress()->sin_family = AF_INET;
	GetAsSocketAddress()->sin_addr.S_un.S_addr = htonl(address);
	GetAsSocketAddress()->sin_port = htons(port);
}

SocketAddress::SocketAddress(const sockaddr& sockAddr)
{
	Memory::Memcpy(&m_socketAddress, &sockAddr, sizeof(sockaddr));
}
