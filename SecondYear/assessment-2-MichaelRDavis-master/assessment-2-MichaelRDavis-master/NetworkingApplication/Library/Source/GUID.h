#pragma once

#include <random>
#include <sstream>

namespace Net
{
	/**
	 * A class for generating a Globally Unique Identifier 
	 * Wrapped within the Net namespace to avoid conflicts with the Windows GUID object
	 */
	class GUID
	{
	public:
		/** Generate a randomized char */
		inline unsigned char GetRandomChar()
		{
			std::random_device device;
			std::mt19937 seed(device());
			std::uniform_int_distribution<> dis(0, 255);
			return static_cast<unsigned char>(dis(seed));
		}

		/** Generates a GUID has a string */
		inline std::string GenerateGUID(const uint32_t length)
		{
			std::stringstream stream;

			for (uint32_t i = 0; i < length; i++)
			{
				auto randChar = GetRandomChar();

				std::stringstream hexStream;
				hexStream << std::hex << static_cast<int32_t>(randChar);

				auto hex = hexStream.str();
				stream << (hex.length() < 2 ? '0' + hex : hex);
			}

			return stream.str();
		}
	};
}