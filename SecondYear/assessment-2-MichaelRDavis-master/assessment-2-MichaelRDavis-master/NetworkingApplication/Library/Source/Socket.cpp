#include "Socket.h"
#include "SocketAddress.h"
#include <iostream>
#include <cassert>

Socket::Socket(ESocketType type, SOCKET socket)
	: m_socketType(type)
	, m_socket(socket)
{

}

Socket::~Socket()
{
	closesocket(m_socket);
}

int32_t Socket::Connect(const SocketAddress& address)
{
	int32_t error = connect(m_socket, &address.m_socketAddress, address.GetSize());
	if (error < 0)
	{
		std::cerr << "Socket Connect: " << WSAGetLastError() << std::endl;
	}

	return NO_ERROR;
}

int32_t Socket::Bind(const SocketAddress& address)
{
	int32_t error = bind(m_socket, &address.m_socketAddress, address.GetSize());
	if (error != INVALID_SOCKET)
	{
		std::cerr << "Socket Bind: " << WSAGetLastError() << std::endl;
	}

	return NO_ERROR;
}

int32_t Socket::Listen(int32_t log)
{
	int32_t error = listen(m_socket, log);
	if (error < 0)
	{
		std::cerr << "Socket Listen: " << WSAGetLastError() << std::endl;
	}

	return NO_ERROR;
}

std::shared_ptr<Socket> Socket::Accept(SocketAddress& address)
{
	int32_t length = address.GetSize();
	SOCKET newSocket = accept(m_socket, &address.m_socketAddress, &length);
	if (newSocket != INVALID_SOCKET)
	{
		return std::make_shared<Socket>(m_socketType, newSocket);
	}
	else
	{
		std::cerr << "Socket Accept: " << WSAGetLastError() << std::endl;
		return nullptr;
	}
}

int32_t Socket::Send(const void* data, size_t length)
{
	int32_t bytesSentCount = send(m_socket, static_cast<const char*>(data), length, 0);
	if (bytesSentCount < 0)
	{
		std::cerr << "Socket Send: " << WSAGetLastError() << std::endl;
	}

	return bytesSentCount;
}

int32_t Socket::Receive(void* buffer, size_t length)
{
	int32_t bytesReceivedCount = recv(m_socket, static_cast<char*>(buffer), length, 0);
	if (bytesReceivedCount < 0)
	{
		std::cerr << "Socket Receive: " << WSAGetLastError() << std::endl;
	}

	return bytesReceivedCount;
}

int32_t Socket::SendTo(const void* data, int32_t length, const SocketAddress& to)
{
	assert(m_socketType == ESocketType::EStreaming && "Not available on streaming sockets");

	int32_t bytesSentCount = sendto(m_socket, static_cast<const char*>(data), length, 0, &to.m_socketAddress, to.GetSize());
	if (bytesSentCount >= 0)
	{
		return bytesSentCount;
	}

	std::cerr << "Socket SendTo: " << WSAGetLastError() << std::endl;
	return WSAGetLastError();
}

int32_t Socket::ReceiveFrom(void* buffer, int32_t length, SocketAddress& from)
{
	assert(m_socketType == ESocketType::EStreaming && "Not available on streaming sockets");

	int32_t fromLength = from.GetSize();
	int32_t readByteCount = recvfrom(m_socket, static_cast<char*>(buffer), length, 0, &from.m_socketAddress, &length);
	if (readByteCount >= 0)
	{
		return readByteCount;
	}

	std::cerr << "Socket RecieveFrom: " << WSAGetLastError() << std::endl;
	return WSAGetLastError();
}

void Socket::SetSocketType(ESocketType newSocketType)
{
	m_socketType = newSocketType;
}
