#pragma once

#include <memory>
#include <string>
#include <WinSock2.h>

class Socket;
class SocketAddress;

enum SocketFamily
{
	INET = AF_INET,
	INET16 = AF_INET6
};

class SocketLbrary
{
public:
	static std::shared_ptr<Socket> CreateTCPSocket(SocketFamily family);
	static std::shared_ptr<SocketAddress> CreateIPV4FromString(const std::string& string);
};