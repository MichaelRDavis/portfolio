#pragma once

class Socket;

#include <cstdint>
#include <memory>
#include <string>

 class Message;
 class MemoryWriter;
 class MemoryReader;

class User
{
public:
	User(uint64_t userID);
	User() = default;
	~User() = default;

	void Write(MemoryWriter& stream);
	void Read(MemoryReader& stream);

	void StoreUserName();
	void GenerateUserID();
	void LogIn();
	void LogOff();

	void SetUserID(const uint64_t& newID);
	void SetUserName(const std::string& newName);
	void SetIsLoggedIn(bool newLogin);

	inline uint64_t GetUserID() const
	{
		return m_userID;
	}

	inline std::string GetUserName() const
	{
		return m_userName;
	}

	inline bool GetIsLoggedIn() const
	{
		return m_isLoggedIn;
	}

private:
	std::shared_ptr<Message> m_message;
	uint64_t m_userID;
	std::string m_userName;
	bool m_isLoggedIn;
};