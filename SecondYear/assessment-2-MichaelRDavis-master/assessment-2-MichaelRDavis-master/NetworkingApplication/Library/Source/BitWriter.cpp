#include "BitWriter.h"
#include "Memory.h"
#include <algorithm>

BitWriter::BitWriter()
{
	m_buffer = nullptr;
	m_head = 0;
	m_capacity = 0;
	ReallocateBuffer(256);
}

BitWriter::~BitWriter()
{
	Memory::Free(m_buffer);
}

void BitWriter::WriteBits(uint8_t data, size_t bitCount)
{
	uint32_t nextHead = m_head + static_cast<uint32_t>(bitCount);
	if (nextHead > m_capacity)
	{
		ReallocateBuffer(std::max(m_capacity * 2, nextHead));
	}

	uint32_t byteOffset = m_head >> 3;
	uint32_t bitOffset = m_head & 0x7;

	uint8_t currentMask = ~(0xff << bitOffset);
	m_buffer[byteOffset] = (m_buffer[byteOffset] & currentMask) | (data << bitOffset);

	uint32_t freeBits = 8 - bitOffset;

	if (freeBits < bitCount)
	{
		m_buffer[byteOffset + 1] = data >> freeBits;
	}

	m_head = nextHead;
}

void BitWriter::WriteBits(const void* data, size_t bitCount)
{
	const char* byte = static_cast<const char*>(data);
	while (bitCount)
	{
		WriteBits(*byte, 8);
		++byte;
		bitCount -= 8;
	}
}

void BitWriter::WriteBytes(const void* data, size_t byteCount)
{
	WriteBits(data, byteCount << 3);
}

void BitWriter::ReallocateBuffer(uint32_t newBitCapacity)
{

}
