#include "MemoryWriter.h"
#include "Memory.h"
#include <algorithm>

MemoryWriter::MemoryWriter()
	: m_writeBuffer(nullptr)
	, m_head(0)
	, m_capcity(0)
{
	ReallocateBuffer(32);
}

MemoryWriter::~MemoryWriter()
{
	Memory::Free(m_writeBuffer);
}

void MemoryWriter::Write(const void* data, size_t byteCount)
{
	uint32_t head = m_head + static_cast<uint32_t>(byteCount);
	if (head > m_capcity)
	{
		ReallocateBuffer(std::max(m_capcity * 2, head));
	}

	Memory::Memcpy(m_writeBuffer + m_head, data, byteCount);
	m_head = head;
}

void MemoryWriter::ReallocateBuffer(uint32_t newLength)
{
	m_writeBuffer = static_cast<char*>(Memory::Realloc(m_writeBuffer, newLength));
	m_capcity = newLength;
}

