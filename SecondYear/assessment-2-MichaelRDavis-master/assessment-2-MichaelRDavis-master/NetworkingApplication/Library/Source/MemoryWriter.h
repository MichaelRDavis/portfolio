#pragma once

#include <cstdint>

/**
 * A Memory Writer for writing data to a memory buffer
 */
class MemoryWriter
{
public:
	/** MemoryWriter constructor */
	MemoryWriter();

	/** MemoryWriter destructor */
	~MemoryWriter();

	/**  */
	void Write(const void* data, size_t byteCount);

	/**  */
	template<typename T>
	void Write(T data)
	{
		Write(&data, sizeof(data));
	}

	/**  */
	inline const char* GetWriteBuffer() const { return m_writeBuffer; }

	/**  */
	inline uint32_t GetLength() const { return m_head; }

private:
	/**  */
	void ReallocateBuffer(uint32_t newLength);

private:
	/**  */
	char* m_writeBuffer;

	/**  */
	uint32_t m_head;

	/**  */
	uint32_t m_capcity;
};