#include "MemoryReader.h"
#include "Memory.h"

MemoryReader::MemoryReader(char* buffer, uint32_t byteCount)
	: m_readBuffer(buffer)
	, m_capacity(byteCount)
	, m_head()
{
}

MemoryReader::~MemoryReader()
{
	Memory::Free(m_readBuffer);
}

void MemoryReader::Read(void* data, uint32_t byteCount)
{
	uint32_t head = m_head + byteCount;
	if (head > m_capacity)
	{

	}

	Memory::Memcpy(data, m_readBuffer + m_head, byteCount);
	m_head = head;
}
