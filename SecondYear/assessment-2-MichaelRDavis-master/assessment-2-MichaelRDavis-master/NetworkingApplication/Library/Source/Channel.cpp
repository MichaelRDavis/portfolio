#include "Channel.h"
#include "User.h"
#include "MemoryReader.h"
#include "MemoryWriter.h"

Channel::Channel()
{

}

Channel::~Channel()
{

}

void Channel::GenerateChannelID()
{

}

void Channel::Read(MemoryWriter stream)
{
	stream.Write(m_channelID);
	stream.Write(m_channelName);
}

void Channel::Write(MemoryReader stream)
{
	stream.Read(m_channelID);
	stream.Read(m_channelName);
}

void Channel::AddUser(std::shared_ptr<User> user)
{
	if (m_users.size() < MAX_USERS)
	{
		m_users.push_back(user);
	}
}

void Channel::RemoveUser(std::shared_ptr<User> user)
{
	m_users.remove(user);
}

void Channel::SendChannelMessage(std::string& message, std::shared_ptr<Socket> socket)
{
	for (auto user : m_users)
	{
		
	}
}

void Channel::SetChannelID(uint64_t newID)
{
	m_channelID = newID;
}

void Channel::SetChannelName(std::string& newName)
{
	m_channelName = newName;
}
