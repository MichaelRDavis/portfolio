#pragma once

#include <cstdint>

/**
 * A Memory Reader for reading data from a memory buffer
 */
class MemoryReader
{
public:
	/** Default MemoryReader constructor */
	MemoryReader() = default;

	/** Memory reader constructor with parameters  */
	MemoryReader(char* buffer, uint32_t byteCount);

	/** Default MemoryReader destructor */
	~MemoryReader();

	void Read(void* data, uint32_t byteCount);

	template<typename T>
	void Read(T& data)
	{
		Read(&data, sizeof(data));
	}

	inline uint32_t GetDataSize() const
	{
		return m_capacity - m_head;
	}

private:
	char* m_readBuffer;
	uint32_t m_head;
	uint32_t m_capacity;
};