#pragma once

#include <cstdint>

/**
 * A BitWriter for serializing data into a series of bits
 */
class BitWriter
{
public:
	/** Default BitWriter constructor */
	BitWriter();

	/** Default BitWriter destructor */
	~BitWriter();

	template<typename T>
	void Write(T data, size_t bitCount = sizeof(T) * 8)
	{

	}

	void WriteBits(uint8_t data, size_t bitCount);
	void WriteBits(const void* data, size_t bitCount);

	void WriteBytes(const void* data, size_t byteCount);

	/** Returns the buffer */
	inline const char* GetBuffer() const { return m_buffer; }

	/** Returns the bit length */
	inline uint32_t GetBitLength() const { return m_head; }

	/** Returns the byte length */
	inline uint32_t GetByteLength() const { return m_head + 7 >> 3; }

private:
	void ReallocateBuffer(uint32_t newBitCapacity);

private:
	/** Buffer to write to */
	char* m_buffer;

	/** Buffer head */
	uint32_t m_head;

	/** Capacity of the buffer */
	uint32_t m_capacity;
};