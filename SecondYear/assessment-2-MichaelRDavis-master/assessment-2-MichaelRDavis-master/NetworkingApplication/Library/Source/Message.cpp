#include "Message.h"
#include "MemoryWriter.h"
#include "MemoryReader.h"

void Message::Write(MemoryWriter& stream)
{
	stream.Write(m_msg);
	stream.Write(m_timeStamp);
	stream.Write(m_msgID);
	stream.Write(m_userID);
}

void Message::Read(MemoryReader& stream)
{
	stream.Read(m_msg);
	stream.Read(m_timeStamp);
	stream.Read(m_msgID);
	stream.Read(m_userID);
}

void Message::GenerateMessageTimeStamp()
{

}