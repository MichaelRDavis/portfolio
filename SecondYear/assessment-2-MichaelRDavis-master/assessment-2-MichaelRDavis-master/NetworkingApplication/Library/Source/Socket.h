#pragma once

#include <WinSock2.h>
#include <cstdint>
#include <memory>

class SocketAddress;

/**
 * Indicates the socket type
 */
enum class ESocketType
{
	EStreaming, // TCP socket type
	EDatagram,	// UDP socket type
	EUnknown	// Unknown socket type
};

/**
 * Wrapper class for a network socket
 */
class Socket
{
public:
	/** Default Socket constructor */
	Socket() = default;

	/** Socket constructor with parameters */
	Socket(ESocketType type, SOCKET socket);

	/** Socket destructor */
	~Socket();

	/** Connect to a socket via its address */
	int32_t Connect(const SocketAddress& address);

	/** Bind socket to a socket address */
	int32_t Bind(const SocketAddress& address);

	/** Listens for an incoming connection */
	int32_t Listen(int32_t log = 0);

	/** Accept an incoming connection */
	std::shared_ptr<Socket> Accept(SocketAddress& address);

	/** Send data over a connection */
	int32_t Send(const void* data, size_t length);

	/** Receive data from a connection */
	int32_t Receive(void* buffer, size_t length);

	/** Send data to a connected socket, datagram socket use only */
	int32_t SendTo(const void* data, int32_t length, const SocketAddress& to);

	/** Receive data from a connected socket, datagram socket use only */
	int32_t ReceiveFrom(void* buffer, int32_t length, SocketAddress& from);

	/** Sets the socket type 
	*	@param newSocketType - Changes socket type, maybe unsafe use with caution	
	*/
	void SetSocketType(ESocketType newSocketType);

	/** Returns the the socket type */
	inline ESocketType GetSocketType() const { return m_socketType; }

private:
	friend class SocketLibrary;

	/** Berkeley socket */
	SOCKET m_socket;

	/** The type of socket */
	ESocketType m_socketType;
};