#pragma once

#include <list>
#include <memory>
#include <string>

class User;
class Socket;
class MemoryWriter;
class MemoryReader;

constexpr uint64_t MAX_USERS{ 50 };

class Channel
{
public:
	Channel();
	~Channel();

	void GenerateChannelID();

	void Read(MemoryWriter stream);
	void Write(MemoryReader stream);

	void AddUser(std::shared_ptr<User> user);
	void RemoveUser(std::shared_ptr<User> user);
	void SendChannelMessage(std::string& message, std::shared_ptr<Socket> socket);
	
	void SetChannelID(uint64_t newID);
	void SetChannelName(std::string& newName);

	inline uint64_t GetChannelID() const { return m_channelID; }
	inline std::string GetChannelName() const { return m_channelName; }
	inline uint32_t GetNumUsers() const { return m_users.size(); }

private:
	uint64_t m_channelID;
	std::string m_channelName;
	std::list<std::shared_ptr<User>> m_users;
};