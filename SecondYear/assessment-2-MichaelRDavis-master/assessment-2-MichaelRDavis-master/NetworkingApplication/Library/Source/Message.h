#pragma once

#include <cstdint>
#include <string>

class MemoryWriter;
class MemoryReader;

class Message
{
public:
	Message() = default;
	~Message() = default;

	void Write(MemoryWriter& stream);
	void Read(MemoryReader& stream);

	void GenerateMessageTimeStamp();

	inline std::string GetMessage() const { return m_msg; }

private:
	/** Actual user message */
	std::string m_msg;

	/** Time this message was sent, appended to the beginning of the message. */
	std::string m_timeStamp;

	/** Unique ID of the message */
	uint64_t m_msgID;

	/** User ID that sent this message */
	uint64_t m_userID;

	/** Recipient user ID for direct messages */
	uint64_t m_recipientID;
};