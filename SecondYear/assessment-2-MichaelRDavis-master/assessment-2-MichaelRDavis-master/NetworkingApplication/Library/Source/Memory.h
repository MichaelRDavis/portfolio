#pragma once

#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <string>

/**
 * C-style memory allocation wrapper
 */
class Memory
{
public:
	inline static void Free(void* block)
	{
		free(block);
	}

	inline static void* Malloc(size_t size)
	{
		return malloc(size);
	}

	inline static int32_t Memcmp(const void* buffer1, const void* buffer2, size_t size)
	{
		return memcmp(buffer1, buffer2, size);
	}

	inline static void* Memset(void* destination, int32_t value, size_t size)
	{
		return memset(destination, value, size);
	}

	inline static void* Memcpy(void* destination, const void* source, size_t size)
	{
		return memcpy(destination, source, size);
	}

	inline static void* Realloc(void* block, size_t size)
	{
		return realloc(block, size);
	}
};