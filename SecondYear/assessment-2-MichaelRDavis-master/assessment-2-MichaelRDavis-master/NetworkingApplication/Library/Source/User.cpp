#include "User.h"
#include "GUID.h"
#include "MemoryWriter.h"
#include "MemoryReader.h"
#include "Socket.h"
#include "Memory.h"
#include <iostream>

User::User(uint64_t userID)
	: m_userID(userID)
	, m_isLoggedIn(false)
{

}

void User::Write(MemoryWriter& stream)
{
	stream.Write(m_userID);
	stream.Write(m_userName);
	stream.Write(m_isLoggedIn);
}

void User::Read(MemoryReader& stream)
{
	stream.Read(m_userID);
	stream.Read(m_userName);
	stream.Read(m_isLoggedIn);
}

void User::StoreUserName()
{
	std::string input;
	std::cout << "Insert your desired user name" << std::endl;
	std::cin >> input;
	SetUserName(input);
}

void User::GenerateUserID()
{
	Net::GUID guid;
	std::string stringID = guid.GenerateGUID(24);
	int32_t id = std::stoi(stringID);
	SetUserID(id);
}

void User::LogIn()
{
	SetIsLoggedIn(true);
}

void User::LogOff()
{
	SetIsLoggedIn(false);
}

void User::SetUserID(const uint64_t& newID)
{
	m_userID = newID;
}

void User::SetUserName(const std::string& newName)
{
	m_userName = newName;
}

void User::SetIsLoggedIn(bool newLogin)
{
	m_isLoggedIn = newLogin;
}
