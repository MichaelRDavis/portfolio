#pragma once

#include <Windows.h>
#include <cstdint>

class SocketAddress
{
public:
	SocketAddress();
	SocketAddress(uint32_t address, uint16_t port);
	SocketAddress(const sockaddr& sockAddr);
	~SocketAddress() = default;

	inline size_t GetSize() const
	{
		return sizeof(sockaddr);
	}

private:
	friend class Socket;

	sockaddr m_socketAddress;

	inline sockaddr_in* GetAsSocketAddress()
	{
		return reinterpret_cast<sockaddr_in*>(&m_socketAddress);
	}
};