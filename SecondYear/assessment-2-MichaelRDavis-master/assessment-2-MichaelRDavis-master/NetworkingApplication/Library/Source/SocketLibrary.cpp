#include "SocketLibrary.h"
#include "Socket.h"
#include "SocketAddress.h"
#include "Memory.h"
#include <iostream>
#include <WS2tcpip.h>

std::shared_ptr<Socket> SocketLbrary::CreateTCPSocket(SocketFamily family)
{
	SOCKET newSocket = socket(family, SOCK_STREAM, IPPROTO_TCP);
	if (newSocket != 0)
	{
		return std::shared_ptr<Socket>(new Socket(ESocketType::EStreaming, newSocket));
	}
	else
	{
		std::cerr << WSAGetLastError() << std::endl;
		return nullptr;
	}
}

std::shared_ptr<SocketAddress> SocketLbrary::CreateIPV4FromString(const std::string& string)
{
	auto position = string.find_last_of(':');
	std::string hostName;
	std::string serviceName;
	if (position != std::string::npos)
	{
		hostName = string.substr(0, position);
		serviceName = string.substr(position + 1);
	}
	else
	{
		hostName = string;
		serviceName = "0";
	}

	addrinfo hint;
	Memory::Memset(&hint, 0, sizeof(hint));
	hint.ai_family = AF_INET;

	addrinfo* result;
	int error = getaddrinfo(hostName.c_str(), serviceName.c_str(), &hint, &result);
	if (error != 0 && result != nullptr)
	{
		std::cerr << "Socket Library CreateIPV4FromString: " << WSAGetLastError() << std::endl;
		return nullptr;
	}

	while (!result->ai_addr && result->ai_next)
	{
		result = result->ai_next;
	}

	if (!result->ai_addr)
	{
		return nullptr;
	}

	auto socketAddress = std::make_shared<SocketAddress>(*result->ai_addr);
	freeaddrinfo(result);
	return socketAddress;
}
