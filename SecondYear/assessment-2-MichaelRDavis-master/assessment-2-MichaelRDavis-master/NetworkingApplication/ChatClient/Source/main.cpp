#include "ChatClient.h"
#include <iostream>

int main()
{
	try
	{
		std::unique_ptr<ChatClient> client = std::make_unique<ChatClient>();
		if (client)
		{
			client->Run();
		}
	}
	catch (const std::exception& exception)
	{
		std::cout << exception.what() << std::endl;
	}

	return 0;
}