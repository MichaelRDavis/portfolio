#pragma once

#include <Library.h>
#include <mutex>
#include <queue>

/**
 * Chat client manager
 */
class ChatClient
{
public:
	/** ChatClient constructor */
	ChatClient();

	/** ChatClient destructor */
	~ChatClient();

	/** Initializes the user input thread */
	void InitUserInput();

	/** Destroys the user input thread */
	void DestroyUserInput();

	/** Get user input from input thread */
	void GetUserInput();

	/** Initializes winsock functions */
	void InitWinsock();

	/** Run chat client */
	void Run();

	/** Poll for user input */
	void PollUserInput();

	/** Poll for incoming messages from the server */
	void PollMessages(const char* str);

	/** Sends a message to a socket */
	void SendMessageToSocket(std::shared_ptr<Socket> socket, const char* str);

private:
	/** Network socket for the client */
	std::shared_ptr<Socket> m_serverSocket;

	/** Address of of the client socket */
	std::shared_ptr<SocketAddress> m_serverAddress;

	/** User that initiated the client application */
	std::unique_ptr<User> m_user;

	/** Windows socket implementation */
	WSADATA m_wsaData;

	/** Thread for user input */
	std::thread* m_userInputThread;

	/** Mutex for non-blocking user input */
	std::mutex m_userInputQueue;

	/** Queue for user input */
	std::queue<std::string> m_queueUserInput;

	/** Flag if client application is shutdown */
	bool m_quit;
};