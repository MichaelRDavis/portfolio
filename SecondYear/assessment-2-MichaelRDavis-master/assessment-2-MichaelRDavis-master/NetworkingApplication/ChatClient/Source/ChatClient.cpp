#include "ChatClient.h"
#include "MemoryWriter.h"
#include <iostream>

ChatClient::ChatClient()
{
	InitWinsock();
	m_serverSocket = SocketLbrary::CreateTCPSocket(INET);
	m_serverAddress = SocketLbrary::CreateIPV4FromString("127.0.0.1");
	m_quit = false;
}

ChatClient::~ChatClient()
{

}

void ChatClient::InitUserInput()
{

}

void ChatClient::DestroyUserInput()
{

}

void ChatClient::GetUserInput()
{

}

void ChatClient::InitWinsock()
{
	int32_t reuslt = WSAStartup(MAKEWORD(2, 2), &m_wsaData);
	if (reuslt != 0)
	{
		std::cout << "WSAStartup failed: " << reuslt << std::endl;
	}
}

void ChatClient::Run()
{
	while (!m_quit)
	{
		PollUserInput();
	}
}

void ChatClient::PollUserInput()
{
	char buffer[1024];

	std::string msg;
	std::getline(std::cin, msg);
	Memory::Memcpy(buffer, msg.c_str(), msg.length());
	SendMessageToSocket(m_serverSocket, buffer);
	PollMessages(buffer);
}

void ChatClient::PollMessages(const char* str)
{
	m_serverSocket->Receive((void*)str, sizeof(str));
}

void ChatClient::SendMessageToSocket(std::shared_ptr<Socket> socket, const char* str)
{
	socket->Send(str, sizeof(str));
}
