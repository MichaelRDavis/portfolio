#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_ChatGuiClient.h"

class ChatGuiClient : public QMainWindow
{
	Q_OBJECT

public:
	ChatGuiClient(QWidget *parent = Q_NULLPTR);

private:
	Ui::ChatGuiClientClass ui;
};
