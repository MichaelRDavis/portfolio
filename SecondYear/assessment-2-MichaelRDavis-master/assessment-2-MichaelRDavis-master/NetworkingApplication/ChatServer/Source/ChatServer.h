#pragma once

#include <memory>
#include <Library.h>

class Socket;
class SocketAddress;
class Channel;

/**
 * Class responsible for running the chat server application.
 */
class ChatServer
{
public:
	/** ChatServer constructor */
	ChatServer();

	/** ChatServer destructor */
	~ChatServer();

	/** Initializes winsock functions */
	void InitWinsock();

	/** Run chat server */
	void Run();

	/** Poll for incoming messages from clients */
	void PollMessages();

	/** Sends a message to a client */
	void SendMessageToClient(std::shared_ptr<Socket> socket, const char* str);

	/** Sends a messages to all client connected to the server */
	void SendMessageToAllClients(const char* str);

private:
	/** Chat server socket  */
	std::shared_ptr<Socket> m_serverSocket;

	/** Address of the server socket */
	std::unique_ptr<SocketAddress> m_serverAddress;

	/** Address of the client socket */
	std::unique_ptr<SocketAddress> m_clientAddress;

	/** Channel that exits on server */
	std::shared_ptr<Channel> m_channel;

	/** Windows socket implementation */
	WSADATA m_wsaData;

	/** Is chat server currently running */
	bool m_isRunning;
};