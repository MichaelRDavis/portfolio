#include "ChatServer.h"
#include "MemoryReader.h"
#include "Time.h"
#include <iostream>

ChatServer::ChatServer()
{
	InitWinsock();
	m_serverSocket = SocketLbrary::CreateTCPSocket(INET);
	m_serverAddress = std::make_unique<SocketAddress>(INADDR_ANY, 1234);
	m_isRunning = true;
}

ChatServer::~ChatServer()
{

}

void ChatServer::InitWinsock()
{
	int32_t reuslt = WSAStartup(MAKEWORD(2, 2), &m_wsaData);
	if (reuslt != 0)
	{
		std::cout << "WSAStartup failed: " << reuslt << std::endl;
	}
}

void ChatServer::Run()
{
	m_serverSocket->Bind(*m_serverAddress);
	m_serverSocket->Listen(0);
	printf("Server listening to port\n");

	while (m_isRunning)
	{
		PollMessages();
	}
}

void ChatServer::PollMessages()
{
	char buffer[1024];
	Memory::Memset(buffer, 0, sizeof(buffer));

	auto client = m_serverSocket->Accept(*m_clientAddress);
	if (client) 
	{
		printf("Client connected to server\n");
		client->Receive(buffer, sizeof(buffer));
		printf("Client message: %s", buffer);

		SendMessageToClient(client, buffer);
	}
}

void ChatServer::SendMessageToAllClients(const char* str)
{
	
}

void ChatServer::SendMessageToClient(std::shared_ptr<Socket> socket, const char* str)
{
	socket->Send(str, sizeof(str));
}
