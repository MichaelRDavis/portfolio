#include "ChatServer.h"
#include <iostream>

int main()
{
	try
	{
		std::unique_ptr<ChatServer> server = std::make_unique<ChatServer>();
		if (server)
		{
			server->Run();
		}
	}
	catch (const std::exception& exception)
	{

		std::cout << exception.what() << std::endl;
	}

	return 0;
}