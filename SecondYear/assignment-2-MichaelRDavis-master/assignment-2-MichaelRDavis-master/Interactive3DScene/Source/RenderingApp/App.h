#pragma once

#include <SDL.h>
#undef main
#include <glad/glad.h>
#include <cstdio>
#include <string>
#include "Scenes/SceneMenu.h"

class IScene;
class SceneMenu;

class App
{
public:
	App();
	~App();

	void Init();
	void Shutdown();
	void Update(float deltaTime);

	void RenderGUI();

	void HandleMessages();

	void SwapBuffers();
	void Clear();

	template<typename T>
	inline void AddScene(const std::string& sceneName)
	{
		m_menu->AddScene<T>(sceneName);
	}

	inline bool HasQuit() const { return m_hasQuit; }

private:
	SDL_Window* m_pWindow;
	IScene* m_scene;
	SceneMenu* m_menu;
	SDL_GLContext m_pContext;
	int32_t m_windowWidth;
	int32_t m_windowHeight;
	bool m_hasQuit;
	bool m_enableVSYNC;
	bool m_isMenuActive;
};

#ifdef _DEBUG
static void GLAPIENTRY MessageCallback(
	GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam)
{
	fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
		(type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
		type, severity, message);
}
#endif