#include "App.h"
#include "Logging/Log.h"
#include <iostream>
#include <cmath>
#include <imgui.h>
#include <imgui_impl_opengl3.h>
#include <imgui_impl_sdl.h>

App::App()
{
	m_pWindow = nullptr;
	m_scene = nullptr;
	m_menu = new SceneMenu(m_scene);
	m_scene = m_menu;
	m_pContext = nullptr;
	m_windowWidth = 0;
	m_windowHeight = 0;
	m_hasQuit = false;
	m_enableVSYNC = false;
	m_isMenuActive = true;
}

App::~App()
{
	Shutdown();
}

void App::Init()
{
	Log::Init();

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
	}

	SDL_DisplayMode displayMode;
	for (int32_t i = 0; i < SDL_GetNumVideoDisplays(); i++)
	{
		int32_t displayCount = SDL_GetCurrentDisplayMode(i, &displayMode);
		if (displayCount != 0)
		{
			SDL_Log("Could not get display mode for video display #%d: %s", i, SDL_GetError());
		}

		SDL_Log("Display #%d: current display mode is %dx%dpx", i, displayMode.w, displayMode.h);
		m_windowWidth = displayMode.w;
		m_windowHeight = displayMode.h;
	}

	m_pWindow = SDL_CreateWindow("Interactive 3D Scene", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, m_windowWidth, m_windowHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
	if (m_pWindow == nullptr)
	{
		SDL_Log("Could not create window: %s", SDL_GetError());
	}

	SDL_MaximizeWindow(m_pWindow);
	SDL_SetWindowFullscreen(m_pWindow, SDL_FALSE);

	m_pContext = SDL_GL_CreateContext(m_pWindow);
	SDL_GL_MakeCurrent(m_pWindow, m_pContext);
	if (!gladLoadGLLoader((GLADloadproc)SDL_GL_GetProcAddress))
	{
		std::cout << "Failed to initialize the OpenGL context." << std::endl;
	}

	ImGui::CreateContext();
	ImGui_ImplSDL2_InitForOpenGL(m_pWindow, m_pContext);
	ImGui_ImplOpenGL3_Init();
	ImGui::StyleColorsDark();

	std::cout << "OpenGL version loaded: " << glGetString(GL_VERSION) << std::endl;
	std::cout << "GPU Vendor: " << glGetString(GL_VENDOR) << std::endl;
	std::cout << "GPU: " << glGetString(GL_RENDERER) << std::endl;

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

	if (m_enableVSYNC)
	{
		SDL_GL_SetSwapInterval(1);
	}

	glViewport(0, 0, m_windowWidth, m_windowHeight);
	glEnable(GL_DEPTH_TEST);
	glCullFace(GL_FRONT);

#ifdef _DEBUG
	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(MessageCallback, 0);
#endif
}

void App::Shutdown()
{
	delete m_scene;
	if (m_scene != m_menu)
	{
		delete m_menu;
	}

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplSDL2_Shutdown();
	ImGui::DestroyContext();

	SDL_GL_DeleteContext(m_pContext);
	SDL_DestroyWindow(m_pWindow);
}

void App::Update(float deltaTime)
{
	HandleMessages();
	RenderGUI();
}

void App::RenderGUI()
{
	Clear();

	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplSDL2_NewFrame(m_pWindow);
	ImGui::NewFrame();

	if (m_scene)
	{
		m_scene->OnUpdate((float)SDL_GetTicks() / 1000);
		m_scene->OnRender();

		ImGui::Begin("Scenes", &m_isMenuActive, ImGuiWindowFlags_MenuBar);
		if (m_scene != m_menu && ImGui::Button("Back"))
		{
			delete m_scene;
			m_scene = m_menu;
		}

		m_scene->OnGUIRender();
		ImGui::End();
	}

	ImGui::Render();
	SDL_GL_MakeCurrent(m_pWindow, m_pContext);
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

	SwapBuffers();
}

void App::HandleMessages()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
		case SDL_QUIT:
			m_hasQuit = true;
			Shutdown();
		}
	}
}

void App::SwapBuffers()
{
	SDL_GL_SwapWindow(m_pWindow);
}

void App::Clear()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}
