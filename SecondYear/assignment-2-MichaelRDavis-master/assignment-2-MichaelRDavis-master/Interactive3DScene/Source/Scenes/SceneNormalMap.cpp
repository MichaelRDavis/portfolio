#include "SceneNormalMap.h"
#include <glad/glad.h>
#include "Primitives/Plane.h"
#include "Shaders/Shader.h"
#include "Resources/TextureLoader.h"

SceneNormalMap::SceneNormalMap()
{
	m_plane = std::make_unique<Plane>(16, 16);
	m_shader = std::make_unique<Shader>("Content/Shaders/NormalMap.vs", "Content/Shaders/NormalMap.fs");
	m_texture = TextureLoader::LoadTexture("Content/Textures/brickwall.jpg", GL_RGBA8, GL_RGBA);
	m_normal = TextureLoader::LoadTexture("Content/Textures/brickwallnormal.jpg", GL_RGBA8, GL_RGBA);
}

SceneNormalMap::~SceneNormalMap()
{

}

void SceneNormalMap::OnUpdate(float deltaTime)
{

}

void SceneNormalMap::OnRender()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_texture->BindTexture(1);
	m_normal->BindTexture(2);
	m_shader->UseShaderProgram();

	m_plane->DrawPrimitive();
}

void SceneNormalMap::OnGUIRender()
{

}

