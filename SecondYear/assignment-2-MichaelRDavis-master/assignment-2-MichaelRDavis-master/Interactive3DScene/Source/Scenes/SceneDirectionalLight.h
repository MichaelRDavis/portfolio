#pragma once

#include "IScene.h"
#include <memory>
#include <glm.hpp>

class DirectionalLight;
class Shader;
class Texture;
class Cube;

class SceneDirectionalLight : public IScene
{
public:
	SceneDirectionalLight();
	~SceneDirectionalLight();

	void OnUpdate(float deltaTime) override;
	void OnRender() override;
	void OnGUIRender() override;

private:
	std::unique_ptr<DirectionalLight> m_light;
	std::unique_ptr<Cube> m_cube;
	std::unique_ptr<Shader> m_shader;
	std::shared_ptr<Texture> m_texture;

	glm::mat4 m_view;
	glm::mat4 m_model;
	glm::mat4 m_proj;
};