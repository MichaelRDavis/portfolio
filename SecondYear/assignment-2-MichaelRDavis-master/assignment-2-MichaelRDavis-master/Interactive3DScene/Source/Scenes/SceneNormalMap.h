#pragma once

#include "IScene.h"
#include <memory>

class Plane;
class Shader;
class Texture;

class SceneNormalMap : public IScene
{
public:
	SceneNormalMap();
	~SceneNormalMap();

	virtual void OnUpdate(float deltaTime);
	virtual void OnRender();
	virtual void OnGUIRender();

private:
	std::unique_ptr<Plane> m_plane;
	std::unique_ptr<Shader> m_shader;
	std::shared_ptr<Texture> m_texture;
	std::shared_ptr<Texture> m_normal;
};