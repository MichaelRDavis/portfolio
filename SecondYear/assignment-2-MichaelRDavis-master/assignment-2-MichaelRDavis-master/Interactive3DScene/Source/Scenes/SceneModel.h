#pragma once

#include "IScene.h"
#include <memory>

class Model;

class SceneModel : public IScene
{
public:
	SceneModel();
	~SceneModel();

private:
	std::unique_ptr<Model> m_model;
};