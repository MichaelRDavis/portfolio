#include "SceneRimLighting.h"
#include "Shaders/Shader.h"

SceneRimLighting::SceneRimLighting()
{
	m_shader = std::make_unique<Shader>("Content/Shaders/RimLighting.vs", "Content/Shaders/RimLighting.fs");
}

SceneRimLighting::~SceneRimLighting()
{

}

void SceneRimLighting::OnUpdate(float deltaTime)
{

}

void SceneRimLighting::OnRender()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_shader->UseShaderProgram();
}

void SceneRimLighting::OnGUIRender()
{

}
