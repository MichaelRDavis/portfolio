#pragma once

#include "IScene.h"
#include <memory>
#include <glm.hpp>

class Cube;
class Shader;

class SceneCube : public IScene
{
public:
	SceneCube();
	~SceneCube();

	void OnUpdate(float deltaTime) override;
	void OnRender() override;
	void OnGUIRender() override;

private:
	std::unique_ptr<Cube> m_cube;
	std::unique_ptr<Shader> m_shader;

	glm::mat4 m_view;
	glm::mat4 m_model;
	glm::mat4 m_proj;
};