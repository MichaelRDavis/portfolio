#pragma once

#include "IScene.h"
#include <glm.hpp>
#include <glad/glad.h>
#include <memory>

class Sphere;
class Shader;

class SceneGouraud : public IScene
{
public:
	SceneGouraud();
	~SceneGouraud();

	void OnUpdate(float deltaTime) override;
	void OnRender() override;
	void OnGUIRender() override;

private:
	std::unique_ptr<Sphere> m_sphere;
	std::unique_ptr<Shader> m_shader;

	glm::mat4 m_model;
	glm::mat4 m_view;
	glm::mat4 m_proj;

	glm::vec3 m_lightPosition;
	GLint m_intensity;
	GLfloat m_diffuse;
	GLfloat m_specular;
};