#pragma once

class IScene
{
public:
	IScene() = default;
	virtual ~IScene() = default;

	virtual void OnUpdate(float deltaTime) = 0;
	virtual void OnRender() = 0;
	virtual void OnGUIRender() = 0;
};