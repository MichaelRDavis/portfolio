#pragma once

#include "IScene.h"
#include <memory>
#include <glad/glad.h>
#include <glm.hpp>

class Plane;
class FrameBuffer;
class Shader;

class SceneShadowMap : public IScene
{
public:
	SceneShadowMap();
	~SceneShadowMap();

	virtual void OnUpdate(float deltaTime) override;
	virtual void OnRender() override;
	virtual void OnGUIRender() override;

private:
	std::unique_ptr<Plane> m_plane;
	std::unique_ptr<FrameBuffer> m_buffer;
	std::unique_ptr<Shader> m_shader;

	GLuint m_shadowWidth;
	GLuint m_shadowHeight;

	glm::mat4 m_lightViewMatrix;
	glm::mat4 m_lightProjectionMatrix;

	glm::mat4 m_cameraViewMatrix;
	glm::mat4 m_cameraProjectionMatrix;
};