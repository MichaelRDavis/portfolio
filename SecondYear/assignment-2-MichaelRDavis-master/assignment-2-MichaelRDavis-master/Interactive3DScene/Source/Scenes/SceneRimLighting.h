#pragma once

#include "IScene.h"
#include <memory>

class Shader;

class SceneRimLighting : public IScene
{
public:
	SceneRimLighting();
	~SceneRimLighting();

	virtual void OnUpdate(float deltaTime) override;
	virtual void OnRender() override;
	virtual void OnGUIRender() override;

private:
	std::unique_ptr<Shader> m_shader;
};