#include "ScenePhong.h"
#include "Primitives/Sphere.h"
#include "Shaders/Shader.h"
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>
#include <SDL.h>

ScenePhong::ScenePhong()
{
	m_sphere = std::make_unique<Sphere>(1.0f, 36, 18);
	m_shader = std::make_unique<Shader>("Content/Shaders/Phong.vs", "Content/Shaders/Phong.fs");
	m_shader->UseShaderProgram();
}

ScenePhong::~ScenePhong()
{

}

void ScenePhong::OnUpdate(float deltaTime)
{

}

void ScenePhong::OnRender()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_shader->UseShaderProgram();

	m_model = glm::mat4(1.0f);
	m_view = glm::mat4(1.0f);
	m_projection = glm::mat4(1.0f);
	m_model = glm::rotate(m_model, (float)SDL_GetTicks() / 1000 * glm::radians(50.0f), glm::vec3(0.3f, 1.0f, 0.0f));
	m_view = glm::translate(m_view, glm::vec3(0.0f, 0.0f, -3.0f));
	m_projection = glm::perspective(glm::radians(45.0f), 1280.0f / 768.0f, 0.1f, 100.0f);

	m_shader->SetUniformMat4f("model", m_model);
	m_shader->SetUniformMat4f("view", m_view);
	m_shader->SetUniformMat4f("projection", m_projection);

	m_sphere->DrawIndexPrimitive();
}

void ScenePhong::OnGUIRender()
{

}
