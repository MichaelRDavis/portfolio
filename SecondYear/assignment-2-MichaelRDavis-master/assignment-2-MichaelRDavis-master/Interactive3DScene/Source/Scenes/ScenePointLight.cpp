#include "ScenePointLight.h"
#include "Primitives/Sphere.h"
#include "Lighting/PointLight.h"

ScenePointLight::ScenePointLight()
{
	m_sphere = std::make_unique<Sphere>(1.0f, 36, 18);
	m_light = std::make_unique<PointLight>();
}

ScenePointLight::~ScenePointLight()
{

}

void ScenePointLight::OnUpdate(float deltaTime)
{

}

void ScenePointLight::OnRender()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_light->DrawLightMesh();
	m_light->EnableLight();
	m_sphere->DrawPrimitive();
}

void ScenePointLight::OnGUIRender()
{

}

