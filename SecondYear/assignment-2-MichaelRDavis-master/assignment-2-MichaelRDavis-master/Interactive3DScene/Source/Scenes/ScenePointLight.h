#pragma once

#include "IScene.h"
#include <memory>

class PointLight;
class Sphere;
class Shader;

class ScenePointLight : public IScene
{
public:
	ScenePointLight();
	~ScenePointLight();

	void OnUpdate(float deltaTime) override;
	void OnRender() override;
	void OnGUIRender() override;

private:
	std::unique_ptr<PointLight> m_light;
	std::unique_ptr<Sphere> m_sphere;
};