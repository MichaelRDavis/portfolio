#include "SceneCamera.h"
#include "Components/InputComponent.h"
#include "Components/CameraComponent.h"
#include "Shaders/Shader.h"
#include "Primitives/Cube.h"
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>
#include <SDL.h>

SceneCamera::SceneCamera()
{
	m_input = std::make_unique<InputComponent>();
	m_camera = std::make_unique<CameraComponent>();
	m_camera->SetPerespective(90.0f, 1280.0f / 768.0f, 0.1f, 100.0f);
	m_shader = std::make_unique<Shader>("Content/Shaders/Cube.vs", "Content/Shaders/Cube.fs");
	m_cube = std::make_unique<Cube>();

	m_positions.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	m_positions.push_back(glm::vec3(2.0f, 5.0f, -15.0f));
	m_positions.push_back(glm::vec3(-1.5f, -2.2f, -2.5f));
	m_positions.push_back(glm::vec3(-3.8f, -2.0f, -12.3f));
	m_positions.push_back(glm::vec3(2.4f, -0.4f, -3.5f));
	m_positions.push_back(glm::vec3(-1.7f, 3.0f, -7.5f));
	m_positions.push_back(glm::vec3(1.3f, -2.0f, -2.5f));
	m_positions.push_back(glm::vec3(1.5f, 2.0f, -2.5f));
	m_positions.push_back(glm::vec3(1.5f, 0.2f, -1.5f));
	m_positions.push_back(glm::vec3(-1.3f, 1.0f, -1.5f));
}

SceneCamera::~SceneCamera()
{

}

void SceneCamera::OnUpdate(float deltaTime)
{
	if (m_input->IsKeyPressed(SDL_SCANCODE_W))
	{
		m_camera->HandleKeyMovement(ECameraMovementMode::EForward, deltaTime);
	}
	if (m_input->IsKeyPressed(SDL_SCANCODE_S))
	{
		m_camera->HandleKeyMovement(ECameraMovementMode::EBackward, deltaTime);
	}
	if (m_input->IsKeyPressed(SDL_SCANCODE_D))
	{
		m_camera->HandleKeyMovement(ECameraMovementMode::ERight, deltaTime);
	}
	if (m_input->IsKeyPressed(SDL_SCANCODE_A))
	{
		m_camera->HandleKeyMovement(ECameraMovementMode::ELeft, deltaTime);
	}

	m_camera->Update(deltaTime);
}

void SceneCamera::OnRender()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_shader->UseShaderProgram();

	m_view = glm::mat4(1.0f);
	m_projection = glm::mat4(1.0f);
	m_projection = glm::perspective(glm::radians(m_camera->GetFOV()), m_camera->GetAspectRatio(), m_camera->GetNear(), m_camera->GetFar());
	m_view = m_camera->GetView();

	m_shader->SetUniformMat4f("proj", m_projection);
	m_shader->SetUniformMat4f("view", m_view);

	for (unsigned int i = 0; i < 10; i++)
	{
		m_model = glm::mat4(1.0f);
		m_model = glm::translate(m_model, m_positions[i]);
		float angle = 20.0f * i;
		m_model = glm::rotate(m_model, glm::radians(angle), glm::vec3(1.0f, 0.5f, 0.5f));
		m_shader->SetUniformMat4f("model", m_model);
		m_cube->DrawPrimitive();
	}
}

void SceneCamera::OnGUIRender()
{

}

