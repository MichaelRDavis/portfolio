#include "SceneSphereTexture.h"
#include "Primitives/Sphere.h"
#include "Shaders/Shader.h"

SceneSphereTexture::SceneSphereTexture()
{
	m_sphere = std::make_unique<Sphere>(1.0f, 36, 18);
	m_shader = std::make_unique<Shader>();
}

SceneSphereTexture::~SceneSphereTexture()
{

}

void SceneSphereTexture::OnUpdate(float deltaTime)
{

}

void SceneSphereTexture::OnRender()
{

}

void SceneSphereTexture::OnGUIRender()
{

}

