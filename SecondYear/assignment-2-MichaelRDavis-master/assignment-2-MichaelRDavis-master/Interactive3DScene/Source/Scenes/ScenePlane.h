#pragma once

#include "IScene.h"
#include <memory>

class Plane;
class Shader;

class ScenePlane : public IScene
{
public:
	ScenePlane();
	~ScenePlane();

	virtual void OnUpdate(float deltaTime) override;
	virtual void OnRender() override;
	virtual void OnGUIRender() override;

private:
	std::unique_ptr<Plane> m_plane;
	//std::unique_ptr<Shader> m_shader;
};