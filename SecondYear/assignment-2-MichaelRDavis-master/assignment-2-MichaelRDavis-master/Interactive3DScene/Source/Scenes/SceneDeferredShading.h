#pragma once

#include "IScene.h"

class GeometryBuffer;

class SceneDeferredShading : public IScene
{
public:
	SceneDeferredShading();
	~SceneDeferredShading();

private:
	GeometryBuffer* m_buffer;
};