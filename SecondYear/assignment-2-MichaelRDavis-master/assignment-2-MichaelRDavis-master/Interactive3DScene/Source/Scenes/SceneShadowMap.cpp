#include "SceneShadowMap.h"
#include "OpenGL/OpenGLBuffer.h"
#include "Primitives/Plane.h"
#include "Shaders/Shader.h"

SceneShadowMap::SceneShadowMap()
{
	m_shadowWidth = 1024;
	m_shadowHeight = 1024;
	m_buffer = std::make_unique<FrameBuffer>(m_shadowWidth, m_shadowHeight);
	m_buffer->GenerateBuffer();
	m_plane = std::make_unique<Plane>(24, 24);
	m_shader = std::make_unique<Shader>("Content/Shaders/ShadowMap.vs", "Content/Shaders/ShadowMap.vs");
}

SceneShadowMap::~SceneShadowMap()
{

}

void SceneShadowMap::OnUpdate(float deltaTime)
{

}

void SceneShadowMap::OnRender()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_buffer->BindFrameBuffer();

	m_shader->UseShaderProgram();

	m_plane->DrawPrimitive();
}

void SceneShadowMap::OnGUIRender()
{

}

