#include "ScenePlaneTexture.h"
#include "Primitives/Plane.h"
#include "Shaders/Shader.h"

ScenePlaneTexture::ScenePlaneTexture()
{
	m_plane = std::make_unique<Plane>(18, 18);
	m_shader = std::make_unique<Shader>();
}

ScenePlaneTexture::~ScenePlaneTexture()
{

}

void ScenePlaneTexture::OnUpdate(float deltaTime)
{

}

void ScenePlaneTexture::OnRender()
{

}

void ScenePlaneTexture::OnGUIRender()
{

}

