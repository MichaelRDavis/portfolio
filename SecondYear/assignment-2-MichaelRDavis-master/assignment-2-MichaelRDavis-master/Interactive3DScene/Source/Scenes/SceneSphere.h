#pragma once

#include "IScene.h"
#include <glm.hpp>
#include <memory>

class Sphere;
class Shader;

class SceneSphere : public IScene
{
public:
	SceneSphere();
	~SceneSphere();

	void OnUpdate(float deltaTime) override;
	void OnRender() override;
	void OnGUIRender() override;

private:
	std::unique_ptr<Sphere> m_sphere;
	std::unique_ptr<Shader> m_shader;

	glm::mat4 m_view;
	glm::mat4 m_model;
	glm::mat4 m_proj;
};