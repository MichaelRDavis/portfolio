#pragma once

#include "IScene.h"
#include <glm.hpp>
#include <vector>
#include <memory>

class Cube;
class Shader;
class Texture;

class SceneCubeTexture : public IScene
{
public:
	SceneCubeTexture();
	~SceneCubeTexture();

	void OnUpdate(float deltaTime);
	void OnRender();
	void OnGUIRender();

private:
	std::unique_ptr<Cube> m_cube;
	std::unique_ptr<Shader> m_shader;
	std::shared_ptr<Texture> m_texture;

	glm::mat4 m_view;
	glm::mat4 m_model;
	glm::mat4 m_proj;
};