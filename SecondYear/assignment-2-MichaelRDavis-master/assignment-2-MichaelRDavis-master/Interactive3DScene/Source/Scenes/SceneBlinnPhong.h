#pragma once

#include "IScene.h"
#include <memory>
#include <glm.hpp>

class Sphere;
class Shader;

class SceneBlinnPhong : public IScene
{
public:
	SceneBlinnPhong();
	~SceneBlinnPhong();

	void OnUpdate(float deltaTime) override;
	void OnRender() override;
	void OnGUIRender() override;

private:
	std::unique_ptr<Sphere> m_sphere;
	std::unique_ptr<Shader> m_shader;

	glm::mat4 m_view;
	glm::mat4 m_model;
	glm::mat4 m_projection;
};