#pragma once

#include "IScene.h"
#include <memory>
#include <glm.hpp>
#include <vector>

class CameraComponent;
class InputComponent;
class Shader;
class Cube;

class SceneCamera : public IScene
{
public:
	SceneCamera();
	~SceneCamera();

	void OnUpdate(float deltaTime) override;
	void OnRender() override;
	void OnGUIRender() override;

private:
	std::unique_ptr<InputComponent> m_input;
	std::unique_ptr<CameraComponent> m_camera;
	std::unique_ptr<Shader> m_shader;
	std::unique_ptr<Cube> m_cube;

	glm::mat4 m_view;
	glm::mat4 m_model;
	glm::mat4 m_projection;

	std::vector<glm::vec3> m_positions;
};