#pragma once

#include "IScene.h"
#include <memory>

class Sphere;
class Shader;
class Texture;

class SceneSphereTexture : public IScene
{
public:
	SceneSphereTexture();
	~SceneSphereTexture();

	virtual void OnUpdate(float deltaTime) override;
	virtual void OnRender() override;
	virtual void OnGUIRender() override;

private:
	std::unique_ptr<Sphere> m_sphere;
	std::unique_ptr<Shader> m_shader;
	//std::shared_ptr<Texture> m_texture;
};