#include "ScenePlane.h"
#include "Primitives/Plane.h"
#include "Shaders/Shader.h"

ScenePlane::ScenePlane()
{
	m_plane = std::make_unique<Plane>(16, 16);
	//m_shader = std::make_unique<Shader>("Content/Shaders/Cube.vs", "Content/Shaders/Cube.fs");
}

ScenePlane::~ScenePlane()
{

}

void ScenePlane::OnUpdate(float deltaTime)
{

}

void ScenePlane::OnRender()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//m_shader->UseShaderProgram();

	m_plane->DrawPrimitive();
}

void ScenePlane::OnGUIRender()
{

}

