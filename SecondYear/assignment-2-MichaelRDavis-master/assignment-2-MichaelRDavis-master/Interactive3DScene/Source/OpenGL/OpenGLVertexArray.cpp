#include "OpenGLVertexArray.h"
#include "OpenGLBuffer.h"

VertexArray::VertexArray()
{
	glGenVertexArrays(1, &m_vertexArray);
}

VertexArray::~VertexArray()
{
	glDeleteVertexArrays(1, &m_vertexArray);
}

void VertexArray::AddBuffer(const VertexBuffer& buffer, const BufferLayout& layout)
{
	BindVertexArray();
	buffer.BindBuffer();
	const auto& elements = layout.GetBufferElements();
	GLuint offset = 0;
	for (GLuint i = 0; i < elements.size(); i++)
	{
		const auto& element = elements[i];
		glEnableVertexAttribArray(i);
		glVertexAttribPointer(i, element.bufferSize, element.bufferType, element.normalized, layout.GetStride(), reinterpret_cast<void*>(offset));
		offset += element.bufferSize * BufferElement::GetBufferSize(element.bufferType);
	}
}

void VertexArray::BindVertexArray() const
{
	glBindVertexArray(m_vertexArray);
}

void VertexArray::UnBindVertexArray() const
{
	glBindVertexArray(0);
}
