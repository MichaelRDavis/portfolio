#pragma once

#include <glad/glad.h>

class VertexBuffer;
class BufferLayout;

class VertexArray
{
public:
	VertexArray();
	~VertexArray();

	void AddBuffer(const VertexBuffer& buffer, const BufferLayout& layout);

	void BindVertexArray() const;
	void UnBindVertexArray() const;

private:
	GLuint m_vertexArray;
};