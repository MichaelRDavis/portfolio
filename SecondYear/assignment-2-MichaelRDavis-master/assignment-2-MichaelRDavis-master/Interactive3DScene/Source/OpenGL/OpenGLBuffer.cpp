#include "OpenGLBuffer.h"
#include "Shaders/Texture.h"

BufferLayout::BufferLayout()
	: m_stride(0)
{

}

VertexBuffer::VertexBuffer(const void* bufferData, GLuint bufferSize)
{
	glGenBuffers(1, &m_vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, bufferSize * sizeof(GLfloat), bufferData, GL_STATIC_DRAW);
}

VertexBuffer::~VertexBuffer()
{
	glDeleteBuffers(1, &m_vertexBuffer);
}

void VertexBuffer::BindBuffer() const
{
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
}

void VertexBuffer::UnBindBuffer() const
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

FrameBuffer::FrameBuffer(GLuint width, GLuint height)
	: m_bufferWidth(width)
	, m_bufferHeight(height)
{
	
}

FrameBuffer::~FrameBuffer()
{
	glDeleteFramebuffers(1, &m_frameBufffer);
}

void FrameBuffer::GenerateBuffer()
{
	glGenFramebuffers(1, &m_frameBufffer);
	glGenFramebuffers(1, &m_depthBuffer);

	Texture texture;
	texture.GenerateTexture(m_bufferWidth, m_bufferHeight, 0, GL_RGB, GL_RGB, GL_UNSIGNED_BYTE, nullptr);

	BindDpethBuffer();
	BindFrameBuffer();

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture.GetTextureID(), 0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_depthBuffer);

	UnBindFrameBuffer();
}

void FrameBuffer::BindFrameBuffer() const
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_frameBufffer);
	glViewport(0, 0, m_bufferWidth, m_bufferHeight);
}

void FrameBuffer::UnBindFrameBuffer() const
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FrameBuffer::BindDpethBuffer() const
{
	glBindRenderbuffer(GL_RENDERBUFFER, m_depthBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, m_bufferWidth, m_bufferHeight);
}

void FrameBuffer::UnBindDepthBuffer() const
{
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

IndexBuffer::IndexBuffer(const void* bufferData, GLuint bufferSize)
{
	glGenBuffers(1, &m_indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, bufferSize * sizeof(GLuint), bufferData, GL_STATIC_DRAW);
}

void IndexBuffer::BindBuffer() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
}

void IndexBuffer::UnBindBuffer() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
