#pragma once

#include <glad/glad.h>
#include <vector>

struct BufferElement
{
	GLuint bufferType;
	GLuint bufferSize;
	GLubyte normalized;

	static GLuint GetBufferSize(GLuint bufferType)
	{
		switch (bufferType)
		{
		case GL_FLOAT:
			return 4;
		case GL_UNSIGNED_INT:
			return 4;
		case GL_UNSIGNED_BYTE:
			return 1;
		}
		return 0;
	}
};

class BufferLayout
{
public:
	BufferLayout();

	template<typename T>
	void AddBufferElement(GLuint count)
	{

	}

	template<>
	void AddBufferElement<GLfloat>(GLuint count)
	{
		m_bufferElements.push_back({ GL_FLOAT, count, GL_FALSE });
		m_stride += count * BufferElement::GetBufferSize(GL_FLOAT);
	}

	template<>
	void AddBufferElement<GLuint>(GLuint count)
	{
		m_bufferElements.push_back({ GL_UNSIGNED_INT, count, GL_FALSE });
		m_stride += count * BufferElement::GetBufferSize(GL_UNSIGNED_INT);
	}

	template<>
	void AddBufferElement<GLubyte>(GLuint count)
	{
		m_bufferElements.push_back({ GL_UNSIGNED_BYTE, count, GL_TRUE });
		m_stride += count * BufferElement::GetBufferSize(GL_UNSIGNED_BYTE);
	}

	inline std::vector<BufferElement> GetBufferElements() const { return m_bufferElements; }

	inline GLuint GetStride() const { return m_stride; }

private:
	std::vector<BufferElement> m_bufferElements;
	GLuint m_stride;
};

class VertexBuffer
{
public:
	VertexBuffer(const void* bufferData, GLuint bufferSize);
	~VertexBuffer();

	void BindBuffer() const;
	void UnBindBuffer() const;

private:
	GLuint m_vertexBuffer;
};

class IndexBuffer
{
public:
	IndexBuffer(const void* bufferData, GLuint bufferSize);

	void BindBuffer() const;
	void UnBindBuffer() const;

private:
	GLuint m_indexBuffer;
};

class FrameBuffer
{
public:
	FrameBuffer(GLuint width, GLuint height);
	~FrameBuffer();

	void GenerateBuffer();

	void BindFrameBuffer() const;
	void UnBindFrameBuffer() const;
	void BindDpethBuffer() const;
	void UnBindDepthBuffer() const;

private:
	GLuint m_bufferWidth;
	GLuint m_bufferHeight;
	GLuint m_frameBufffer;
	GLuint m_depthBuffer;
};

class GBuffer
{
public:
	GBuffer();
	~GBuffer();
};