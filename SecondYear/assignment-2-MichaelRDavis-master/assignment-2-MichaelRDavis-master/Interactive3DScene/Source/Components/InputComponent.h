#pragma once

#include "Component.h"
#include <SDL.h>

/** List of key states. */
enum class EKeyState
{
	ENone,
	EPressed,
	EReleased,
	EHeld
};

/**
 * 
 */
class InputComponent : public Component
{
public:
	/** Default constructor. */
	InputComponent();

	/** Default destructor. */
	~InputComponent();

	/** Initializes input interface. */
	void Init();

	/** Returns the current key state. */
	EKeyState GetKeyState(uint32_t KeyCode) const;

	/** Is key currently pressed this frame. */
	bool IsKeyPressed(uint32_t KeyCode) const;

	/** Is key released this frame. */
	bool IsKeyReleased(uint32_t KeyCode) const;

private:
	/** Current key state. */
	const uint8_t* m_CurrentKeyState;

	/** List of key states. */
	uint8_t m_KeyStates[SDL_NUM_SCANCODES];
};