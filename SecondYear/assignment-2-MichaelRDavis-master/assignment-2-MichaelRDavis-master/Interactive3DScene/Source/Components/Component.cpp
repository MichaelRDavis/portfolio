#include "Component.h"

void Component::Enable()
{
	m_isActive = true;
}

void Component::Disable()
{
	m_isActive = false;
}
