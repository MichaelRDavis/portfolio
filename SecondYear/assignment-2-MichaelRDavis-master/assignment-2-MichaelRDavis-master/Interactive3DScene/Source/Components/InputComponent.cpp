#include "InputComponent.h"

InputComponent::InputComponent()
{
	Init();
}

InputComponent::~InputComponent()
{

}

void InputComponent::Init()
{
	m_CurrentKeyState = SDL_GetKeyboardState(nullptr);
}

EKeyState InputComponent::GetKeyState(uint32_t KeyCode) const
{
	if (m_KeyStates[KeyCode] == 0)
	{
		if (m_CurrentKeyState[KeyCode] == 0)
		{
			return EKeyState::ENone;
		}
		else
		{
			return EKeyState::EPressed;
		}
	}
	else
	{
		if (m_CurrentKeyState[KeyCode] == 0)
		{
			return EKeyState::EReleased;
		}
		else
		{
			return EKeyState::EHeld;
		}
	}
}

bool InputComponent::IsKeyPressed(uint32_t KeyCode) const
{
	return m_CurrentKeyState[KeyCode];
}

bool InputComponent::IsKeyReleased(uint32_t KeyCode) const
{
	return m_CurrentKeyState[KeyCode];
}
