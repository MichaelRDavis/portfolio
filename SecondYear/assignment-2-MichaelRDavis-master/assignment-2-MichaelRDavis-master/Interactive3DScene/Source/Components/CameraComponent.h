#pragma once

#include <glm.hpp>
#include "Component.h"

enum class ECameraMovementMode
{
	EForward,
	EBackward,
	ELeft,
	ERight,
	EUp,
	EDown
};

class CameraComponent : public Component
{
public:
	CameraComponent();
	~CameraComponent() = default;

	void Update(float deltaTime);
	void UpdateCameraView();

	void SetPerespective(float fov, float aspectRatio, float near, float far);
	void SetOrthographic(float left, float right, float top, float bottom, float near, float far);

	void HandleKeyMovement(ECameraMovementMode movementMode, float deltaTime);
	void HandleMouseMovement(float x, float y);
	void HandleMouseScroll(float x, float y);

	inline glm::mat4 GetProjection() const { return m_projection; }
	inline glm::mat4 GetView() const { return m_view; }
	inline glm::vec3 GetPosition() const { return m_position; }
	inline glm::vec3 GetForwardVector() const { return m_forwardVector; }
	inline glm::vec3 GetUpVector() const { return m_upVector; }
	inline glm::vec3 GetRightVector() const { return m_rightVector; }
	inline float GetFOV() const { return m_fov; }
	inline float GetAspectRatio() const { return m_aspectRatio; }
	inline float GetNear() const { return m_near; }
	inline float GetFar() const { return m_far; }
	inline bool GetIsPerspective() const { return m_isPerspective; }

private:
	glm::mat4 m_projection;
	glm::mat4 m_view;
	glm::vec3 m_position;
	glm::vec3 m_forwardVector;
	glm::vec3 m_upVector;
	glm::vec3 m_rightVector;
	float m_fov;
	float m_aspectRatio;
	float m_near;
	float m_far;
	bool m_isPerspective;

	float m_yaw;
	float m_pitch;
	float m_damping = 5.0f;
	float m_movementSpeed = 10.0f;
	float m_mouseSensitivity = 0.1f;
};