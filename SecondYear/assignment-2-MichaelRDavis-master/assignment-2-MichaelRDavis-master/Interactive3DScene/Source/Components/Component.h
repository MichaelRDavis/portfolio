#pragma once

#include <cstdint>

class Component
{
public:
	Component() = default;
	~Component() = default;

	void Enable();
	void Disable();

	inline uint64_t GetID() const { return m_componentID; }
	inline bool GetIsActive() const { return m_isActive; }

protected:
	uint64_t m_componentID;
	bool m_isActive;
};