#include "CameraComponent.h"
#include <gtc/matrix_transform.hpp>

CameraComponent::CameraComponent()
{
	m_position = glm::vec3(0.0f, 0.0f, 0.0f);
	m_upVector = glm::vec3(0.0f, 1.0f, 0.0f);
	m_forwardVector = glm::vec3(0.0f, 0.0f, -1.0f);

	m_yaw = -90.0f;
}

void CameraComponent::Update(float deltaTime)
{
	glm::vec3 frontVector;
	frontVector.x = cos(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));

	UpdateCameraView();
}

void CameraComponent::UpdateCameraView()
{
	m_view = glm::lookAt(m_position, m_position + m_forwardVector, m_upVector);
}

void CameraComponent::SetPerespective(float fov, float aspectRatio, float near, float far)
{
	m_isPerspective = true;
	m_projection = glm::perspective(fov, aspectRatio, near, far);
	m_fov = fov;
	m_aspectRatio = aspectRatio;
	m_near = near;
	m_far = far;
}

void CameraComponent::SetOrthographic(float left, float right, float top, float bottom, float near, float far)
{
	m_isPerspective = false;
	m_projection = glm::ortho(left, right, bottom, top);
	m_near = near;
	m_far = far;
}

void CameraComponent::HandleKeyMovement(ECameraMovementMode movementMode, float deltaTime)
{
	float cameraSpeed = m_movementSpeed * deltaTime;
	if (movementMode == ECameraMovementMode::EForward)
	{
		m_position += cameraSpeed;
	}
	else if (movementMode == ECameraMovementMode::EBackward)
	{
		m_position -= cameraSpeed;
	}
	else if (movementMode == ECameraMovementMode::ERight)
	{
		m_position += cameraSpeed;
	}
	else if (movementMode == ECameraMovementMode::ELeft)
	{
		m_position -= cameraSpeed;
	}
	else if (movementMode == ECameraMovementMode::EUp)
	{
		m_position += cameraSpeed;
	}
	else if (movementMode == ECameraMovementMode::EDown)
	{
		m_position -= cameraSpeed;
	}
}

void CameraComponent::HandleMouseMovement(float x, float y)
{
	float xAxis = x * m_mouseSensitivity;
	float yAxis = y * m_mouseSensitivity;

	m_yaw += xAxis;
	m_pitch += yAxis;

	if (m_pitch > 89.0f)
	{
		m_pitch = 89.0f;
	}
	if (m_pitch < -89.0f)
	{
		m_pitch = -89.0f;
	}
}

void CameraComponent::HandleMouseScroll(float x, float y)
{
	
}
