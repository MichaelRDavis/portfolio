#pragma once

#include <memory>
#include <vector>
#include <glm.hpp>

class SceneNode
{
public:
	SceneNode();
	~SceneNode();

	void AddChildNode(std::shared_ptr<SceneNode> node);
	void RemoveChildNode(uint64_t nodeID);

	void SetNodeID(uint64_t newNodeID);

	inline glm::mat4 GetTransform() const { return m_transform; }
	inline glm::vec3 GetPosition() const { return m_position; }
	inline glm::vec3 GetRotation() const {return m_rotation; }
	inline glm::vec3 GetScale() const {	return m_scale;	}
	inline uint64_t GetNodeID() const {	return m_nodeID; }

private:
	std::vector<std::shared_ptr<SceneNode>> m_children;
	std::shared_ptr<SceneNode> m_parentNode;

	glm::mat4 m_transform;
	glm::vec3 m_position;
	glm::vec3 m_rotation;
	glm::vec3 m_scale;

	uint64_t m_nodeID;
};