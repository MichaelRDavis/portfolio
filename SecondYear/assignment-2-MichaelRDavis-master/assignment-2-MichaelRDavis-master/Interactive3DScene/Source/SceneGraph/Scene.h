#pragma once

#include <memory>

class SceneNode;

class Scene
{
public:
	Scene();
	~Scene();

private:
	std::shared_ptr<SceneNode> m_rootNode;
};