#pragma once

#include <memory>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

enum class EVerbosity
{
	ETrace,
	EInfo,
	EWarning,
	EError,
	ECritical
};

#define Error EVerbosity::EError

class Log
{
public:
	static void Init()
	{
		spdlog::set_pattern("%^[%T] %n: %v%$");
		m_logger = spdlog::stdout_color_mt("Engine_Log");
		m_logger->set_level(spdlog::level::trace);
	}

	inline static std::shared_ptr<spdlog::logger> GetLogger()
	{
		return m_logger;
	}

	static void SetVerbosity(EVerbosity verbosity, const char* msg)
	{
		switch (verbosity)
		{
		case EVerbosity::ETrace:
			GetLogger()->trace(msg);
			break;
		case EVerbosity::EInfo:
			GetLogger()->info(msg);
			break;
		case EVerbosity::EWarning:
			GetLogger()->warn(msg);
			break;
		case EVerbosity::EError:
			GetLogger()->error(msg);
			break;
		case EVerbosity::ECritical:
			GetLogger()->critical(msg);
			break;
		}
	}

private:
	static std::shared_ptr<spdlog::logger> m_logger;
};

#define LOG(EVerbosity, ...) Log::SetVerbosity(EVerbosity, __VA_ARGS__)