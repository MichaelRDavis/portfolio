#pragma once

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_STATIC
#include <stb_image.h>
#include <string>
#include <iostream>
#include "Shaders/Texture.h"

class TextureLoader
{
public:
	static std::shared_ptr<Texture> LoadTexture(const std::string& path, GLenum internalFormat, GLenum format)
	{
		std::shared_ptr<Texture> texture;
		int32_t width = 0;
		int32_t height = 0;
		int32_t bpp = 0;
		stbi_set_flip_vertically_on_load(1);
		void* buffer = stbi_load(path.c_str(), &width, &height, &bpp, 4);
		if (buffer)
		{
			texture = std::make_shared<Texture>();
			texture->GenerateTexture(width, height, bpp, internalFormat, format, GL_UNSIGNED_BYTE, buffer);
			stbi_image_free(buffer);
		}
		else
		{
			std::cout << "Failed to load texture" << std::endl;
			stbi_image_free(buffer);
			return nullptr;
		}

		return texture;
	}
};