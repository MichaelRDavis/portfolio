#pragma once

#include "Primitives/Primitive.h"
#include "Shaders/Material.h"
#include <string>
#include <memory>
#include <iostream>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

class ModelLoader
{
public:
	static void LoadModel(const std::string& path)
	{
		Assimp::Importer importer;
		const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);
		if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
		{
			std::cout << "Error Assimp: " << importer.GetErrorString() << std::endl;
			return;
		}

		ProcessNode(scene->mRootNode, scene);
	}

	static void ProcessNode(aiNode* node, const aiScene* scene)
	{
		for (uint32_t i = 0; i < node->mNumMeshes; i++)
		{
			aiMesh* assimpMesh = scene->mMeshes[node->mMeshes[i]];
			Primitive* mesh = ParseMesh(assimpMesh, scene);
		}

		for (uint32_t i = 0; i < node->mNumChildren; i++)
		{
			ProcessNode(node->mChildren[i], scene);
		}
	}

	static Primitive* ParseMesh(aiMesh* mesh, const aiScene* scene)
	{
		std::vector<glm::vec3> vertices;
		std::vector<glm::vec2> uv;
		std::vector<glm::vec3> normals;

		for (uint32_t i = 0; i < mesh->mNumVertices; i++)
		{
			vertices[i] = glm::vec3(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z);
			normals[i] = glm::vec3(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z);
			if (mesh->mTextureCoords[0])
			{
				uv[i] = glm::vec2(mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y);
			}
		}

		return &Primitive(vertices, uv, normals);
	}

	static Material* ParseMaterial(aiMaterial material, const aiScene* scene)
	{

	}
};