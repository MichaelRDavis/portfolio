#pragma once

#include <string>
#include <fstream>
#include <istream>

class GLSLParser
{
public:
	static std::string ParseShader(const std::string& file)
	{
		std::fstream fStream;
		fStream.open(file, std::ios::in | std::ios::binary);

		std::string strBuffer;
		fStream.seekg(0, std::ios::end);
		strBuffer.resize(fStream.tellg());
		fStream.seekg(0, std::ios::beg);
		fStream.read(&strBuffer[0], strBuffer.size());
		return strBuffer;
	}
};