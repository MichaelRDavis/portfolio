#pragma once

#include <glad/glad.h>
#include "Primitive.h"

class Sphere : public Primitive
{
public:
	Sphere(GLfloat radius, GLuint sectors, GLuint stacks);
	~Sphere() = default;
};