#include "Primitive.h"

Primitive::Primitive()
{

}

Primitive::Primitive(std::vector<glm::vec3> inVertices)
	: m_vertices(inVertices)
{

}

Primitive::Primitive(std::vector<glm::vec3> inVertices, std::vector<glm::vec2> inUV)
	: m_vertices(inVertices)
	, m_uv(inUV)
{

}

Primitive::Primitive(std::vector<glm::vec3> inVertices, std::vector<glm::vec2> inUV, std::vector<glm::vec3> inNormals)
	: m_vertices(inVertices)
	, m_uv(inUV)
	, m_normals(inNormals)
{

}

Primitive::Primitive(std::vector<glm::vec3> inVertices, std::vector<glm::vec2> inUV, std::vector<glm::vec3> inNormals, std::vector<glm::vec3> inTangents)
	: m_vertices(inVertices)
	, m_uv(inUV)
	, m_normals(inNormals)
	, m_tangents(inTangents)
{

}

void Primitive::DrawPrimitive()
{
	m_vao->BindVertexArray();
	if (m_vertices.size() > 0)
	{
		glDrawArrays(m_topology, 0, static_cast<GLsizei>(m_vertices.size()));
	}
}

void Primitive::DrawIndexPrimitive()
{
	m_vao->BindVertexArray();
	if (m_indices.size() > 0)
	{
		glDrawElements(m_topology, static_cast<GLsizei>(m_indices.size()), GL_UNSIGNED_INT, m_indices.data());
	}
}

void Primitive::SubmitPrimitive(bool interleaved)
{
	std::vector<GLfloat> bufferData;

	if (interleaved)
	{
		for (int32_t i = 0; i < m_vertices.size(); ++i)
		{
			bufferData.push_back(m_vertices[i].x);
			bufferData.push_back(m_vertices[i].y);
			bufferData.push_back(m_vertices[i].z);

			if (m_uv.size() > 0)
			{
				bufferData.push_back(m_uv[i].x);
				bufferData.push_back(m_uv[i].y);
			}

			if (m_normals.size() > 0)
			{
				bufferData.push_back(m_normals[i].x);
				bufferData.push_back(m_normals[i].y);
				bufferData.push_back(m_normals[i].z);
			}

			if (m_tangents.size() > 0)
			{
				bufferData.push_back(m_tangents[i].x);
				bufferData.push_back(m_tangents[i].y);
				bufferData.push_back(m_tangents[i].z);
			}
		}
	}
	else
	{
		for (int32_t i = 0; i < m_vertices.size(); ++i)
		{
			bufferData.push_back(m_vertices[i].x);
			bufferData.push_back(m_vertices[i].y);
			bufferData.push_back(m_vertices[i].z);
		}

		for (int32_t i = 0; i < m_uv.size(); ++i)
		{
			bufferData.push_back(m_uv[i].x);
			bufferData.push_back(m_uv[i].y);
		}

		for (int32_t i = 0; i < m_normals.size(); ++i)
		{
			bufferData.push_back(m_normals[i].x);
			bufferData.push_back(m_normals[i].y);
			bufferData.push_back(m_normals[i].z);
		}

		for (int32_t i = 0; i < m_tangents.size(); ++i)
		{
			bufferData.push_back(m_tangents[i].x);
			bufferData.push_back(m_tangents[i].y);
			bufferData.push_back(m_tangents[i].z);
		}
	}

	m_vao = std::make_unique<VertexArray>();
	m_vbo = std::make_unique<VertexBuffer>(bufferData.data(), bufferData.size());

	if (m_indices.size() > 0)
	{
		m_ebo = std::make_unique<IndexBuffer>(m_indices.data(), m_indices.size());
	}

	BufferLayout layout;
	if (interleaved)
	{
		if (m_vertices.size() > 0)
		{
			layout.AddBufferElement<GLfloat>(3);
			m_vao->AddBuffer(*m_vbo, layout);
		}
		if (m_uv.size() > 0)
		{
			layout.AddBufferElement<GLfloat>(2);
			m_vao->AddBuffer(*m_vbo, layout);
		}
		if (m_normals.size() > 0)
		{
			layout.AddBufferElement<GLfloat>(3);
			m_vao->AddBuffer(*m_vbo, layout);
		}
		if (m_tangents.size() > 0)
		{
			layout.AddBufferElement<GLfloat>(3);
			m_vao->AddBuffer(*m_vbo, layout);
		}
	}
	else
	{
		if (m_vertices.size() > 0)
		{
			m_vao->AddBuffer(*m_vbo, layout);
		}
		if (m_uv.size() > 0)
		{
			m_vao->AddBuffer(*m_vbo, layout);
		}
		if (m_normals.size() > 0)
		{
			m_vao->AddBuffer(*m_vbo, layout);
		}
		if (m_tangents.size() > 0)
		{
			m_vao->AddBuffer(*m_vbo, layout);
		}
	}

	m_vao->UnBindVertexArray();
}
