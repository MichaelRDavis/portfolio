#include "Cube.h"

Cube::Cube()
{
	m_vertices.push_back(glm::vec3(-0.5f, -0.5f, -0.5f));
	m_vertices.push_back(glm::vec3(0.5f, -0.5f, -0.5f));
	m_vertices.push_back(glm::vec3(0.5f, 0.5f, -0.5f));
	m_vertices.push_back(glm::vec3(0.5f, 0.5f, -0.5f));
	m_vertices.push_back(glm::vec3(-0.5f, 0.5f, -0.5f));
	m_vertices.push_back(glm::vec3(-0.5f, -0.5f, -0.5f));

	m_vertices.push_back(glm::vec3(-0.5f, -0.5f, 0.5f));
	m_vertices.push_back(glm::vec3(0.5f, -0.5f, 0.5f));
	m_vertices.push_back(glm::vec3(0.5f, 0.5f, 0.5f));
	m_vertices.push_back(glm::vec3(0.5f, 0.5f, 0.5f));
	m_vertices.push_back(glm::vec3(-0.5f, 0.5f, 0.5f));
	m_vertices.push_back(glm::vec3(-0.5f, -0.5f, 0.5f));

	m_vertices.push_back(glm::vec3(-0.5f, 0.5f, 0.5f));
	m_vertices.push_back(glm::vec3(-0.5f, 0.5f, -0.5f));
	m_vertices.push_back(glm::vec3(-0.5f, -0.5f, -0.5f));
	m_vertices.push_back(glm::vec3(-0.5f, -0.5f, -0.5f));
	m_vertices.push_back(glm::vec3(-0.5f, -0.5f, 0.5f));
	m_vertices.push_back(glm::vec3(-0.5f, 0.5f, 0.5f));

	m_vertices.push_back(glm::vec3(0.5f, 0.5f, 0.5f));
	m_vertices.push_back(glm::vec3(0.5f, 0.5f, -0.5f));
	m_vertices.push_back(glm::vec3(0.5f, -0.5f, -0.5f));
	m_vertices.push_back(glm::vec3(0.5f, -0.5f, -0.5f));
	m_vertices.push_back(glm::vec3(0.5f, -0.5f, 0.5f));
	m_vertices.push_back(glm::vec3(0.5f, 0.5f, 0.5f));

	m_vertices.push_back(glm::vec3(-0.5f, -0.5f, -0.5f));
	m_vertices.push_back(glm::vec3(0.5f, -0.5f, -0.5f));
	m_vertices.push_back(glm::vec3(0.5f, -0.5f, 0.5f));
	m_vertices.push_back(glm::vec3(0.5f, -0.5f, 0.5f));
	m_vertices.push_back(glm::vec3(-0.5f, -0.5f, 0.5f));
	m_vertices.push_back(glm::vec3(-0.5f, -0.5f, -0.5f));

	m_vertices.push_back(glm::vec3(-0.5f, 0.5f, -0.5f));
	m_vertices.push_back(glm::vec3(0.5f, 0.5f, -0.5f));
	m_vertices.push_back(glm::vec3(0.5f, 0.5f, 0.5f));
	m_vertices.push_back(glm::vec3(0.5f, 0.5f, 0.5f));
	m_vertices.push_back(glm::vec3(-0.5f, 0.5f, 0.5f));
	m_vertices.push_back(glm::vec3(-0.5f, 0.5f, -0.5f));

	m_uv.push_back(glm::vec2(0.0f, 0.0f));
	m_uv.push_back(glm::vec2(1.0f, 0.0f));
	m_uv.push_back(glm::vec2(1.0f, 1.0f));
	m_uv.push_back(glm::vec2(1.0f, 1.0f));
	m_uv.push_back(glm::vec2(0.0f, 1.0f));
	m_uv.push_back(glm::vec2(0.0f, 0.0f));

	m_uv.push_back(glm::vec2(0.0f, 0.0f));
	m_uv.push_back(glm::vec2(1.0f, 0.0f));
	m_uv.push_back(glm::vec2(1.0f, 1.0f));
	m_uv.push_back(glm::vec2(1.0f, 1.0f));
	m_uv.push_back(glm::vec2(0.0f, 1.0f));
	m_uv.push_back(glm::vec2(0.0f, 0.0f));

	m_uv.push_back(glm::vec2(0.0f, 0.0f));
	m_uv.push_back(glm::vec2(1.0f, 0.0f));
	m_uv.push_back(glm::vec2(1.0f, 1.0f));
	m_uv.push_back(glm::vec2(1.0f, 1.0f));
	m_uv.push_back(glm::vec2(0.0f, 1.0f));
	m_uv.push_back(glm::vec2(0.0f, 0.0f));

	m_uv.push_back(glm::vec2(0.0f, 0.0));
	m_uv.push_back(glm::vec2(1.0f, 0.0f));
	m_uv.push_back(glm::vec2(1.0f, 1.0f));
	m_uv.push_back(glm::vec2(1.0f, 1.0f));
	m_uv.push_back(glm::vec2(0.0f, 1.0f));
	m_uv.push_back(glm::vec2(0.0f, 0.0f));

	m_uv.push_back(glm::vec2(0.0f, 0.0f));
	m_uv.push_back(glm::vec2(1.0f, 0.0f));
	m_uv.push_back(glm::vec2(1.0f, 1.0f));
	m_uv.push_back(glm::vec2(1.0f, 1.0f));
	m_uv.push_back(glm::vec2(0.0f, 1.0f));
	m_uv.push_back(glm::vec2(0.0f, 0.0f));

	m_uv.push_back(glm::vec2(0.0f, 0.0f));
	m_uv.push_back(glm::vec2(1.0f, 0.0f));
	m_uv.push_back(glm::vec2(1.0f, 1.0f));
	m_uv.push_back(glm::vec2(1.0f, 1.0f));
	m_uv.push_back(glm::vec2(0.0f, 1.0f));
	m_uv.push_back(glm::vec2(0.0f, 0.0f));

	m_normals.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
	m_normals.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	m_normals.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
	m_normals.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	m_normals.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
	m_normals.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	m_normals.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
	m_normals.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	m_normals.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
	m_normals.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	m_normals.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
	m_normals.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	m_normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	SetTopology(GL_TRIANGLES);
	SubmitPrimitive();
}
