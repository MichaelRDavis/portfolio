#pragma once

#include <vector>
#include <memory>
#include <glm.hpp>
#include "OpenGl/OpenGLVertexArray.h"
#include "OpenGL/OpenGLBuffer.h"

class Primitive
{
public:
	Primitive();
	Primitive(std::vector<glm::vec3> inVertices);
	Primitive(std::vector<glm::vec3> inVertices, std::vector<glm::vec2> inUV);
	Primitive(std::vector<glm::vec3> inVertices, std::vector<glm::vec2> inUV, std::vector<glm::vec3> inNormals);
	Primitive(std::vector<glm::vec3> inVertices, std::vector<glm::vec2> inUV, std::vector<glm::vec3> inNormals, std::vector<glm::vec3> inTangents);
	~Primitive() = default;

	virtual void DrawPrimitive();
	virtual void DrawIndexPrimitive();
	virtual void SubmitPrimitive(bool interleaved = true);

	inline void SetTopology(GLenum topology)
	{
		m_topology = topology;
	}

private:
	std::unique_ptr<VertexArray> m_vao;
	std::unique_ptr<VertexBuffer> m_vbo;
	std::unique_ptr<IndexBuffer> m_ebo;

protected:
	std::vector<glm::vec3> m_vertices;
	std::vector<GLuint> m_indices;
	std::vector<glm::vec2> m_uv;
	std::vector<glm::vec3> m_normals;
	std::vector<glm::vec3> m_tangents;

	GLenum m_topology;
};