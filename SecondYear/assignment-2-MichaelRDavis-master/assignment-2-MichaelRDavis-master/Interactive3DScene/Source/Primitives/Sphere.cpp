#include "Sphere.h"
#include <cmath>
#include <SDL.h>
#include <glm.hpp>

Sphere::Sphere(GLfloat radius, GLuint sectors, GLuint stacks)
{
	GLfloat sectorStep = 2 * M_PI / sectors;
	GLfloat stackStep = M_PI / stacks;
	GLfloat sectorAngle = 0.0f;
	GLfloat stackAngle = 0.0f;

	GLfloat xPos = 0.0f;
	GLfloat yPos = 0.0f;
	GLfloat zPos = 0.0f;
	GLfloat xyPos = 0.0f;

	for (GLint x = 0; x <= stacks; ++x)
	{
		stackAngle = M_PI / 2 - x * stackStep;
		xyPos = radius * cosf(stackAngle);
		zPos = radius * sinf(stackAngle);

		for (GLint y = 0; y <= sectors; ++y)
		{
			sectorAngle = y * sectorStep;
			xPos = xyPos * cosf(sectorAngle);
			yPos = xyPos * sinf(sectorAngle);
			m_vertices.push_back(glm::vec3(xPos, yPos, zPos));
			m_normals.push_back(glm::vec3(xPos, yPos, zPos));
		}
	}


	GLint index1;
	GLint index2;
	for (GLint x = 0; x < stacks; ++x)
	{
		index1 = x * (sectors + 1);
		index2 = index1 + sectors + 1;

		for (GLint y = 0; y < sectors; ++y, ++index1, ++index2)
		{
			if (x != 0)
			{
				m_indices.push_back(index1);
				m_indices.push_back(index2);
				m_indices.push_back(index1 + 1);
			}

			if (x != (stacks - 1))
			{
				m_indices.push_back(index1 + 1);
				m_indices.push_back(index2);
				m_indices.push_back(index2 + 1);
			}
		}
	}

	SetTopology(GL_TRIANGLES);
	SubmitPrimitive();
}
