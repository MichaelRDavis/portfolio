#pragma once

#include "Primitive.h"

class Cube : public Primitive
{
public:
	Cube();
	~Cube() = default;
};