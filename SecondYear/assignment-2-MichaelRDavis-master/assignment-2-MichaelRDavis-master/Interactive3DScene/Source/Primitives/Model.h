#pragma once

#include "Primitive.h"

class Model : public Primitive
{
public:
	Model();
	~Model();
};