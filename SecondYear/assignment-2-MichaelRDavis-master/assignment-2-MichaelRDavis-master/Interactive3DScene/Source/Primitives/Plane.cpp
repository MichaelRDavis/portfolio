#include "Plane.h"

Plane::Plane(GLuint xSegments, GLuint ySegments)
{
	float dX = 1.0f / xSegments;
	float dY = 1.0f / ySegments;

	for (int32_t y = 0; y <= ySegments; ++y)
	{
		for (int32_t x = 0; x <= xSegments; ++x)
		{
			m_vertices.push_back(glm::vec3(dX * x * 2.0f - 1.0f, dY * y * 2.0f - 1.0f, 0.0f));
			m_uv.push_back(glm::vec2(dX * x, 1.0f - y * dY));
			m_normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
		}
	}

	SetTopology(GL_TRIANGLE_STRIP);
	SubmitPrimitive();
}

Plane::~Plane()
{

}
