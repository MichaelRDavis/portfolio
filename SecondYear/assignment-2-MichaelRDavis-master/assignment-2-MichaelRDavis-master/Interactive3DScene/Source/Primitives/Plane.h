#pragma once

#include "Primitive.h"

class Plane : public Primitive
{
public:
	Plane(GLuint xSegments, GLuint ySegments);
	~Plane();
};