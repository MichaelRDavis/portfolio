#pragma once

#include <glm.hpp>
#include <glad/glad.h>
#include <memory>

class Shader;
class Cube;

class PointLight
{
public:
	PointLight();
	~PointLight();

	void DrawLightMesh();
	void EnableLight();

private:
	std::unique_ptr<Shader> m_shader;
	std::unique_ptr<Cube> m_lightMesh;

	glm::vec3 m_position;
	glm::vec3 m_color;
	GLfloat m_intensity;
	GLfloat m_radius;
};