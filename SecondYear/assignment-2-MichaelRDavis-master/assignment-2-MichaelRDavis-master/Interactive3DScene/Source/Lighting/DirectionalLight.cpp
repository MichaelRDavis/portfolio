#include "DirectionalLight.h"
#include "Shaders/Shader.h"
#include "Primitives/Cube.h"

DirectionalLight::DirectionalLight()
{
	m_shader = std::make_unique<Shader>();
	m_shader->CreateShader("Content/Shaders/Light.vs", "Content/Shaders/Light.vs");
	m_lightMesh = std::make_unique<Cube>();
}

DirectionalLight::~DirectionalLight()
{

}

void DirectionalLight::DrawLightMesh()
{
	m_lightMesh->DrawPrimitive();
}

void DirectionalLight::EnableLight()
{
	m_shader->UseShaderProgram();
}
