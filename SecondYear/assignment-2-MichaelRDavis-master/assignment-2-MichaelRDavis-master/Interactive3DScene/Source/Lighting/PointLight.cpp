#include "PointLight.h"
#include "Primitives/Cube.h"
#include "Shaders/Shader.h"

PointLight::PointLight()
{
	m_lightMesh = std::make_unique<Cube>();
	m_shader = std::make_unique<Shader>("Content/Shaders/PointLight.vs", "Content/Shaders/PointLight.vs");
}

PointLight::~PointLight()
{

}

void PointLight::DrawLightMesh()
{

}

void PointLight::EnableLight()
{

}
