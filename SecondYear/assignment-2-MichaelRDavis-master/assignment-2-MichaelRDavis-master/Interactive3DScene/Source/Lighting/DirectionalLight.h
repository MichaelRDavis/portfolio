#pragma once

#include <memory>
#include <glad/glad.h>
#include <glm.hpp>

class Shader;
class Cube;

class DirectionalLight 
{
public:
	DirectionalLight();
	~DirectionalLight();

	void DrawLightMesh();
	void EnableLight();

private:
	std::unique_ptr<Shader> m_shader;
	std::unique_ptr<Cube> m_lightMesh;

	glm::vec3 m_lightDirection;
	glm::vec3 m_lightColor;
	GLfloat m_lightIntensity;

	glm::mat4 m_lightViewProjection;
};