#pragma once

#include <glad/glad.h>
#include <string>
#include <unordered_map>
#include <glm.hpp>

class Shader
{
public:
	Shader() = default;
	Shader(const std::string& vertexPath, const std::string& fragmentPath);
	~Shader() = default;

	void CreateShader(const std::string& vertexPath, const std::string& fragmentPath);

	void UseShaderProgram();

	void SetUniform1i(const std::string& name, int val);
	void SetUniform1i(const std::string& name, bool val);
	void SetUniform1f(const std::string& name, float val);
	void SetUniform2f(const std::string& name, glm::vec2 val);
	void SetUniform3f(const std::string& name, glm::vec3 val);
	void SetUniform4f(const std::string& name, glm::vec4 val);
	void SetUniformMat2f(const std::string& name, glm::mat2 mat);
	void SetUniformMat3f(const std::string& name, glm::mat3 mat);
	void SetUniformMat4f(const std::string& name, glm::mat4 mat);

private:
	GLint GetUniformLocation(const std::string& name);

private:
	GLuint m_shaderProgram;
	std::unordered_map<std::string, GLint> m_uniforms;
};