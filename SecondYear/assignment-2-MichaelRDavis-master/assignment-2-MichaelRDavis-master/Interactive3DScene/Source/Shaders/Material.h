#pragma once

#include <memory>

class Shader;

class Material
{
public:
	Material();
	~Material();

private:
	std::unique_ptr<Shader> m_shader;
};