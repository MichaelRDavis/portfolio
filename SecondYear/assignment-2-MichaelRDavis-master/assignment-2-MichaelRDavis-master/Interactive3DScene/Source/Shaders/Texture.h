#pragma once

#include <glad/glad.h>
#include <string>

class Texture
{
public:
	Texture() = default;
	~Texture() = default;

	void GenerateTexture(int32_t width, int32_t height, int32_t bpp, GLenum internalFormat, GLenum format, GLenum type, void* textureBuffer);

	void BindTexture(GLuint slot = 0);
	void UnBindTexture();

	inline void SetWidth(int32_t width) { m_width = width; }
	inline void SetHeight(int32_t height) { m_height = height; }
	inline void SetBPP(int32_t bpp) { m_bpp = bpp; }

	inline GLuint GetTextureID() const
	{
		return m_textureID;
	}

private:
	GLuint m_textureID;
	int32_t m_width;
	int32_t m_height;
	int32_t m_bpp;
};