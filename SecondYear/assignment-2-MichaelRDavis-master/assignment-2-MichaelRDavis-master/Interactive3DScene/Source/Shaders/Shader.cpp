#include "Shader.h"
#include "Resources/GLSLParser.h"
#include <gtc/type_ptr.hpp>
#include <iostream>

Shader::Shader(const std::string& vertexPath, const std::string& fragmentPath)
{
	CreateShader(vertexPath, fragmentPath);
}

void Shader::CreateShader(const std::string& vertexPath, const std::string& fragmentPath)
{
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	std::string vertexStr = GLSLParser::ParseShader(vertexPath);
	std::string frgamentStr = GLSLParser::ParseShader(fragmentPath);
	const GLchar* vertexSource = reinterpret_cast<const GLchar*>(vertexStr.c_str());
	const GLchar* fragmentSource = reinterpret_cast<const GLchar*>(frgamentStr.c_str());
	glShaderSource(vertexShader, 1, &vertexSource, 0);
	glShaderSource(fragmentShader, 1, &fragmentSource, 0);

	glCompileShader(vertexShader);
	GLint isVertexShaderCompiled = 0;
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &isVertexShaderCompiled);
	if (isVertexShaderCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> infoLog(maxLength);
		glGetShaderInfoLog(vertexShader, maxLength, &maxLength, &infoLog[0]);

		glDeleteShader(vertexShader);

		return;
	}

	glCompileShader(fragmentShader);
	GLint isFragmentShaderCompiled = 0;
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &isFragmentShaderCompiled);
	if (isFragmentShaderCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> infoLog(maxLength);
		glGetShaderInfoLog(fragmentShader, maxLength, &maxLength, &infoLog[0]);

		glDeleteShader(fragmentShader);

		return;
	}

	m_shaderProgram = glCreateProgram();
	glAttachShader(m_shaderProgram, vertexShader);
	glAttachShader(m_shaderProgram, fragmentShader);

	glLinkProgram(m_shaderProgram);
	GLint isLinked = 0;
	glGetProgramiv(m_shaderProgram, GL_LINK_STATUS, &isLinked);
	if (isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(m_shaderProgram, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(m_shaderProgram, maxLength, &maxLength, &infoLog[0]);

		glDeleteProgram(m_shaderProgram);

		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);

		return;
	}

	glValidateProgram(m_shaderProgram);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

void Shader::UseShaderProgram()
{
	glUseProgram(m_shaderProgram);
}

void Shader::SetUniform1i(const std::string& name, int val)
{
	glUniform1i(GetUniformLocation(name), val);
}

void Shader::SetUniform1i(const std::string& name, bool val)
{
	glUniform1i(GetUniformLocation(name), val);
}

void Shader::SetUniform1f(const std::string& name, float val)
{
	glUniform1f(GetUniformLocation(name), val);
}

void Shader::SetUniform2f(const std::string& name, glm::vec2 val)
{
	glUniform2f(GetUniformLocation(name), val.x, val.y);
}

void Shader::SetUniform3f(const std::string& name, glm::vec3 val)
{
	glUniform3f(GetUniformLocation(name), val.x, val.y, val.z);
}

void Shader::SetUniform4f(const std::string& name, glm::vec4 val)
{
	glUniform4f(GetUniformLocation(name), val.x, val.y, val.z, val.w);
}

void Shader::SetUniformMat2f(const std::string& name, glm::mat2 mat)
{
	glUniformMatrix2fv(GetUniformLocation(name), 1, GL_FALSE, glm::value_ptr(mat));
}

void Shader::SetUniformMat3f(const std::string& name, glm::mat3 mat)
{
	glUniformMatrix3fv(GetUniformLocation(name), 1, GL_FALSE, glm::value_ptr(mat));
}

void Shader::SetUniformMat4f(const std::string& name, glm::mat4 mat)
{
	glUniformMatrix4fv(GetUniformLocation(name), 1, GL_FALSE, glm::value_ptr(mat));
}

GLint Shader::GetUniformLocation(const std::string& name)
{
	GLint location = glGetUniformLocation(m_shaderProgram, name.c_str());
	if (m_uniforms.find(name) != m_uniforms.end())
	{
		return m_uniforms[name];
	}

	m_uniforms[name] = location;
	return location;
}
