#include "Texture.h"

void Texture::GenerateTexture(int32_t width, int32_t height, int32_t bpp, GLenum internalFormat, GLenum format, GLenum type, void* textureBuffer)
{
	m_width = width;
	m_height = height;
	m_bpp = bpp;

	glGenTextures(1, &m_textureID);
	glBindTexture(GL_TEXTURE_2D, m_textureID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, m_width, m_height, 0, format, type, textureBuffer);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::BindTexture(GLuint slot /*= 0*/)
{
	glActiveTexture(GL_TEXTURE0 + slot);
	glBindTexture(GL_TEXTURE_2D, m_textureID);
}

void Texture::UnBindTexture()
{
	glBindTexture(GL_TEXTURE_2D, 0);
}
