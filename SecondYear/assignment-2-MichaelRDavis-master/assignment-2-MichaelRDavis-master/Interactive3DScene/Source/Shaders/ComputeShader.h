#pragma once

#include <string>
#include <glad/glad.h>

class ComputeShader
{
public:
	ComputeShader() = default;
	ComputeShader(const std::string& path);
	~ComputeShader() = default;

	void CreateComputeShader();
	void UseComputeShader();

private:
	GLuint m_program;
};