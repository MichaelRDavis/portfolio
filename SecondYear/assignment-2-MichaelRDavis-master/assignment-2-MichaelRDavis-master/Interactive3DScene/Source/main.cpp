#include "RenderingApp/App.h"
#include <memory>
#include <iostream>
#include "Scenes/SceneCube.h"
#include "Scenes/ScenePlane.h"
#include "Scenes/SceneSphere.h"
#include "Scenes/SceneCubeTexture.h"
#include "Scenes/ScenePhong.h"
#include "Scenes/SceneGouraud.h"
#include "Scenes/SceneBlinnPhong.h"
#include "Scenes/SceneCamera.h"
#include "Scenes/SceneDirectionalLight.h"
#include "Scenes/ScenePointLight.h"
#include "Scenes/SceneRimLighting.h"
#include "Scenes/SceneNormalMap.h"
#include "Scenes/SceneShadowMap.h"

int main()
{
	try
	{
		std::unique_ptr<App> app = std::make_unique<App>();
		if (app)
		{
			app->Init();

			app->AddScene<SceneCube>("Cube");
			app->AddScene<ScenePlane>("Plane");
			app->AddScene<SceneSphere>("Sphere");
			app->AddScene<SceneCubeTexture>("Textured Cube");
			app->AddScene<SceneGouraud>("Gouraud Shading");
			app->AddScene<ScenePhong>("Phong Shading");
			app->AddScene<SceneBlinnPhong>("Blinn-Phong Shading");
			app->AddScene<SceneCamera>("Camera");
			app->AddScene<SceneDirectionalLight>("Directional Light");
			app->AddScene<ScenePointLight>("Point Light");
			app->AddScene<SceneRimLighting>("Rim Lighting");
			app->AddScene<SceneNormalMap>("Normal Mapping");
			app->AddScene<SceneShadowMap>("Shadow Mapping");

			while (!app->HasQuit())
			{
				app->Update(SDL_GetTicks());
			}
		}
	}
	catch (const std::exception& exception)
	{
		std::cout << exception.what() << std::endl;
	}

	return 0;
}