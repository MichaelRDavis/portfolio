#version 430 core

layout(location = 0) in vec3 position;

out vec4 color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	gl_Position = projection * view * model * vec4(position, 1.0);
	color = vec4(position, 1.0) * 0.5 + vec4(0.3, 0.3, 0.3, 0.3);
}
