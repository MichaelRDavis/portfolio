#version 430 core

layout (location = 0) in vec4 position;
layout (location = 1) in vec3 normal;

unfiform mat4 model;
uniform mat4 view;
uniform mat4 proj;

out VS_OUT
{
	vec3 N;
	vec3 L;
	vec3 V;
} vs_out;

uniform vec3 lightPosition = vec3(100.0, 100.0, 100.0);

void main()
{
	vec4 viewSpace = model * position;
	vs_out.N = mat3(model) * normal;
	vs_out.L = lightPosition - viewSpace.xyz;
	vs_out.V = -P.xyz;

	gl_Position = proj * viewSpace; 
}