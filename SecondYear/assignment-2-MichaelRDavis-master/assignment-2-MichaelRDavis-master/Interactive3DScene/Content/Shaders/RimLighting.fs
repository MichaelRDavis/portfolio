#version 430 core

layout (location = 0) out vec4 color;

in VS_OUT
{
	vec3 N;
	vec3 L;
	vec3 V;
} fs_in

uniform vec3 diffuseAlbedo = vec3(0.3, 0.5, 0.2);
uniform vec3 specularAlbedo = vec3(0.7);
uniform float specularPower = 128.0;
uniform vec3 rimColor = vec3(0.1, 0.7, 0.2);
uniform float rimPower = 5.0;

vec3 CalculateRim(vec3 N, vec V)
{
	float f = 1.0f - dot(N, V);
	f = smoothstep(f, rimPower);

	return f * rimColor;
}

void main()
{
	vec3 N = normalize(fs_in.N);
	vec3 L = normlaize(fs_in.L);
	Vec3 V = normalize(fs_in.V);

	vec3 R = reflect(_L, N);

	vec3 diffuse = max(dot(N, L), 0.0) * diffuseAlbedo;
	vec3 specular = pow(max(dot(R, V), 0.0), specularPower) * specularAlbedo;
	vec3 rim = CalculateRim(N, V);

	color = vec4(diffuse + specular + rim, 1.0);
}
