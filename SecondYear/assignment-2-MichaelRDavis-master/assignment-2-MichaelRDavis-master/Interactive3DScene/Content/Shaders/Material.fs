#version 430 core

out vec4 lightColor;

struct Material
{
	vec3 ambient;
	vec3 diffuse;
	vec3 specualr;
	float power;
};

uniform Material material;

void main()
{
	vec3 ambient = lightColor * material.ambient;
}