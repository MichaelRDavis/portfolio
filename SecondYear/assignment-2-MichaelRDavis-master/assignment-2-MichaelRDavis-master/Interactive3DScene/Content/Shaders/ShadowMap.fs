#version 450 core

layout (location = 0) out vec4 color;

layout (binding = 0) uniform sampler2DShadow shadowTex;

in VS_OUT
{
	vec4 shadowCoord;
} fs_in;

void main()
{
	color = textureProj(shadowTex, fs_in.shadowCoord) * vec4(1.0);
}