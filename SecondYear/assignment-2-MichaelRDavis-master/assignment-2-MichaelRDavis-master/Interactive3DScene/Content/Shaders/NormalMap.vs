#version 450 core

layout (location = 0) in vec4 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 tangent;
layout (location = 4) in vec2 texcoord;

out VS_OUT
{
	vec2 texcord;
	vec3 eyeDir;
	vec3 lightDir;
} vs_out;

uniform mat4 view;
uniform mat4 proj;
uniform vec3 lightPos = vec3(0.0, 0.0, 100.0);

void main()
{
	vec4 P = view * position;

	vec3 N = normalize(mat3(view) * normal);
	vec3 T = normalize(mat3(view) * tangent);

	vec3 B = cross(N, T);

	vec3 L = lightPos - P.xyz;
	vs_out.lightDir = normalize(vec3(dot(V, T), dot(V, B), dot(V, N)));

	vec3 V = -P.xyz;
	vs_out.eyeDir = normalize(vec3(dot(V, T), dot(V, B), dot(V, N)));

	vs_out.texcoord = texcoord;

	gl_Position = proj * P;
}