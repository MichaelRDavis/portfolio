#version 430 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec3 normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec3 lightPosition = vec3(100.0, 100.0, 100.0);
uniform vec3 diffuseAlbedo = vec3(0.5, 0.2, 0.7);
uniform vec3 specularAlbedo = vec3(0.7);
uniform float specularIntensity = 128.0;
uniform vec3 ambientLight = vec3(0.1, 0.1, 0.1);

out vec3 lightColor;

void main()
{
	vec4 P = model * position;

	vec3 N = mat3(model) * normal;
	vec3 L = lightPosition - P.xyz;
	vec3 V = -P.xyz;

	N = normalize(N);
	L = normalize(L);
	V = normalize(V);

	vec3 R = reflect(-L, N);

	vec3 diffuse = max(dot(N, L), 0.0) * diffuseAlbedo;
	vec3 specular = pow(max(dot(R, V), 0.0), specularIntensity) * specularAlbedo;

	lightColor = ambientLight + diffuse + specular;

	gl_Position = projection * view * P;
}