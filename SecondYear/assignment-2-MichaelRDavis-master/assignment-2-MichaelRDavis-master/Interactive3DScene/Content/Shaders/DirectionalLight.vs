#version 430 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

out VS_OUT
{
	vec3 colorPos;
	vec3 normal;
} vs_out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	vs_out.colorPos = vec3(model * vec4, (1.0));
	vs_out.normal = mat3(transpose(inverse(model))) * normal;
	gl_Position = projection * view * model * vec4(colorPos, 1.0);
}