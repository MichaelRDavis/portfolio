#version 450 core

uniform mat4 view;
uniform mat4 proj
uniform mat4 shadow;

layout (location = 0) in vec4 position;

out VS_OUT
{
	vec4 shadowCoord;
} vs_out;

void main()
{
	gl_Position = proj * view * position;
	vs_out.shadowCoord = shadow * position;
}