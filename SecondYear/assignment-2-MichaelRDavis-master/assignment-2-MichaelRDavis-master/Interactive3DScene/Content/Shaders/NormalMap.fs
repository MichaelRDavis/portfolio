#version 450 core

out vec4 color

layout (binding = 0) uniform sampler2D texColor;
layout (binding = 1) uniform sampler2D texNormal;

in VS_OUT
{
	vec2 texcoord;
	vec3 eyeDir;
	vec3 lightDir;
} fs_in;

void main()
{
	vec3 V = nomralize(fs_in.eyeDir);
	vec3 L = normalize(fs_in.lightDir);
	vec3 N = normalize(texture(texNormal), fs_in.texcoord).rgb * 2.0 - vec3(1.0));
	vec4 R = reflect(-L, N);

	vec3 diffuseAlbedo = texture(texColor, fs_in.texcoord).rgb;
	vec3 diffuse = max(dot(N, L), 0.0) * diffuseAlbedo;

	vec3 specularAlbedo = vec3(1.0);
	vec3 specular = max(pow(dot(R, V), 5.0), 0.0) * specularAlbedo;

	color = vec4(diffuse + specular, 1.0);
}