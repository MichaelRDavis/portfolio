#version 430 core

struct DirectionalLight
{
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
}

in vec3 color;
in vec3 normal;

uniform vec3 position;
uniform DirectionalLight light;

void main()
{
	vec3 ambient = light.ambient;

	vec3 normal = normalize(normal);
	vec3 lightDirection = normalize(-light.direction);
	float diff = max(dot(normal, lightDirection), 0.0);
	vec3 diffuse = light.diffuse * diff;

	vec3 viewDirection = normalize(position - color);
	vec3 reflectDirection = reflect(-lightDirection, normal);
	float spec = pow(max(dot(viewDirection, reflectDirection), 0.0), 2.0);
	vec3 specular = light.specular * spec;

	vec3 result = ambient + diffuse + specular;
	color = vec4(result, 1.0);
}