#version 430 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec3 normal;

uniform mat4 view;
uniform mat4 model;
uniform mat4 projection;

out vec3 aL;
out vec3 aN;
out vec3 aV;

uniform vec3 lightPos = vec3(100.0, 100.0, 100.0);

void main()
{
	vec4 P = model * position;
	aN = mat3(model) * normal;
	aL = lightPos - P.xyz;
	aV = -P.xyz;

	gl_Position = projection * view * P;
}