# 3D Rendering Application
A 3D rendering application detailing a multitude of 3D rendering techniques.

# Bibliography
* Yan Cherkov, YC. (2018) *OpenGL*. Available at: [Link](https://www.youtube.com/playlist?list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2) (Accessed on: 10/03/19)
* Yan Cherkov, YC. (2016) *Sparky*. Available at: [Link](https://github.com/TheCherno/Sparky)
* Joey de Vries, JV. (2019) *LearnOpenGL*. Available at: [Link](https://learnopengl.com/) (Accessed on: 12/03/19)
* Joey de Vries, JV. (2019) *Cell*. Available at: [Link](https://github.com/JoeyDeVries/Cell)
* Sellers, Wright, Haemel, GS, RSW, NH. (2016) *OpenGL SuperBible*. Seventh Edition. United States. Addison-Wesley.
* Sellers, Wright, Haemel, GS, RSW, NH. (2016) *OpenGL SuperBible 7th Edition Source Code*. [Link](https://github.com/openglsuperbible/sb7code)
* Ahn, S.A. (2018-2019) *OpenGL Sphere*. [Link](http://www.songho.ca/opengl/gl_sphere.html)