#shader vertex
#version 430 core

layout(location = 0) in vec3 position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main()
{
	gl_Position = proj * view * model * vec4(position, 1.0);
}

#shader fragment
#version 430 core

out vec4 color;

void main()
{
	color = vec4(1.0);
}