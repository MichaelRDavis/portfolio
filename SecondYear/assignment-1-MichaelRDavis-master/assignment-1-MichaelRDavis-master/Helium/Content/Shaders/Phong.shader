#shader vertex
#version 430 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec3 normal;

uniform mat4 view;
uniform mat4 model;
uniform mat4 proj;

out vec3 aL;
out vec3 aN;
out vec3 aV;

uniform vec3 lightPos = vec3(100.0, 100.0, 100.0);

void main()
{
	vec4 P = model * position;
	aN = mat3(model) * normal;
	aL = lightPos - P.xyz;
	aV = -P.xyz;

	gl_Position = proj * view * P;
}

#shader fragment
#version 430 core

layout(location = 0) out vec4 color;

in vec3 aL;
in vec3 aN;
in vec3 aV;

uniform vec3 diffuseAlbedo = vec3(0.5, 0.2, 0.7);
uniform vec3 specularAlbedo = vec3(0.7);
uniform float specularIntensity = 128.0;

void main()
{
	vec3 L = normalize(aL);
	vec3 N = normalize(aN);
	vec3 V = normalize(aV);

	vec3 reflection = reflect(-L, N);

	vec3 diffuse = max(dot(N, L), 0.0) * diffuseAlbedo;
	vec3 specular = pow(max(dot(reflection, V), 0.0), specularIntensity) + specularAlbedo;

	color = vec4(diffuse + specular, 1.0);
}