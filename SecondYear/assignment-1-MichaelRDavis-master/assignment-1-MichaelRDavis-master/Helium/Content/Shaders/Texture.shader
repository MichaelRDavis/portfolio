#shader vertex
#version 430 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texCoord;

out vec2 tex1;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main()
{
	gl_Position = proj * view * model * vec4(position, 1.0);
	tex1 = vec2(texCoord.x, texCoord.y);
}

#shader fragment
#version 430 core

out vec4 color;

in vec2 tex1;

uniform sampler2D tex;

void main()
{
	color = texture(tex, tex1);
}