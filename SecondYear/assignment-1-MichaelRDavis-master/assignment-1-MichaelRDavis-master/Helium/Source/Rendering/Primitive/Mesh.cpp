#include "Mesh.h"
#include "Rendering/RenderCore/Shader.h"

Mesh::Mesh(Vertices inVerts, Indices inIndices, Textures inTextures)
{
	m_vertices = inVerts;
	m_indices = inIndices;
	m_textures = inTextures;

	CreateMesh();
}

Mesh::~Mesh()
{

}

void Mesh::Draw(Shader* shader)
{
	GLuint diffuse = 1;
	GLuint specular = 1;

	for (GLuint i = 0; i < m_textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i);
		std::string num;
		std::string name = m_textures[i].textureType;
		if (name == "texture_diffuse")
		{
			num = std::to_string(diffuse++);
		}
		else if (name == "texture_specular")
		{
			num = std::to_string(specular++);
		}

		shader->SetUniform1f(("material." + name + num).c_str(), i);
		glBindTexture(GL_TEXTURE_2D, m_textures[i].textureID);
	}

	glActiveTexture(GL_TEXTURE0);

	glBindVertexArray(m_vao);
	glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void Mesh::CreateMesh()
{
	glGenVertexArrays(1, &m_vao);
	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ebo);

	glBindVertexArray(m_vao);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

	glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(SVertex), &m_vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size() * sizeof(GLuint), &m_indices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(SVertex), (void*)0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(SVertex), (void*)offsetof(SVertex, normal));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(SVertex), (void*)offsetof(SVertex, texCoords));

	glBindVertexArray(0);
}
