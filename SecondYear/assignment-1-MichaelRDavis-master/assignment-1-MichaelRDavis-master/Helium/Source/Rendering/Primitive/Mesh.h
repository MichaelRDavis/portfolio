#pragma once

#include <gl/glew.h>
#include <string>
#include <vector>
#include <glm.hpp>

class Shader;

struct SVertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec3 texCoords;
};

struct STexture
{
	GLuint textureID;
	std::string textureType;
};

class Mesh
{
	typedef std::vector<SVertex> Vertices;
	typedef std::vector<GLuint> Indices;
	typedef std::vector<STexture> Textures;

public:
	Mesh() = default;
	Mesh(Vertices inVerts, Indices inIndices, Textures inTextures);
	~Mesh();

	void Draw(Shader* shader);

	void CreateMesh();

public:
	Vertices m_vertices;
	Indices m_indices;
	Textures m_textures;

private:
	std::unique_ptr<Shader> m_shader;
	GLuint m_vao;
	GLuint m_vbo;
	GLuint m_ebo;
};