#pragma once

#include <vector>
#include <gl/glew.h>

class Sphere
{
public:
	Sphere();
	~Sphere();

private:
	std::vector<GLfloat> m_vertices;
};