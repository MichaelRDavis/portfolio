#pragma once

#include <cstdint>
#include <gl/glew.h>

/**  */
enum class EGBufferData
{

};

/**
 * Geometry buffer object for deferred shading.
 */
class GBuffer
{
public:
	/** Constructor. */
	GBuffer() = default;

	/** Destructor. */
	~GBuffer() = default;

	/** Create the GBuffer. */
	void Initialize(uint32_t viewportwidth, uint32_t viewportHeight);

	/** Destroy the GBuffer. */
	void Destroy();

private:
	/**  */
	GLuint m_buffer;

	/**  */
	GLuint m_bufferTexture[3];
};