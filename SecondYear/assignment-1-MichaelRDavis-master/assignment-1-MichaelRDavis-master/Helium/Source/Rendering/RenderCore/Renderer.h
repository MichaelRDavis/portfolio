/**
*	Renderer - Yan Cherkov - Writing a Basic Renderer in OpenGL
*	https://www.youtube.com/watch?v=jjaTTRFXRAk&list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2&index=16
*	Accessed on: 16/03/2019
*/

#pragma once

#include <GL/glew.h>

class VertexArray;
class IndexBuffer;
class Shader;

class Renderer
{
public:
	/** Default constructor. */
	Renderer();

	/** Default destructor. */
	~Renderer();

	void Clear() const;
	void Draw(VertexArray& va, const IndexBuffer& ib, const Shader& shader) const;

	/** Callback function to retrieve OpenGL debug output. */
	static void GLAPIENTRY MessageCallback(
		GLenum source,
		GLenum type,
		GLuint id,
		GLenum severity,
		GLsizei length,
		const GLchar* message,
		const void* userParam);
};
