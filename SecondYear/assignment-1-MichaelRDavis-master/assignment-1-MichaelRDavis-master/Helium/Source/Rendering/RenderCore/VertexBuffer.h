/**
*	VertexBuffer - Yan Cherkov - Abstracting OpneGL into Classes
*	https://www.youtube.com/watch?v=bTHqmzjm2UI&list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2&index=13
*	Accessed on: 16/03/2019
*/

#pragma once

#include <gl/glew.h>

class VertexBuffer
{
public:
	VertexBuffer(const void* Data, GLuint Size);
	~VertexBuffer();

	void Bind() const;
	void UnBind() const;

private:
	GLuint m_RendererID;
};
