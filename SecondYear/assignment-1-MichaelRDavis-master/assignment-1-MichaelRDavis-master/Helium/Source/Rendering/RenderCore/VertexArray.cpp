#include "VertexArray.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"

VertexArray::VertexArray()
{
	glGenVertexArrays(1, &m_RendererID);
}

VertexArray::~VertexArray()
{
	glDeleteVertexArrays(1, &m_RendererID);
}

void VertexArray::AddBuffer(const VertexBuffer& Buffer, const VertexBufferLayout& Layout)
{
	Bind();
	Buffer.Bind();
	const auto& Elements = Layout.GetElements();
	uint32_t Offset = 0;
	for (uint32_t i = 0; i < Elements.size(); i++)
	{
		const auto& Element = Elements[i];
		glEnableVertexAttribArray(i);
		glVertexAttribPointer(i, Element.Count, Element.Type, Element.Normalized, Layout.GetStride(), (const void*)Offset);
		Offset += Element.Count * VertexBufferElement::GetSize(Element.Type);
	}
}

void VertexArray::Bind() const
{
	glBindVertexArray(m_RendererID);
}

void VertexArray::UnBind() const
{
	glBindVertexArray(0);
}
