/**
*	Texture - Yan Cherkov - Textures in OpenGL
*	https://www.youtube.com/watch?v=n4k7ANAFsIQ&index=17&list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2
*	Accessed on: 16/03/2019
*/

#pragma once

#include <string>

class Texture
{
public:
	Texture(const std::string& Path);
	~Texture();

	void Bind(unsigned int Slot = 0) const;
	void UnBind() const;

	inline int GetWidth() const { return m_Width; }
	inline int GetHeight() const { return m_Height; }

private:
	unsigned int m_RendererID;
	std::string m_FilePath;
	unsigned char* m_LocalBuffer;
	int m_Width, m_Height, m_BPP;
};