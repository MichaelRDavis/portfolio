/**
*	Shader - Yan Cherkov - Shader Abstraction in OpenGL
*	https://www.youtube.com/watch?v=gDtHL6hy9R8&index=15&list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2
*	Accessed on: 16/03/2019
*/

#pragma once

#include <string>
#include <unordered_map>
#include <glm.hpp>
#include <GL/glew.h>

struct ShaderProgramSource
{
	std::string VertexSource;
	std::string FragmentSource;
	std::string TessControlSource;
	std::string TessEvaluationSource;
	std::string GeometrySource;
	std::string ComputeSource;
};

enum class ShaderType
{
	ENone = -1,
	EVertexShader = 0,
	EFragmentShader = 1,
	ETessControlShader = 2,
	ETessEvaluationShader = 3,
	EGeometryShader = 4,
	EComputeShader = 5,
};

class Shader
{
public:
	Shader(const std::string& filename);
	~Shader();

	void Bind() const;
	void UnBind() const;

	GLuint CreateShaderProgram(const std::string& source, GLuint type);

	GLuint CompileShader(const std::string& source, GLuint type);

	GLuint CreateShader(
		const std::string& vertexShader, 
		const std::string& fragmentShader, 
		const std::string& tessControlShader, 
		const std::string& tessEvaluationShader, 
		const std::string& geometryShader,
		const std::string& computeShader);

	ShaderProgramSource ParseShader(const std::string& filepath);

	void SetUniform1i(const std::string& Name, int val);
	void SetUniform1f(const std::string& Name, float a);
	void SetUniform2f(const std::string& name, float a, float b);
	void SetUniform3f(const std::string& name, float a, float b, float c);
	void SetUniform4f(const std::string& Name, float a, float b, float c, float d);
	void SetUniformMat2f(const std::string& name, glm::mat2 mat);
	void SetUniformMat3f(const std::string& name, glm::mat3 mat);
	void SetUniformMat4f(const std::string& Name, glm::mat4 mat);

private:
	int GetUniformLocation(const std::string& Name);

private:
	GLuint m_programPipeline;
	std::string m_filepath;
	std::unordered_map<std::string, GLint> m_uniformMap;
};
