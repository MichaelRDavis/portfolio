/**
*	VertexBufferLayout - Yan Cherkov - Buffer Layout Abstraction
*	https://www.youtube.com/watch?v=oD1dvfbyf6A&list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2&index=15&t=0s
*	Accessed on: 16/03/2019
*/

#pragma once

#include <vector>
#include <cassert>
#include <gl/glew.h>

struct VertexBufferElement
{
	GLuint Type;
	GLuint Count;
	unsigned char Normalized;

	static GLuint GetSize(GLuint Type)
	{
		switch (Type)
		{
		case GL_FLOAT:
			return 4;
		case GL_UNSIGNED_INT:
			return 4;
		case GL_UNSIGNED_BYTE:
			return 1;
		}
		assert(false);
		return 0;
	}
};

class VertexBufferLayout
{
public:
	VertexBufferLayout()
		: m_Stride(0)
	{

	}

	template<typename T>
	void Push(GLuint Count)
	{
		static_assert(false);
	}

	template<>
	void Push<float>(GLuint Count)
	{
		m_Elements.push_back({ GL_FLOAT, Count, GL_FALSE });
		m_Stride += Count * VertexBufferElement::GetSize(GL_FLOAT);
	}

	template<>
	void Push<unsigned int>(GLuint Count)
	{
		m_Elements.push_back({ GL_UNSIGNED_INT, Count, GL_FALSE });
		m_Stride += Count * VertexBufferElement::GetSize(GL_UNSIGNED_INT);
	}

	template<>
	void Push<unsigned char>(GLuint Count)
	{
		m_Elements.push_back({ GL_UNSIGNED_BYTE, Count, GL_TRUE });
		m_Stride += Count * VertexBufferElement::GetSize(GL_UNSIGNED_BYTE);
	}

	inline const std::vector<VertexBufferElement> GetElements() const { return m_Elements; }

	inline GLuint GetStride() const { return m_Stride; }

private:
	std::vector<VertexBufferElement> m_Elements;
	GLuint m_Stride;
};
