#include "GBuffer.h"

void GBuffer::Initialize(uint32_t viewportwidth, uint32_t viewportHeight)
{
	glGenBuffers(1, &m_buffer);
	glBindFramebuffer(GL_FRAMEBUFFER, m_buffer);

	glGenTextures(3, m_bufferTexture);
	glBindTexture(GL_TEXTURE_2D, m_bufferTexture[0]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32UI, viewportwidth, viewportHeight);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glGenTextures(3, m_bufferTexture);
	glBindTexture(GL_TEXTURE_2D, m_bufferTexture[1]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32UI, viewportwidth, viewportHeight);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glGenTextures(3, m_bufferTexture);
	glBindTexture(GL_TEXTURE_2D, m_bufferTexture[2]);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA32UI, viewportwidth, viewportHeight);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

void GBuffer::Destroy()
{

}
