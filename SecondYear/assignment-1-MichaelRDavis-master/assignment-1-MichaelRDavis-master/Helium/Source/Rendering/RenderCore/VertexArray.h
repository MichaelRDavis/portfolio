/**
*	VertexArray - Yan Cherkov - Buffer Layout Abstraction in OpenGL
*	https://www.youtube.com/watch?v=oD1dvfbyf6A&list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2&index=14
*	Accessed on: 16/03/2019
*/

#pragma once

#include <gl/glew.h>

class VertexBuffer;
class VertexBufferLayout;

class VertexArray
{
public:
	VertexArray();
	~VertexArray();

	void AddBuffer(const VertexBuffer& Buffer, const VertexBufferLayout& Layout);

	void Bind() const;
	void UnBind() const;

private:
	GLuint m_RendererID;
};
