/**
*	IndexBuffer - Yan Cherkov - Abstracting OpenGL into Classes
*	https://www.youtube.com/watch?v=bTHqmzjm2UI&list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2&index=13
*	Accessed on: 16/03/2019
*/

#pragma once

#include <gl/glew.h>

class IndexBuffer
{
public:
	IndexBuffer(const GLuint* Data, GLuint Count);
	~IndexBuffer();

	void Bind() const;
	void UnBind() const;

	inline GLuint GetCount() const { return m_Count; }

private:
	GLuint m_RendererID;
	GLuint m_Count;
};
