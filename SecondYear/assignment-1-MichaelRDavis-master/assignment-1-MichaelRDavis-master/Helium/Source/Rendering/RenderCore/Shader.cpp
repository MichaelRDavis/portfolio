#include "Shader.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <gtc/type_ptr.hpp>

Shader::Shader(const std::string& filename)
	: m_filepath(filename)
	, m_programPipeline(0)
{
	ShaderProgramSource source = ParseShader(filename);
	m_programPipeline = CreateShader(
		source.VertexSource, 
		source.FragmentSource, 
		source.TessControlSource,
		source.TessEvaluationSource, 
		source.GeometrySource, 
		source.ComputeSource);
}

Shader::~Shader()
{
	glDeleteProgram(m_programPipeline);
}

void Shader::Bind() const
{
	glUseProgram(m_programPipeline);
}

void Shader::UnBind() const
{
	glUseProgram(0);
}

GLuint Shader::CreateShaderProgram(const std::string& source, GLuint type)
{
	const char* src = source.c_str();
	GLuint program = glCreateShaderProgramv(type, 1, &src);
	return program;
}

GLuint Shader::CompileShader(const std::string& source, GLuint type)
{
	GLuint id = glCreateShader(type);
	const char* src = source.c_str();
	glShaderSource(id, 1, &src, nullptr);
	glCompileShader(id);

	int result;
	glGetShaderiv(id, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE)
	{
		int length;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
		char* message = (char*)alloca(length * sizeof(char));
		glGetShaderInfoLog(id, length, &length, message);
		std::string shaderType;
		switch (type)
		{
		case GL_VERTEX_SHADER:
			shaderType = "vertex";
			break;
		case GL_FRAGMENT_SHADER:
			shaderType = "fragment";
			break;
		case GL_TESS_CONTROL_SHADER:
			shaderType = "tessellation control";
			break;
		case GL_TESS_EVALUATION_SHADER:
			shaderType = "tessellation evaluation";
			break;
		case GL_GEOMETRY_SHADER:
			shaderType = "geometry shader";
			break;
		case GL_COMPUTE_SHADER:
			shaderType = "compute shader";
			break;
		}
		std::cout << "Failed to compile " << shaderType << " shader" << std::endl;
		std::cout << message << std::endl;
		glDeleteShader(id);
		return 0;
	}

	return id;
}

GLuint Shader::CreateShader(
	const std::string& vertexShader,
	const std::string& fragmentShader,
	const std::string& tessControlShader,
	const std::string& tessEvaluationShader,
	const std::string& geometryShader,
	const std::string& computeShader)
{
	//GLuint program = glCreateProgram();
	GLuint vs = CompileShader(vertexShader, GL_VERTEX_SHADER);
	GLuint fs = CompileShader(fragmentShader, GL_FRAGMENT_SHADER);
	GLuint tcs = CompileShader(tessControlShader, GL_TESS_CONTROL_SHADER);
	GLuint tes = CompileShader(tessEvaluationShader, GL_TESS_EVALUATION_SHADER);
	GLuint gs = CompileShader(geometryShader, GL_GEOMETRY_SHADER);
	GLuint cs = CompileShader(computeShader, GL_COMPUTE_SHADER);

	// TODO: This doesn't work, need to find a fix.
	//GLuint vs_program = glCreateProgram();
	//glProgramParameteri(vs_program, GL_PROGRAM_SEPARABLE, GL_TRUE);
	//glAttachShader(vs_program, vs);
	//glLinkProgram(vs_program);
	//GLuint fs_program = glCreateProgram();
	//glProgramParameteri(fs_program, GL_PROGRAM_SEPARABLE, GL_TRUE);
	//glAttachShader(fs_program, fs);
	//glLinkProgram(fs_program);
	GLuint program = glCreateProgram();
	//glGenProgramPipelines(1, &program);
	//glUseProgramStages(program, GL_VERTEX_SHADER_BIT, vs_program);
	//glUseProgramStages(program, GL_FRAGMENT_SHADER_BIT, fs_program);
	//glBindProgramPipeline(program);

	// TODO: Need to find a way to attach multiple shader programs to the same pipeline. 
	glAttachShader(program, vs);
	glAttachShader(program, fs);
	//glAttachShader(program, tcs);
	//glAttachShader(program, tes);
	//glAttachShader(program, gs);
	//glAttachShader(program, cs);
	glLinkProgram(program);
	glValidateProgram(program);

	//glDetachShader(vs_program, vs);
	//glDetachShader(fs_program, fs);
	glDeleteShader(vs);
	glDeleteShader(fs);
	//glDeleteShader(tcs);
	//glDeleteShader(tes);
	//glDeleteShader(gs);
	//glDeleteShader(cs);

	return program;
}

ShaderProgramSource Shader::ParseShader(const std::string& filepath)
{
	std::fstream stream(filepath);
	std::string line;
	std::stringstream ss[2];
	ShaderType type = ShaderType::ENone;
	while (getline(stream, line))
	{
		if (line.find("#shader") != std::string::npos)
		{
			if (line.find("vertex") != std::string::npos)
			{
				type = ShaderType::EVertexShader;
			}
			else if (line.find("fragment") != std::string::npos)
			{
				type = ShaderType::EFragmentShader;
			}
			else if (line.find("tessControl") != std::string::npos)
			{
				type = ShaderType::ETessControlShader;
			}
			else if (line.find("tessEvaluation") != std::string::npos)
			{
				type = ShaderType::ETessEvaluationShader;
			}
			else if (line.find("geometry") != std::string::npos)
			{
				type = ShaderType::EGeometryShader;
			}
			else if (line.find("compute") != std::string::npos)
			{
				type = ShaderType::EComputeShader;
			}
		}
		else
		{
			ss[(int)type] << line << "\n";
		}
	}

	return { ss[0].str(), ss[1].str() }; //ss[2].str(), ss[3].str(), ss[4].str(), ss[5].str() };
}

void Shader::SetUniform1i(const std::string& Name, int val)
{
	glUniform1i(GetUniformLocation(Name), val);
}

void Shader::SetUniform1f(const std::string& Name, float a)
{
	glUniform1f(GetUniformLocation(Name), a);
}

void Shader::SetUniform2f(const std::string& name, float a, float b)
{
	glUniform2f(GetUniformLocation(name), a, b);
}

void Shader::SetUniform3f(const std::string& name, float a, float b, float c)
{
	glUniform3f(GetUniformLocation(name), a, b, c);
}

void Shader::SetUniform4f(const std::string& Name, float a, float b, float c, float d)
{
	glUniform4f(GetUniformLocation(Name), a, b, c, d);
}

void Shader::SetUniformMat2f(const std::string& name, glm::mat2 mat)
{
	glUniformMatrix2fv(GetUniformLocation(name), 1, GL_FALSE, glm::value_ptr(mat));
}

void Shader::SetUniformMat3f(const std::string& name, glm::mat3 mat)
{
	glUniformMatrix3fv(GetUniformLocation(name), 1, GL_FALSE, glm::value_ptr(mat));
}

void Shader::SetUniformMat4f(const std::string& Name, glm::mat4 mat)
{
	glUniformMatrix4fv(GetUniformLocation(Name), 1, GL_FALSE, glm::value_ptr(mat));
}

int Shader::GetUniformLocation(const std::string& Name)
{
	GLint location = glGetUniformLocation(m_programPipeline, Name.c_str());
	if (m_uniformMap.find(Name) != m_uniformMap.end())
	{
		return m_uniformMap[Name];
	}

	m_uniformMap[Name] = location;
	return location;
}
