#pragma once

#include <SDL.h>

/**
 * Generic window.
 */
class Window
{
public:
	/** Default Window constructor. */
	Window();

	/** Default Window destructor. */
	~Window();

	/** Create window. */
	void InitWindow();

	/** Render the window, called every frame. */
	void RenderWindow();

	void Clear();
	void SwapBuffers();

	/** Destroy window, called on window close. */
	void DestroyWindow();

	/** Poll window events. */
	void PollWindowEvents(SDL_Event& Event);

	/** Returns a pointer the current window. */
	inline SDL_Window* GetWindow() const
	{
		return m_Window;
	}

	/** Returns a handle to the current OpenGL context. */
	inline SDL_GLContext GetContext() const
	{
		return m_ContextHandle;
	}

private:
	/** Pointer to the OS specific window. */
	SDL_Window* m_Window;

	/** Pointer to the window surface. */
	SDL_Surface* m_Surface;

	/** Handle to the window context. */
	SDL_GLContext m_ContextHandle;

	/** handle to the current display. */
	SDL_DisplayMode m_displayMode;

	/** Width of the screen. */
	int m_ScreenWidth;

	/** Height of the window. */
	int m_ScreenHeight;

	/** Width of the window. */
	int m_WindowWidth;

	/** Height of the window. */
	int m_WindowHeight;

	/** Flag if window is fullscreen. */
	bool m_isFullscreen;

	/** Flag if window is windowed. */
	bool m_isWindowed;

	/** Flag if VSync is enabled. */
	bool m_IsVSyncEnabled;
};
