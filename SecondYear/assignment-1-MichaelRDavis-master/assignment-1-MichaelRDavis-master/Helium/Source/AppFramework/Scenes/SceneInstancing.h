#pragma once

#include "Scene.h"
#include <GL/glew.h>
#include <glm.hpp>

class Shader;

class SceneInstancing : public Scene
{
public:
	SceneInstancing();
	~SceneInstancing();

	void OnUpdate(float deltaTime) override;
	void OnRender() override;
	void OnImGuiRender() override;

private:
	std::vector<glm::vec3> m_positions;
	std::unique_ptr<Shader> m_shader;

	GLuint m_vao;
	GLuint m_vbo;
	glm::mat4 m_view;
	glm::mat4 m_model;
	glm::mat4 m_proj;
	bool m_wireframe;
};