#include "ScenePyramid.h"
#include "Rendering/RenderCore/Shader.h"

ScenePyramid::ScenePyramid()
{
	GLfloat vertices[] = {
		-1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f, -1.0f, -1.0f -1.0f, 0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f, -1.0f -1.0f, 1.0f, 0.0f, 1.0f, 0.0f,
		-1.0f -1.0f, -1.0f, 1.0f, -1.0f, 1.0f, -1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f,
	};

	m_vao = 0;
	m_vbo = 0;
}

ScenePyramid::~ScenePyramid()
{

}

void ScenePyramid::OnRender()
{

}

void ScenePyramid::OnUpdate(float deltaTime)
{

}

void ScenePyramid::OnImGuiRender()
{

}
