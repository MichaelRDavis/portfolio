#pragma once

#include "Scene.h"
#include <gl/glew.h>
#include <glm.hpp>

class Shader;

class ScenePyramid : public Scene
{
public:
	ScenePyramid();
	~ScenePyramid();

	void OnRender() override;
	void OnUpdate(float deltaTime) override;
	void OnImGuiRender() override;

private:
	GLuint m_vao;
	GLuint m_vbo;

	glm::mat4 m_view;
	glm::mat4 m_model;
	glm::mat4 m_proj;

	std::unique_ptr<Shader> m_shader;
};