#include "SceneClearColor.h"
#include "Rendering/RenderCore/Renderer.h"
#include <imgui.h>
#include <GL/glew.h>

SceneClearColor::SceneClearColor()
	: m_clearColor{ 0.0f, 0.0f, 0.0f, 1.0f }
{

}

SceneClearColor::~SceneClearColor()
{

}

void SceneClearColor::OnUpdate(float deltaTime)
{

}

void SceneClearColor::OnRender()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(m_clearColor[0], m_clearColor[1], m_clearColor[2], m_clearColor[3]);
}

void SceneClearColor::OnImGuiRender()
{
	ImGui::ColorPicker4("Clear Color", m_clearColor);
}
