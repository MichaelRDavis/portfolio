#include "SceneCamera.h"
#include "Rendering/RenderCore/Shader.h"
#include "Components/Camera.h"
#include "Components/Input.h"
#include <imgui.h>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>
#include <SDL.h>
#include <vector>

SceneCamera::SceneCamera()
{
	m_camera = std::make_unique<Camera>();
	m_input = std::make_unique<Input>();

	m_positions.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	m_positions.push_back(glm::vec3(2.0f, 5.0f, -15.0f));
	m_positions.push_back(glm::vec3(-1.5f, -2.2f, -2.5f));
	m_positions.push_back(glm::vec3(-3.8f, -2.0f, -12.3f));
	m_positions.push_back(glm::vec3(2.4f, -0.4f, -3.5f));
	m_positions.push_back(glm::vec3(-1.7f, 3.0f, -7.5f));
	m_positions.push_back(glm::vec3(1.3f, -2.0f, -2.5f));
	m_positions.push_back(glm::vec3(1.5f, 2.0f, -2.5f));
	m_positions.push_back(glm::vec3(1.5f, 0.2f, -1.5f));
	m_positions.push_back(glm::vec3(-1.3f, 1.0f, -1.5f));

	m_vao = 0;
	m_vbo = 0;

	GLfloat vertices[] =
	{
		-0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
		 0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
		 0.5f,  0.5f, -0.5f, 0.0f, 0.0f, 1.0f,
		 0.5f,  0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
		-0.5f,  0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
		-0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 1.0f,

		-0.5f, -0.5f,  0.5f, 1.0f, 0.0f, 0.0f,
		 0.5f, -0.5f,  0.5f, 0.0f, 1.0f, 0.0f,
		 0.5f,  0.5f,  0.5f, 0.0f, 0.0f, 1.0f,
		 0.5f,  0.5f,  0.5f, 1.0f, 0.0f, 0.0f,
		-0.5f,  0.5f,  0.5f, 0.0f, 1.0f, 0.0f,
		-0.5f, -0.5f,  0.5f, 0.0f, 0.0f, 1.0f,

		-0.5f,  0.5f,  0.5f, 1.0f, 0.0f, 0.0f,
		-0.5f,  0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
		-0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
		-0.5f, -0.5f,  0.5f, 0.0f, 1.0f, 0.0f,
		-0.5f,  0.5f,  0.5f, 0.0f, 0.0f, 1.0f,

		 0.5f,  0.5f,  0.5f, 1.0f, 0.0f, 0.0f,
		 0.5f,  0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
		 0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 1.0f,
		 0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
		 0.5f, -0.5f,  0.5f, 0.0f, 1.0f, 0.0f,
		 0.5f,  0.5f,  0.5f, 0.0f, 0.0f, 1.0f,

		-0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
		 0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
		 0.5f, -0.5f,  0.5f, 0.0f, 0.0f, 1.0f,
		 0.5f, -0.5f,  0.5f, 1.0f, 0.0f, 0.0f,
		-0.5f, -0.5f,  0.5f, 0.0f, 1.0f, 0.0f,
		-0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 1.0f,

		-0.5f,  0.5f, -0.5f, 1.0f, 0.0f, 0.0f,
		 0.5f,  0.5f, -0.5f, 0.0f, 1.0f, 0.0f,
		 0.5f,  0.5f,  0.5f, 0.0f, 0.0f, 1.0f,
		 0.5f,  0.5f,  0.5f, 1.0f, 0.0f, 0.0f,
		-0.5f,  0.5f,  0.5f, 0.0f, 1.0f, 0.0f,
		-0.5f,  0.5f, -0.5f, 0.0f, 0.0f, 1.0f,
	};

	m_shader = std::make_unique<Shader>("Content/Shaders/Cube.shader");
	m_shader->Bind();

	glGenVertexArrays(1, &m_vao);
	glGenBuffers(1, &m_vbo);

	glBindVertexArray(m_vao);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (void*)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
}

SceneCamera::~SceneCamera()
{

}

void SceneCamera::OnUpdate(float deltaTime)
{
	if (m_input->IsKeyPressed(SDL_SCANCODE_W))
	{
		m_camera->HandleKeyboardEvents(EMoveForward, deltaTime);
	}
	if (m_input->IsKeyPressed(SDL_SCANCODE_S))
	{
		m_camera->HandleKeyboardEvents(EMoveBackward, deltaTime);
	}
	if (m_input->IsKeyPressed(SDL_SCANCODE_D))
	{
		m_camera->HandleKeyboardEvents(EMoveRight, deltaTime);
	}
	if (m_input->IsKeyPressed(SDL_SCANCODE_A))
	{
		m_camera->HandleKeyboardEvents(EMoveLeft, deltaTime);
	}
}

void SceneCamera::OnRender()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (m_wireframe)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	m_shader->Bind();

	m_view = glm::mat4(1.0f);
	m_proj = glm::mat4(1.0f);
	m_proj = glm::perspective(glm::radians(m_camera->GetZoom()), 1280.0f / 768.0f, 0.1f, 100.0f);
	m_view = m_camera->GetViewMatrix();

	m_shader->SetUniformMat4f("proj", m_proj);
	m_shader->SetUniformMat4f("view", m_view);

	glBindVertexArray(m_vao);
	for (unsigned int i = 0; i < 10; i++)
	{
		m_model = glm::mat4(1.0f);
		m_model = glm::translate(m_model, m_positions[i]);
		float angle = 20.0f * i;
		m_model = glm::rotate(m_model, glm::radians(angle), glm::vec3(1.0f, 0.5f, 0.5f));
		m_shader->SetUniformMat4f("model", m_model);
		glDrawArrays(GL_TRIANGLES, 0, 36);
	}
}

void SceneCamera::OnImGuiRender()
{
	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	ImGui::Checkbox("Wireframe", &m_wireframe);
}

