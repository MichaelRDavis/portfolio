#pragma once

#include <vector>
#include <string>
#include <functional>

class Renderer;

class Scene
{
public:
	Scene() {}
	virtual ~Scene() {}

	virtual void OnUpdate(float deltaTime) {}
	virtual void OnRender() {}
	virtual void OnImGuiRender() {}
};

class SceneMenu : public Scene
{
public:
	SceneMenu(Scene*& currentScene);
	~SceneMenu();

	void OnImGuiRender() override;

	template<typename T>
	void RegisterScene(const std::string& name)
	{
		std::cout << "Register scene: " << name << std::endl;
		m_scenes.push_back(std::make_pair(name, []() {return new T(); }));
	}

private:
	Scene*& m_currentScene;
	std::vector<std::pair<std::string, std::function<Scene*()>>> m_scenes;
};
