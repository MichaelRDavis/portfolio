#pragma once

#include "Scene.h"
#include <gl/glew.h>
#include <glm.hpp>

class Shader;

class SceneGouraud : public Scene
{
public:
	SceneGouraud();
	~SceneGouraud();

	void OnImGuiRender() override;
	void OnRender() override;
	void OnUpdate(float deltaTime) override;

private:
	GLuint m_vbo;
	GLuint m_vao;

	std::unique_ptr<Shader> m_shader;

	glm::mat4 m_model;
	glm::mat4 m_view;
	glm::mat4 m_proj;

	glm::vec3 m_lightPosition;

	GLint m_intensity;
	float m_diffuse;
	float m_specular;
};