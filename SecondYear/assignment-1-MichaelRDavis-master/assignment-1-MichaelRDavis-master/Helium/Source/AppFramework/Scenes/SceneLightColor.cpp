#include "SceneLightColor.h"
#include "Rendering/RenderCore/Shader.h"
#include <imgui.h>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>
#include <SDL.h>

SceneLightColor::SceneLightColor()
{
	glEnable(GL_DEPTH_TEST);

	GLfloat cubeVertices[] = {
		-0.5f, -0.5f, -0.5f,
		 0.5f, -0.5f, -0.5f,
		 0.5f,  0.5f, -0.5f,
		 0.5f,  0.5f, -0.5f,
		-0.5f,  0.5f, -0.5f,
		-0.5f, -0.5f, -0.5f,

		-0.5f, -0.5f,  0.5f,
		 0.5f, -0.5f,  0.5f,
		 0.5f,  0.5f,  0.5f,
		 0.5f,  0.5f,  0.5f,
		-0.5f,  0.5f,  0.5f,
		-0.5f, -0.5f,  0.5f,

		-0.5f,  0.5f,  0.5f,
		-0.5f,  0.5f, -0.5f,
		-0.5f, -0.5f, -0.5f,
		-0.5f, -0.5f, -0.5f,
		-0.5f, -0.5f,  0.5f,
		-0.5f,  0.5f,  0.5f,

		 0.5f,  0.5f,  0.5f,
		 0.5f,  0.5f, -0.5f,
		 0.5f, -0.5f, -0.5f,
		 0.5f, -0.5f, -0.5f,
		 0.5f, -0.5f,  0.5f,
		 0.5f,  0.5f,  0.5f,

		-0.5f, -0.5f, -0.5f,
		 0.5f, -0.5f, -0.5f,
		 0.5f, -0.5f,  0.5f,
		 0.5f, -0.5f,  0.5f,
		-0.5f, -0.5f,  0.5f,
		-0.5f, -0.5f, -0.5f,

		-0.5f,  0.5f, -0.5f,
		 0.5f,  0.5f, -0.5f,
		 0.5f,  0.5f,  0.5f,
		 0.5f,  0.5f,  0.5f,
		-0.5f,  0.5f,  0.5f,
		-0.5f,  0.5f, -0.5f,
	};

	m_lightPosition = glm::vec3(1.2f, 1.0f, 2.0f);

	m_cubeShader = std::make_unique<Shader>("Content/Shader/CubeLight.shader");
	m_lightShader = std::make_unique<Shader>("Content/Shaders/LightColor.shader");

	m_vao = 0;
	m_vbo = 0;
	glGenVertexArrays(1, &m_vao);
	glGenBuffers(1, &m_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
	glBindVertexArray(m_vao);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*)0);
	glEnableVertexAttribArray(0);

	m_lightVAO = 0;
	glGenVertexArrays(1, &m_lightVAO);
	glBindVertexArray(m_lightVAO);
	//glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	//glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*)0);
	//glEnableVertexAttribArray(0);
}

SceneLightColor::~SceneLightColor()
{

}

void SceneLightColor::OnUpdate(float deltaTime)
{

}

void SceneLightColor::OnRender()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_cubeShader->Bind();
	m_cubeShader->SetUniform3f("cubeColor", 1.0f, 0.5f, 0.31f);
	m_cubeShader->SetUniform3f("lightColor", 1.0f, 1.0f, 1.0f);

	m_proj = glm::mat4(1.0f);
	m_proj = glm::perspective(glm::radians(45.0f), 1280.0f / 768.0f, 0.1f, 100.0f);
	m_cubeShader->SetUniformMat4f("proj", m_proj);

	m_view = glm::mat4(1.0f);
	m_view = glm::translate(m_view, glm::vec3(0.0f, 0.0f, -3.0f));
	m_cubeShader->SetUniformMat4f("view", m_view);

	m_model = glm::mat4(1.0f);
	m_model = glm::rotate(m_model, (float)SDL_GetTicks() / 1000 * glm::radians(50.0f), glm::vec3(0.3f, 1.0f, 0.0f));
	m_cubeShader->SetUniformMat4f("model", m_model);

	glBindVertexArray(m_vao);
	glDrawArrays(GL_TRIANGLES, 0, 36);

	m_lightShader->Bind();
	m_lightShader->SetUniformMat4f("proj", m_proj);
	m_lightShader->SetUniformMat4f("view", m_view);

	m_model = glm::mat4(1.0f);
	m_model = glm::translate(m_model, m_lightPosition);
	m_model = glm::scale(m_model, glm::vec3(0.2f));
	m_lightShader->SetUniformMat4f("model", m_model);

	glBindVertexArray(m_lightVAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
}

void SceneLightColor::OnImGuiRender()
{
	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
}
