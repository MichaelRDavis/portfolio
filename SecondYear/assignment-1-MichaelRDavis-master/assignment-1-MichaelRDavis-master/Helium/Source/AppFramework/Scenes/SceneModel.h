#pragma once

#include "Scene.h"

class SceneModel : public Scene
{
public:
	SceneModel();
	~SceneModel();

	void OnRender() override;
	void OnUpdate(float deltaTime) override;
	void OnImGuiRender() override;
};