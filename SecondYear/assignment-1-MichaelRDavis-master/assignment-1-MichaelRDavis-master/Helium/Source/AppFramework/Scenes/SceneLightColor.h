#pragma once

#include "Scene.h"
#include <gl/glew.h>
#include <glm.hpp>

class Shader;

class SceneLightColor : public Scene
{
public:
	SceneLightColor();
	~SceneLightColor();

	void OnUpdate(float deltaTime) override;
	void OnRender() override;
	void OnImGuiRender() override;

private:
	GLuint m_vbo;
	GLuint m_vao;
	GLuint m_lightVAO;
	GLfloat m_lightColor[4];

	glm::mat4 m_proj;
	glm::mat4 m_view;
	glm::mat4 m_model;

	glm::vec3 m_lightPosition;

	std::unique_ptr<Shader> m_lightShader;
	std::unique_ptr<Shader> m_cubeShader;
};