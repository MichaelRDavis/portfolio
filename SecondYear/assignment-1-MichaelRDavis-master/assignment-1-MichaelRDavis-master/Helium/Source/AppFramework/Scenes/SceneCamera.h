#pragma once

#include "Scene.h"
#include <memory>
#include <gl/glew.h>
#include <glm.hpp>

class Shader;
class Camera;
class Input;

class SceneCamera : public Scene
{
public:
	SceneCamera();
	~SceneCamera();

	void OnUpdate(float deltaTime) override;
	void OnRender() override;
	void OnImGuiRender() override;

private:
	GLuint m_vao;
	GLuint m_vbo;

	std::vector<glm::vec3> m_positions;

	glm::mat4 m_view;
	glm::mat4 m_model;
	glm::mat4 m_proj;

	std::unique_ptr<Shader> m_shader;
	std::unique_ptr<Camera> m_camera;
	std::unique_ptr<Input> m_input;

	bool m_wireframe;
};