#pragma once

#include "Scene.h"
#include <gl/glew.h>

class SceneBlinnPhong : public Scene
{
public:
	SceneBlinnPhong();
	~SceneBlinnPhong();

	void OnRender() override;
	void OnUpdate(float deltaTime) override;
	void OnImGuiRender() override;

private:
	GLuint m_color;
	GLuint m_normals;
	GLuint m_diffuseAlbedo;
	GLuint m_spaecularAlbedo;
	GLuint m_specularPower;
};