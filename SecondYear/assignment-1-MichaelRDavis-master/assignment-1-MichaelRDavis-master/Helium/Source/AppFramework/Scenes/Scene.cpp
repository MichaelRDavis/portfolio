#include "Scene.h"
#include <imgui.h>

SceneMenu::SceneMenu(Scene*& currentScene)
	: m_currentScene(currentScene)
{

}

SceneMenu::~SceneMenu()
{

}

void SceneMenu::OnImGuiRender()
{
	for (auto& scene : m_scenes)
	{
		if (ImGui::Button(scene.first.c_str()))
		{
			m_currentScene = scene.second();
		}
	}
}
