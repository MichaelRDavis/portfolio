#pragma once

#include "Scene.h"

class GBuffer;

class SceneDeferredShading : public Scene
{
public:
	SceneDeferredShading();
	~SceneDeferredShading();

	void OnRender() override;
	void OnUpdate(float deltaTime) override;
	void OnImGuiRender() override;

private:
	std::unique_ptr<GBuffer> m_buffer;
};