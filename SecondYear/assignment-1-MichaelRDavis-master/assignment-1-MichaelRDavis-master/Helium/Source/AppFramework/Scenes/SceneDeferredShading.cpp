#include "SceneDeferredShading.h"
#include "Rendering/RenderCore/GBuffer.h"

SceneDeferredShading::SceneDeferredShading()
{
	m_buffer = std::make_unique<GBuffer>();
}

SceneDeferredShading::~SceneDeferredShading()
{

}

void SceneDeferredShading::OnRender()
{
	// Raw values used for now, need to get viewport size.
	m_buffer->Initialize(1920, 1080);
}

void SceneDeferredShading::OnUpdate(float deltaTime)
{

}

void SceneDeferredShading::OnImGuiRender()
{

}

