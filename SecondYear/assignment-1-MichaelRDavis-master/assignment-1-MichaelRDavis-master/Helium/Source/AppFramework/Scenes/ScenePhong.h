#pragma once

#include "Scene.h"
#include <gl/glew.h>
#include <glm.hpp>

class Shader;

class ScenePhong : public Scene
{
public:
	ScenePhong();
	~ScenePhong();

	void OnUpdate(float deltaTime) override;
	void OnRender() override;
	void OnImGuiRender() override;

private:
	GLuint m_vao;
	GLuint m_vbo;

	std::unique_ptr<Shader> m_shader;

	glm::mat4 m_view;
	glm::mat4 m_model;
	glm::mat4 m_proj;
};