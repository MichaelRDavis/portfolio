#pragma once

#include "Scene.h"
#include <GL/glew.h>
#include <glm.hpp>

class Shader;

struct DirectionaLight
{
	glm::vec4 color;
	float intensity;
};

class SceneCubeLight : public Scene
{
public:
	SceneCubeLight();
	~SceneCubeLight();

	void OnUpdate(float deltaTime) override;
	void OnRender() override;
	void OnImGuiRender() override;

private:
	GLuint m_vao;
	GLuint m_vbo;

	std::unique_ptr<Shader> m_shader;

	glm::mat4 m_view;
	glm::mat4 m_model;
	glm::mat4 m_proj;

	float m_lightColor[3];
	float m_intensity;

	bool m_wireframe;
};
