#include "App.h"
#include "Window.h"
#include "Components/Input.h"
#include "Scenes/Scene.h"
#include <imgui.h>
#include <imgui_impl_sdl.h>
#include <imgui_impl_opengl3.h>

App::App()
{
	m_currentScene =  nullptr;
	m_menu = new SceneMenu(m_currentScene);
	m_currentScene = m_menu;
	m_isMenuActive = true;
}

App::~App()
{

}

void App::AppInit()
{
	m_Window = std::make_unique<Window>();
	if (m_Window)
	{
		m_Window->InitWindow();
	}

	ImGui::CreateContext();
	ImGui_ImplSDL2_InitForOpenGL(m_Window->GetWindow(), m_Window->GetContext());
	ImGui_ImplOpenGL3_Init();
	ImGui::StyleColorsDark();

	m_Input = std::make_unique<Input>();
}

void App::AppExit()
{
	delete m_currentScene;
	if (m_currentScene != m_menu)
	{
		delete m_menu;
	}

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplSDL2_Shutdown();
	ImGui::DestroyContext();

	SDL_GL_DeleteContext(m_Window->GetContext());
	m_Window->DestroyWindow();
	SDL_Quit();
}

void App::Update(Renderer* ren)
{
	SDL_PollEvent(&m_Event);

	if (m_Window)
	{
		m_Window->PollWindowEvents(m_Event);
		m_Window->Clear();

		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplSDL2_NewFrame(m_Window->GetWindow());
		ImGui::NewFrame();

		if (m_currentScene)
		{
			m_currentScene->OnUpdate((float)SDL_GetTicks()/1000);
			m_currentScene->OnRender();
			ImGui::Begin("Scenes", &m_isMenuActive, ImGuiWindowFlags_MenuBar);
			if (m_currentScene != m_menu && ImGui::Button("Back"))
			{
				delete m_currentScene;
				m_currentScene = m_menu;
			}
			m_currentScene->OnImGuiRender();
			ImGui::End();
		}

		ImGui::Render();
		SDL_GL_MakeCurrent(m_Window->GetWindow(), m_Window->GetContext());
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
		m_Window->SwapBuffers();
	}

	if (m_Input)
	{
		if(m_Input->IsKeyPressed(SDL_SCANCODE_ESCAPE) || (m_Event.type == SDL_QUIT))
		{
			AppExit();
		}
	}
}
