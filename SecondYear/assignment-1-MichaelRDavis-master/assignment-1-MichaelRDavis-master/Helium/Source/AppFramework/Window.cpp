#include "Window.h"
#include <iostream>
#include <GL/glew.h>

Window::Window()
{
	m_Window = nullptr;
	m_Surface = nullptr;
	m_isWindowed = true;
	m_IsVSyncEnabled = true;
}

Window::~Window()
{
	
}

void Window::InitWindow()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		std::cout << "SDL_ERROR: " << SDL_GetError() << std::endl;
	}

	if (SDL_GetDesktopDisplayMode(0, &m_displayMode) != 0)
	{
		std::cout << "SDL_ERROR: " << SDL_GetError() << std::endl;
	}

	m_WindowWidth = m_displayMode.w;
	m_WindowHeight = m_displayMode.h;

	unsigned int windowFlags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_MAXIMIZED;

	m_Window = SDL_CreateWindow("Helium", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, m_WindowWidth, m_WindowWidth, windowFlags);
	if (m_Window == nullptr)
	{
		std::cout << "SDL_ERROR: " << SDL_GetError() << std::endl;
	}

	m_ContextHandle = SDL_GL_CreateContext(m_Window);

	const GLenum error = glewInit();
	if (error != GLEW_OK)
	{
		std::cout << "GLEW_ERROR: " << glewGetErrorString(error) << std::endl;
		return;
	}

	if (m_IsVSyncEnabled)
	{
		SDL_GL_SetSwapInterval(1);
	}
	else
	{
		SDL_GL_SetSwapInterval(0);
	}


	if (m_isFullscreen)
	{
		SDL_SetWindowSize(m_Window, m_WindowWidth, m_WindowHeight);
		SDL_SetWindowPosition(m_Window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
		glViewport(0, 0, m_WindowWidth, m_WindowHeight);
	}
	else if (m_isWindowed)
	{
		SDL_SetWindowBordered(m_Window, SDL_TRUE);
		SDL_SetWindowSize(m_Window, m_WindowWidth, m_WindowHeight);
		SDL_SetWindowPosition(m_Window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
		glViewport(0, 0, m_WindowWidth, m_WindowHeight);
	}

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

	std::cout << "OpenGL Version: " << glGetString(GL_VERSION) << std::endl;
	std::cout << "GPU Vendor: " << glGetString(GL_VENDOR) << std::endl;
	std::cout << "GPU: " << glGetString(GL_RENDERER) << std::endl;

	int NumAttributes;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &NumAttributes);
	std::cout << "Number of Vertex Attributes supported: " << NumAttributes << std::endl;

	glEnable(GL_DEPTH_TEST);
}

void Window::RenderWindow()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	SDL_GL_SwapWindow(m_Window);
}

void Window::Clear()
{
	glClear(GL_COLOR_BUFFER_BIT);
}

void Window::SwapBuffers()
{
	SDL_GL_SwapWindow(m_Window);
}

void Window::DestroyWindow()
{
	SDL_DestroyWindow(m_Window);
	SDL_Quit();
}

void Window::PollWindowEvents(SDL_Event& Event)
{
	switch (Event.window.event)
	{
	case SDL_WINDOWEVENT_SIZE_CHANGED:
		break;
	case SDL_WINDOWEVENT_ENTER:
		break;
	case SDL_WINDOWEVENT_LEAVE:
		break;
	case SDL_WINDOWEVENT_FOCUS_GAINED:
		break;
	case SDL_WINDOWEVENT_FOCUS_LOST:
		break;
	case SDL_WINDOWEVENT_MINIMIZED:
		break;
	case SDL_WINDOWEVENT_MAXIMIZED:
		break;
	case SDL_WINDOWEVENT_RESTORED:
		break;
	}
}
