#pragma once

#include <SDL.h>
#include <memory>
#include <string>

#include "Scenes/Scene.h"

class Window;
class Input;
class Scene;
class SceneMenu;

/**
 * Generic application.
 */
class App
{
public:
	/** Default constructor. */
	App();

	/** Default destructor. */
	~App();

	/** Initializes the application. */
	void AppInit();

	/** Shuts down the application. */
	void AppExit();

	/** Update the application, called once per frame. */
	void Update(Renderer* ren);

	/** Add a scene to the scene menu. */
	template<typename T>
	void AddScene(const std::string& sceneName)
	{
		m_menu->RegisterScene<T>(sceneName);
	}

private:
	/** Smart pointer to the window. */
	std::unique_ptr<Window> m_Window;

	/** Smart pointer to the input interface. */
	std::unique_ptr<Input> m_Input;

	/** Pointer the current scene. */
	Scene* m_currentScene;

	/** Pointer to the scene menu. */
	SceneMenu* m_menu;

	/** Application event handler. */
	SDL_Event m_Event;

	/** Flag for closing the scene menu. */
	bool m_isMenuActive;
};
