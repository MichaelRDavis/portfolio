#include "Camera.h"
#include "Input.h"
#include <gtc/matrix_transform.hpp>

Camera::Camera()
{
	m_input = std::make_unique<Input>();

	m_positon = glm::vec3(0.0f, 0.0f, 0.0f);
	m_upVector = glm::vec3(0.0f, 1.0f, 0.0f);
	m_forwardVector = glm::vec3(0.0f, 0.0f, -1.0f);

	m_yaw = CAMERA_YAW;
	m_pitch = CAMERA_PITCH;
	m_movementSpeed = CAMERA_SPEED;
	m_mouseSensitivity = CAMERA_SENSIVITY;
	m_zoom = CAMERA_ZOOM;
}

Camera::~Camera()
{

}

void Camera::HandleKeyboardEvents(EMovementMode movement, float deltaTime)
{
	float vecloity = m_movementSpeed * deltaTime;
	if (movement == EMoveForward)
	{
		m_positon += m_forwardVector * vecloity;
	}
	if (movement == EMoveBackward)
	{
		m_positon -= m_forwardVector * vecloity;
	}
	if (movement == EMoveRight)
	{
		m_positon += m_rightVector * vecloity;
	}
	if (movement == EMoveLeft)
	{
		m_positon -= m_rightVector * vecloity;
	}
}

void Camera::HandleMouseEvents(float x, float y, bool bPitch /*= false*/)
{

}

glm::mat4 Camera::GetViewMatrix() const
{
	return glm::lookAt(m_positon, m_positon + m_forwardVector, m_upVector);
}

void Camera::CalculateCameraVectors()
{
	glm::vec3 frontVec;
	frontVec.x = cos(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
}

void Camera::Update()
{

}

void Camera::SetFOV(float newFOV)
{
	m_FOV = newFOV;
}

void Camera::SetAspectRatio(float newAspectRatio)
{
	m_aspectRatio = newAspectRatio;
}

void Camera::SetRange(float newNear, float newFar)
{
	m_near = newNear;
	m_far = newFar;
}
