#pragma once

#include <glm.hpp>
#include <memory>

class Input;

constexpr float CAMERA_YAW{ -90.0f };
constexpr float CAMERA_PITCH{ 0.0f };
constexpr float CAMERA_SPEED{ 0.5f };
constexpr float CAMERA_SENSIVITY{ 0.01f };
constexpr float CAMERA_ZOOM{ 45.0f };

/** Camera movement modes. */
enum EMovementMode
{
	EMoveForward,
	EMoveBackward,
	EMoveLeft,
	EMoveRight,
};

/**
 * Abstract camera class.
 */
class Camera
{
public:
	/** Default Camera constructor. */
	Camera();

	/** Default Camera destructor. */
	~Camera();

	/** Handle camera keyboard input */
	void HandleKeyboardEvents(EMovementMode movement, float deltaTime);

	/** Handle camera mouse events */
	void HandleMouseEvents(float x, float y, bool bPitch = false);

	/** Returns the view matrix. */
	glm::mat4 GetViewMatrix() const;

	/** Calculate camera vectors. */
	void CalculateCameraVectors();

	/** Called once per frame. */
	void Update();

	/** Set camera FOV. */
	void SetFOV(float newFOV);

	/** Set camera aspect ratio. */
	void SetAspectRatio(float newAspectRatio);

	/** Set camera range. */
	void SetRange(float newNear, float newFar);

	/** Returns forward vector. */
	inline glm::vec3 GetForwardVector() const { return m_forwardVector; }

	/** Returns right vector. */
	inline glm::vec3 GetRightVector() const { return m_rightVector; }

	/** Returns up vector. */
	inline glm::vec3 GetUpVector() const { return m_upVector; }

	/** Returns projection. */
	inline glm::mat4 GetProjection() const { return m_projection; }

	/** Returns view. */
	inline glm::mat4 GetView() const { return m_view; }

	/** Returns FOV. */
	inline float GetFOV() const { return m_FOV; }

	/** Returns aspect ratio. */
	inline float GetAspectRation() const { return m_aspectRatio; }

	/** Returns far */
	inline float GetFar() const { return m_far; }

	/** Returns clip. */
	inline float GetClip() const { return m_near; }

	/** Returns zoom */
	inline float GetZoom() const { return m_zoom; }

private:
	/** Input component. */
	std::unique_ptr<Input> m_input;

	/** Camera position in 3D space. */
	glm::vec3 m_positon;

	/** Camera forward vector. */
	glm::vec3 m_forwardVector;

	/** Camera right vector. */
	glm::vec3 m_rightVector;

	/** Camera up vector. */
	glm::vec3 m_upVector;

	/** Camera projection matrix. */
	glm::mat4 m_projection;

	/** Camera view matrix. */
	glm::mat4 m_view;

	/** Field of view of the camera in degrees. */
	float m_FOV;

	/** Aspect ratio of the camera (Width/Height). */
	float m_aspectRatio;

	/** Far plane of the orthographic view. */
	float m_far;

	/** Near plane of the orthographic view. */
	float m_near;

	/** Camera yaw angle. */
	float m_yaw;

	/** Camera pitch angle */
	float m_pitch;

	/** Camera movement speed. */
	float m_movementSpeed;

	/** Camera mouse sensitivity. */
	float m_mouseSensitivity;

	/** Camera zoom speed. */
	float m_zoom;
};
