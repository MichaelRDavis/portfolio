#include "Input.h"

Input::Input()
{
	Init();
}

Input::~Input()
{

}

void Input::Init()
{
	m_CurrentKeyState = SDL_GetKeyboardState(nullptr);
}

EKeyState Input::GetKeyState(uint32_t KeyCode) const
{
	if (m_KeyStates[KeyCode] == 0)
	{
		if (m_CurrentKeyState[KeyCode] == 0)
		{
			return EKeyState::ENone;
		}
		else
		{
			return EKeyState::EPressed;
		}
	}
	else
	{
		if (m_CurrentKeyState[KeyCode] == 0)
		{
			return EKeyState::EReleased;
		}
		else
		{
			return EKeyState::EHeld;
		}
	}
}

bool Input::IsKeyPressed(uint32_t KeyCode) const
{
	return m_CurrentKeyState[KeyCode];
}

bool Input::IsKeyReleased(uint32_t KeyCode) const
{
	return m_CurrentKeyState[KeyCode];
}
