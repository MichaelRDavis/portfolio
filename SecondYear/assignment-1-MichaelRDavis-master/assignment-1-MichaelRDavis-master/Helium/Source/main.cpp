#include <memory>
#include <iostream>
#include <imgui.h>
#include "AppFramework/App.h"
#include "Rendering/RenderCore/Renderer.h"
#include "AppFramework/Scenes/SceneClearColor.h"
#include "AppFramework/Scenes/SceneCube.h"
#include "AppFramework/Scenes/SceneTexturedCube.h"
#include "AppFramework/Scenes/SceneInstancing.h"
#include "AppFramework/Scenes/SceneLightColor.h"
#include "AppFramework/Scenes/SceneCamera.h"
#include "AppFramework/Scenes/SceneDeferredShading.h"
#include "AppFramework/Scenes/SceneCubeLight.h"
#include "AppFramework/Scenes/SceneGouraud.h"
#include "AppFramework/Scenes/ScenePhong.h"

int main(int argc, char* argv[])
{
	std::unique_ptr<App> renderingApp = std::make_unique<App>();
	std::unique_ptr<Renderer> renderer = std::make_unique<Renderer>();

	if (renderingApp)
	{
		try
		{
			renderingApp->AppInit();
			renderingApp->AddScene<SceneClearColor>("Clear Color");
			renderingApp->AddScene<SceneCube>("Cube");
			renderingApp->AddScene<SceneTexturedCube>("Textured Cube");
			renderingApp->AddScene<SceneCubeLight>("Ambient Lighting");
			renderingApp->AddScene<SceneInstancing>("Instanced Cubes");
			renderingApp->AddScene<SceneLightColor>("Light Color");
			renderingApp->AddScene<SceneCamera>("Camera");
			renderingApp->AddScene<SceneGouraud>("Gouraud Shading");
			renderingApp->AddScene<ScenePhong>("Phong Shading");
			renderingApp->AddScene<SceneDeferredShading>("Deferred Shading");

			while (true)
			{
				renderingApp->Update(renderer.get());
			}
		}
		catch (const std::exception& exception)
		{
			std::cout << exception.what() << std::endl;
		}
		renderingApp->AppExit();
	}

	return 0;
}