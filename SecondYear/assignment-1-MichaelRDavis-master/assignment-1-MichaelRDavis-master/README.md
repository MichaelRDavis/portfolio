# 3D Rendering Application 
All graphical techniques are attempted not fully implemented. 

# Bibliography
* Yan Cherkov, YC. (2018) OpenGL. Available at: [Link](https://www.youtube.com/playlist?list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2) (Accessed on: 10/03/19)
* Joey de Vries, JV. (2019) LearnOpenGL. Available at: [Link](https://learnopengl.com/) (Accessed on: 12/03/19)
* Sellers, Wright, Haemel, GS, RSW, NH. (2016) OpenGL SuperBible. Seventh Edition. United States. Addison-Wesley. 
