#pragma once

/**
 * Base class for all nodes.
 */
class BTNode
{
public:
	/** Default BTNode constructor. */
	BTNode();

	/** Default BTNode destructor. */
	virtual ~BTNode();
};