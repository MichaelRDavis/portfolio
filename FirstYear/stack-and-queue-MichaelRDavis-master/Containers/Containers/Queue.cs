﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Containers
{
    // Node to store a queue
    public class QueueNode<T>
    {
        public T data;
        public QueueNode<T> next;

        public QueueNode(T inData)
        {
            data = inData;
            next = null;
        }

        // Return the data stored at the node
        public T GetData()
        {
            return data;
        }
    }

    public class Queue<T>
    {
        public QueueNode<T> head;
        public QueueNode<T> tail;
        public Int64 size;

        // Constructor 
        public Queue()
        {
            head = null;
            tail = null;
            size = 0;
        }

        // Return true if queue is empty
        public bool IsEmpty()
        {
            return head == null;
        }

        // Return data stored at the head node
        public T Peek(QueueNode<T> inHead)
        {
            if (inHead == null)
                return default(T);

            return inHead.data;
        }

        // Add a new node at the tail, move tail to the next node
        public void Enqueue(T data)
        {
            QueueNode<T> tempNode = new QueueNode<T>(data);

            if(tail == null)
            {
                head = tempNode;
                tail = tempNode;
                return;
            }

            tail.next = tempNode;
            tail = tempNode;
            size++;
        }

        // Remove the head node, move head to the next node
        public QueueNode<T> Dequeue()
        {
            if (IsEmpty())
                return null;

            QueueNode<T> tempNode = new QueueNode<T>(head.GetData());
            head = head.next;

            if (head == null)
                tail = null;

            size--;
            return tempNode;
        }

        // Empty the queue 
        public void Clear()
        {
            while (!IsEmpty())
                Dequeue();
        }
    }
}
