﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Containers
{
    public class Stack<T>
    {
        public Node<T> head;
        public UInt64 size;

        public Stack()
        {
            head = null;
            size = 0;
        }

        // Add a element to the top of the stack
        public void Push(T inData)
        {
            Node<T> newNode = new Node<T>(inData, head);
            newNode.data = inData;
            newNode.next = head;
            head = newNode;
            size++;
        }

        // Remove element from the top of the stack
        public T Pop()
        {
            if (this == null)
            {
                throw new UnderflowException("Error: You can't remove elements from an empty stack.");
            }

            T tempData = head.data;
            head = head.next;
            size--;
            return tempData;
        }

        // Return the size of the stack
        public UInt64 GetSize()
        {
            return size;
        }

        // Return the data stored at the head of the stack
        public T Peek()
        {
            if (head == null)
            {
                throw new UnderflowException("Error: You can't access data from an empty stack.");
            }

            return head.data;
        }

        // Return true if the stack is empty
        public bool IsEmpty()
        {
            return head == null;
        }

        // Clear the stack of all elements
        public void Clear()
        {
            while (!IsEmpty())
                Pop();
        }
    }
}
