﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Containers
{
    public static class StudentProgram
    {
        public class Flat
        {
            // Create an ID number 
            public Int32 ID;

            // The quality of the flat
            public float qualityScore;

            // Keep track of how many years left in occupancy
            public Int32 occupancy;

            // Check to see if flat is currently occupied
            public bool bIsOccupied;

            public Flat()
            {
                ID = 0;
                qualityScore = 0.0f;
                occupancy = 0;
                bIsOccupied = false;
            }
        }

        public class Student
        {
            // Create an ID number 
            public Int32 ID;

            // Students desired flat quality 
            public float quality;

            public Student()
            {
                ID = 0;
                quality = 0.0f;
            }

        }

        // This a badly written function :( it would be nice if we could split it up into several distinct functions.
        public static void Run()
        {
            // Generate random numbers
            Random rand = new Random();

            string numStudents = "";
            string numFlats = "";
            Int32 value = 0;

            while (true)
            {
                // Ask the user for the number of students
                Console.WriteLine("Insert a numerical value for the number of students to house.\n");
                numStudents = Console.ReadLine();
                if (Int32.TryParse(numStudents, out value))
                {
                    Console.WriteLine("Number of Students: " + numStudents);
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid input: Please try again.");
                    continue;
                }
            }

            while (true)
            {
                // Ask the user for the number flats
                Console.WriteLine("Insert a numerical value for the number of flats.\n");
                numFlats = Console.ReadLine();
                if (Int32.TryParse(numFlats, out value))
                {
                    Console.WriteLine("Number of Flats: " + numFlats);
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid input: Please try again.");
                    continue;
                }
            }

            // Create a list of flats 
            LinkedList<Flat> flatList = new LinkedList<Flat>();
            for (Int32 i = 0; i < Int32.Parse(numFlats); i++)
            {
                Flat flat = new Flat();
                flatList.PushFront(flat);
            }

            // Create queue of students
            Queue<Student> studentQueue = new Queue<Student>();
            for (Int32 i = 0; i <= Int32.Parse(numStudents); i++)
            {
                Student student = new Student();
                // Add student to queue
                studentQueue.Enqueue(student);
            }

            // Generate random values for the quality of the flat
            for (UInt32 ctr = 1; ctr <= flatList.GetSize(); ctr++)
            {
                flatList.head.data.qualityScore = rand.Next(0, 2);
                flatList.head.data.occupancy = rand.Next(1, 4);
                flatList.head.data.ID++;
                Console.WriteLine("Flat ID: " + flatList.head.data.ID + " | Flat Quality: " + flatList.head.data.qualityScore + " | Flat Occupancy: " + flatList.head.data.occupancy);
            }

            // Generate random values for the quality of the flat the student desires
            for (UInt32 ctr = 1; ctr <= studentQueue.size; ctr++)
            {
                studentQueue.head.GetData().quality = rand.Next(0, 2);
                studentQueue.head.GetData().ID++;
                Console.WriteLine("Student ID: " + studentQueue.head.GetData().ID + " | Desired Flat Quality: " + studentQueue.head.GetData().quality);
            }

            // Keep track of the number of years
            Int32 years = 1;
            // Keep track of the number of flats occupied this year
            Int32 count = 0;
            // While there are students to house
            while(!studentQueue.IsEmpty())
            {
                Console.WriteLine("Current academic year: " + years);
                Console.WriteLine("Students housed this year: " + studentQueue.size);

                QueueNode<Student> currentStudentNode = studentQueue.head;
                if(currentStudentNode == null)
                {
                    return;
                }

                Node<Flat> currentFlatNode = flatList.head;
                if(currentFlatNode == null)
                {
                    return;
                }

                while (currentStudentNode != null && !studentQueue.IsEmpty())
                {
                    // Remove student from the queue 
                    studentQueue.Dequeue();

                    bool foundFlat = false;

                    // New loop to iterate over the flats
                    while ((currentFlatNode != null) && (!currentFlatNode.data.bIsOccupied) && !studentQueue.IsEmpty())
                    {
                        // If the student can accept the flat 
                        if (!currentFlatNode.data.bIsOccupied && currentStudentNode.data.quality >= currentFlatNode.data.qualityScore)
                        {
                            // Set the flat to be occupied
                            currentFlatNode.data.bIsOccupied = true;
                            currentFlatNode.data.occupancy = rand.Next(1, 4);
                            // Increment the number of flats occupied this year
                            count++;
                            Console.WriteLine("Student can be housed.");
                            foundFlat = true;
                        }

                        currentFlatNode = currentFlatNode.next;
                    }

                    currentStudentNode = currentStudentNode.next;

                    // If the student cannot except the flat add the student to the back queue
                    if (!foundFlat)
                    {
                        Student student = studentQueue.Peek(currentStudentNode);
                        studentQueue.Enqueue(student);
                        currentStudentNode.data.quality *= 0.9f;
                        Console.WriteLine("Student can't be housed.");
                        
                    }
                    break;
                }

                Console.WriteLine("Flats occupied this year: " + count);
                Console.WriteLine("Flats left unoccupied this year: " );

                if(currentFlatNode == null)
                {
                    return;
                }

                // Decrement the occupancy for each flat
                while (currentFlatNode.data.bIsOccupied)
                {
                    // If the flat occupancy is zero then set the flat to be unoccupied 
                    if(currentFlatNode.data.occupancy == 0)
                    {
                        currentFlatNode.data.bIsOccupied = false;
                    }
                    else
                    {
                        currentFlatNode.data.occupancy--;
                    }

                    currentFlatNode = currentFlatNode.next;
                }

                years++;
                break;
            }

            Console.ReadKey();
        }
    }
}
