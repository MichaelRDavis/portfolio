﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Containers
{
    public class Node<T>
    {
        public T data;
        public Node<T> next;

        public Node(T inData, Node<T> inNext)
        {
            data = inData;
            next = inNext;
        }
    }

    public class LinkedList<T>
    {
        public Node<T> head;
        public UInt64 size;

        public LinkedList()
        {
            head = null;
            size = 0;
        }

        // Return the top of the list
        public T Peek()
        {
            if (!Empty())
                return head.data;
            else
                return default(T);
        }

        // Return true if list is empty
        public bool Empty()
        {
            return head == null;
        }

        // Retunrs the size of our list
        public UInt64 GetSize()
        {
            return size;
        }

        // Add element at the end 
        public void PushBack(T item)
        {
            Node<T> newNode = new Node<T>(item, head);

            if(head == null)
            {
                head = new Node<T>(item, head);
            }

            size++;
        }

        // Insert element at beginning
        public void PushFront(T item)
        {
            Node<T> newNode = new Node<T>(item, head);
            newNode.next = head;
            head = newNode;
            size++;
        }

        // Delete the last element in the list
        public Node<T> PopBack()
        {
            Node<T> temp = head;
            while (temp.next != null)
                temp = temp.next;
            size--;

            return temp;
        }

        // Delete the first element in the list
        public Node<T> PopFront()
        {
            if (Empty())
                return null;

            Node<T> temp = head;
            head = head.next;
            size--;
            return temp;
        }

        // Clear the content of the list
        public void Clear()
        {
            Node<T> temp = head;
            while(temp != null)
            {
                Node<T> temp2 = temp.next;
                temp.next = null;
                temp = temp2;
            }
            head = null;
            size = 0;
        }
    }

}
