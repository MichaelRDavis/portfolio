﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Containers
{
    class Program
    {
        public static void Main()
        {
            Console.Title = "Stack and Queue";
            Console.ForegroundColor = ConsoleColor.Cyan;

            Console.WriteLine("What program do you want to run.\n");
            Console.WriteLine("Press the '1' key on the keyboard for stack program.");
            Console.WriteLine("Press the '2' key on the keyboard for queue program.");
            Console.WriteLine("Press the 'esc' key on the keyboard to exit the application.");

            while (true)
            {
                ConsoleKeyInfo input = Console.ReadKey();

                if (input.Key.Equals(ConsoleKey.D1))
                {
                    PalindromeProgram.Run();
                    break;
                }
                else if(input.Key.Equals(ConsoleKey.D2))
                {
                    StudentProgram.Run();
                    break;
                }
                else if(input.Key.Equals(ConsoleKey.Escape))
                {
                    Console.WriteLine("Goodbye!");
                    Environment.Exit(0);
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid option please try again.");
                    continue;
                }
            }
        }
    }
}
