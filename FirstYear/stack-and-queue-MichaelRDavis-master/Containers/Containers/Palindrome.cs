﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Containers
{
    public static class PalindromeProgram
    {
        // Remove punctuation from a particular string
        public static string RemovePunc(string inString)
        {
            string tempStr = string.Empty;
            tempStr = System.Text.RegularExpressions.Regex.Replace(inString, @"[^\w]", string.Empty);
            return tempStr;
        }

        // Read in a file
        public static string[] ReadFile()
        {
            string[] file = System.IO.File.ReadAllLines(@"palindromes.txt");
            return file;
        }

        // Check a file for palindromes 
        public static void CheckForPalindromes()
        {
            string[] lines = ReadFile();
            foreach (string line in lines)
            {
                string tempStr = RemovePunc(line);
                if(IsPalindrome(tempStr.ToLower()))
                {
                    Console.WriteLine("\t" + line);
                }
            }
        }

        // Check to see if a particular string is a palindrome called from within the check palindromes function
        public static bool IsPalindrome(string inString)
        {
            Int32 stringSize = inString.Length;
            Stack<char> stack = new Stack<char>();
            for(Int32 i = 0; i < stringSize / 2; i++)
            {
                stack.Push(inString[i]);     
            }

            for(Int32 i = (stringSize + 1) / 2; i < stringSize; i++)
            {
                if(stack.Peek() != inString[i])
                {
                    return false;
                }

                stack.Pop();
            }

            return true;
        }

        // Run the console app
        public static void Run()
        {
            CheckForPalindromes();
        }
    }  
}