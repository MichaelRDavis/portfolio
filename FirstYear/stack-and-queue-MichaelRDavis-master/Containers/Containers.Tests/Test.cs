﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Containers.Tests
{
    [TestFixture(typeof(Stack<int>))]
    public class TestStack<TStack> where TStack : Stack<int>, new()
    {
        private Stack<int> stk;

        [SetUp]
        public void CreateStack()
        {
            stk = new TStack();
        }

        [Test]
        public void CheckStackIsEmpty()
        {
            bool bIsEmpty = stk.IsEmpty();
            Assert.That(bIsEmpty);
        }

        [Test]
        public void CheckStackPeek()
        {
            stk.Push(7);
            stk.Push(14);
            int head = stk.Peek();
            Assert.AreEqual(14, head);
        }

        [Test]
        public void CheckCanClearStack()
        {
            stk.Push(6);
            stk.Push(12);
            Assert.AreEqual(2, stk.size);
            stk.Clear();
            Assert.AreEqual(0, stk.size);
            bool bIsEmpty = stk.IsEmpty();
            Assert.That(bIsEmpty);
        }

        [Test]
        public void CheckSizeOfStack()
        {
            stk.Push(100);
            int size = (int)stk.GetSize();
            Assert.AreEqual(1, size);
        }

        [Test]
        public void CanPushToStack()
        {
            stk.Push(1);
            stk.Push(2);
            stk.Push(3);
            Assert.AreEqual(3, stk.size);
        }

        [Test]
        public void CanPopFromStack()
        {
            stk.Push(7);
            stk.Push(14);
            int value = stk.Pop();
            Assert.AreEqual(14, value);
        }
    }

    [TestFixture(typeof(LinkedList<int>))]
    public class TestLinkedList<TList> where TList : LinkedList<int>, new()
    {
        private LinkedList<int> list;

        [SetUp]
        public void CreateList()
        {
            list = new TList();
        }

        [Test]
        public void CheckListIsEmpty()
        {
            bool bIsEmpty = list.Empty();
            Assert.That(bIsEmpty);
        }

        [Test]
        public void CheckListClear()
        {
            list.PushBack(100);
            list.PushFront(200);
            Assert.AreEqual(2, list.size);
            list.Clear();
            Assert.That(list.head == null);
        }

        [Test]
        public void CheckListSize()
        {
            list.PushBack(1);
            list.PushBack(2);
            list.PushFront(3);
            list.PushFront(4);
            int size = (int)list.GetSize();
            Assert.AreEqual(4, size);
        }

        [Test]
        public void CanPushToBackOfList()
        {
            list.PushBack(10);
            list.PushBack(20);
            list.PushBack(30);
            Assert.AreEqual(3, list.size);
        }

        [Test]
        public void CanPushToFrontOfList()
        {
            list.PushFront(2);
            list.PushFront(4);
            list.PushFront(6);
            Assert.AreEqual(3, list.size);
        }

        [Test]
        public void CanPopFromFrontOfList()
        {
            list.PushFront(100);
            int value = list.PopFront().data;
            Assert.AreEqual(100, value);
        }

        [Test]
        public void CanPopFromBackOfList()
        {
            list.PushBack(200);
            int value = list.PopBack().data;
            Assert.AreEqual(200, value);
        }
    }

    [TestFixture(typeof(Queue<string>))]
    public class TestQueue<TQueue> where TQueue : Queue<string>, new()
    {
        private Queue<string> q;

        [SetUp]
        public void CreateQueue()
        {
            q = new TQueue();
        }

        [Test]
        public void CanEqueue()
        {
            string name = "Chris";
            q.Enqueue(name);
            Assert.AreEqual(name, q.head.data);
        }

        [Test]
        public void CanDequeue()
        {
            string name = "Steve";
            q.Enqueue(name);
            string retrieveName = q.Dequeue().GetData();
            Assert.AreEqual(retrieveName, name);
            Assert.That(q.head == null);
        }

        [Test]
        public void CheckQueueIsEmpty()
        {
            bool bIsEmpty = q.IsEmpty();
            Assert.That(bIsEmpty);
        }

        [Test]
        public void CheckQueuePeek()
        {
            string personOne = "Rob";
            string personTwo = "Eddie";
            q.Enqueue(personOne);
            q.Enqueue(personTwo);
            string retrieveHead = q.Peek(q.head);
            Assert.AreEqual(personOne, retrieveHead);
        }

        [Test]
        public void CheckQueueClear()
        {
            string personOne = "Rob";
            string personTwo = "Eddie";
            string personThree = "Steve";
            string personFour = "Chris";

            q.Enqueue(personOne);
            q.Enqueue(personTwo);
            q.Enqueue(personThree);
            q.Enqueue(personFour);

            q.Clear();

            Assert.That(q.head == null);
        }
    }
}
