﻿using NUnit.Framework;

namespace HuffmanCode.Test
{
    [TestFixture]
    class HeapTest
    {
        Heap<int> heapTest = Heap<int>.CreateHeap();

        [Test]
        public void TestHeapInsert()
        {
            heapTest.Insert(5);
            int num = heapTest.FindMin();
            Assert.That(num == 5);
        }

        [Test]
        public void TestHeapRemove()
        {
            heapTest.Insert(25);
            int num = heapTest.ExtractMin();
            Assert.That(num == 25);
        }

        [Test]
        public void TestHeapEmpty()
        {
            Heap<int> heap = Heap<int>.CreateHeap();
            Assert.That(heap.IsEmpty());
        }

        [Test]
        public void TestFindMin()
        {
            heapTest.Insert(2);
            heapTest.Insert(4);
            heapTest.Insert(5);
            int num = heapTest.FindMin();
            Assert.That(num == 5);
        }

        [Test]
        public void TestExtractMin()
        {
            heapTest.Insert(4);
            heapTest.Insert(8);
            int num = heapTest.ExtractMin();

        }
    }
}
