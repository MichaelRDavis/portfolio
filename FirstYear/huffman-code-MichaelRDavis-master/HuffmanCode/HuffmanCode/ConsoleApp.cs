﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Media;
using System.Diagnostics;

namespace HuffmanCode
{
    public class ConsoleApp
    { 
        public ConsoleApp()
        {
            Init();
        }

        ~ConsoleApp()
        {
            DeInit();
        }

        void Init()
        {
            Console.Title = "Huffman Code Generator";
            Console.ForegroundColor = ConsoleColor.DarkCyan;
        }

        void DeInit()
        {

        }

        // Print the Huffman codes 
        public void PrintCodes(string strInput, List<Int32> inEcoding, HuffmanTree<char> inCodes)
        {
            var chars = new HashSet<char>(strInput);
            foreach (char c in chars)
            {
                inEcoding = inCodes.Encode(c);
                Console.Write("{0}:     ", c);
                foreach (int bit in inEcoding)
                {
                    Console.Write("{0}", bit);
                }
                Console.WriteLine();
            }
        }

        // Ask the user for input
        public void PrintIntro()
        {
            Console.WriteLine("\nHuffman Code Generator.\n");
            Console.WriteLine("\t1: Encode a String.");
            Console.WriteLine("\t2: Encode a File.\n");
            Console.WriteLine("\t3: Encode a String to a Binary File.");
            Console.WriteLine("\t4: Decode a Binary File to a string.\n");
            Console.WriteLine("\tEsc: Press escape to exit the console application.");
        }

        // Notify user if encoding and decoding was successful, Debug Only
        [Conditional("DEBUG")]
        public void IsSuccessful(string inStr, string strOriginal)
        {
            if (inStr == strOriginal)
            {
                Console.WriteLine("Encoding/Decoding worked");
            }
            else
            {
                Console.WriteLine("Encoding/Decoding failed");
                SystemSounds.Exclamation.Play();
            }
        }
    }
}
