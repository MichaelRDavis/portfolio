﻿using System;
using System.IO;
using System.Media;
using System.Collections.Generic;

namespace HuffmanCode
{
    class Program
    {
        // Program loop
        static void Main(string[] args)
        {
            ConsoleApp app = new ConsoleApp();
            ConsoleKeyInfo input;

            // Application loop
            do
            {
                app.PrintIntro();

                // Read keyboard input from the user
                input = Console.ReadKey();
                if (input.Key == ConsoleKey.D1)
                {
                    try
                    {
                        // Get the user input
                        Console.WriteLine("Enter the string you want to encode");
                        string strInput = Console.ReadLine();

                        // Encode string input 
                        var code = new HuffmanTree<char>();
                        code.CreateHuffmanTree(strInput);
                        List<Int32> encoding = code.Encode(strInput);
                        List<char> decoding = code.Decode(encoding);
                        var encodedString = new string(decoding.ToArray());

                        app.IsSuccessful(encodedString, strInput);

                        // Print encoded string
                        app.PrintCodes(strInput, encoding, code);
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine(exception.Message);
                        SystemSounds.Exclamation.Play();
                    }
                }
                else if (input.Key == ConsoleKey.D2)
                {
                    try
                    {
                        // Get the file name form the user 
                        Console.WriteLine("Enter the file name you want to encode");
                        string strFileName = Console.ReadLine();

                        // Open the file, enable read access
                        FileStream fileStream = File.Open(strFileName, FileMode.Open, FileAccess.Read);

                        // Read the contents of the file
                        string strContents;
                        StreamReader streamReader = new StreamReader(fileStream);
                        strContents = streamReader.ReadToEnd();

                        // Encode string input 
                        var code = new HuffmanTree<char>();
                        code.CreateHuffmanTree(strContents);
                        List<Int32> encoding = code.Encode(strContents);
                        List<char> decoding = code.Decode(encoding);
                        var encodedString = new string(decoding.ToArray());

                        app.IsSuccessful(encodedString, strContents);

                        // Print encoded string
                        app.PrintCodes(strContents, encoding, code);
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine(exception.Message);
                        SystemSounds.Exclamation.Play();
                    }
                }
                else if (input.Key == ConsoleKey.D3)
                {
                    BinaryWriter writer;

                    try
                    {
                        Console.WriteLine("Enter the string you want to write to a Binary File");
                        string strInput = Console.ReadLine();

                        var code = new HuffmanTree<char>();
                        code.CreateHuffmanTree(strInput);
                        List<Int32> encoding = code.Encode(strInput);
                        List<char> decoding = code.Decode(encoding);
                        var encodedString = new string(decoding.ToArray());

                        try
                        {
                            writer = new BinaryWriter(new FileStream("Encoding", FileMode.Create));
                        }
                        catch (IOException exception)
                        {
                            Console.WriteLine(exception.Message + "Error: Cannot create binary file!.\n");
                            SystemSounds.Exclamation.Play();
                            continue;
                        }

                        try
                        {
                            byte[] bytes = new byte[encoding.Count * sizeof(Int32)];
                            writer.Write(bytes);

                            char[] ch = decoding.ToArray();
                            writer.Write(ch);
                        }
                        catch (IOException exception)
                        {
                            Console.WriteLine(exception.Message + "Error: Cannot write to binary file!.\n");
                            SystemSounds.Exclamation.Play();
                        }
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine(exception.Message);
                        SystemSounds.Exclamation.Play();
                    }

                }
                else if(input.Key == ConsoleKey.D4)
                {
                    BinaryReader reader;

                    try
                    {
                        Console.WriteLine("Enter the name of the Binary File you want to decode");
                        string strFileName = Console.ReadLine();

                        try
                        {
                            reader = new BinaryReader(new FileStream(strFileName, FileMode.Open, FileAccess.Read));

                            byte[] encoding = new byte[0];
                            foreach(byte bit in encoding)
                            {
                                reader.ReadBytes(bit);
                            }

                            char[] decoding = new char[0];
                            foreach(char ch in decoding)
                            {
                                reader.ReadChars(ch);
                            }

                            List<int> codes = new List<int>();
                            foreach(byte bit in encoding)
                            {
                                codes.Add(bit);
                            }
                        }
                        catch(IOException exception)
                        {
                            Console.WriteLine(exception.Message);
                            SystemSounds.Exclamation.Play();
                        }
                    }
                    catch(Exception exception)
                    {
                        Console.WriteLine(exception.Message);
                        SystemSounds.Exclamation.Play();
                    }
                }

            } while (input.Key != ConsoleKey.Escape);
        }
    }
}
