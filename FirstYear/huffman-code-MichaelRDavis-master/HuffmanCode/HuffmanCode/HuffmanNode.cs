﻿
// Credit goes to Rosseta Code - https://rosettacode.org/wiki/Huffman_coding#C.23

using System;

namespace HuffmanCode
{
    // HUffman Node, a node that implements an object comparison interface
    public class HuffmanNode<T> : IComparable
    {
        private HuffmanNode<T> leftNode;
        private HuffmanNode<T> rightNode;
        private HuffmanNode<T> parentNode;

        private T key;
        private bool bIsLeaf;
        private bool bIsZero;
        private double frequency;

        // Default Ctor
        public HuffmanNode()
        {
            leftNode = null;
            rightNode = null;
            parentNode = null;
            bIsLeaf = false;
        }

        // Ctor for adding two nodes 
        public HuffmanNode(HuffmanNode<T> inLeftNode, HuffmanNode<T> inRightNode)
        {
            leftNode = inLeftNode;
            rightNode = inRightNode;
            frequency = inLeftNode.frequency + rightNode.frequency;
            leftNode.bIsZero = true;
            rightNode.bIsZero = false;
            leftNode.parentNode = this;
            rightNode.parentNode = this;
            bIsLeaf = false;
        }

        public HuffmanNode(double inFreq, T inData)
        {
            frequency = inFreq;
            leftNode = null;
            rightNode = null;
            parentNode = null;
            key = inData;
            bIsLeaf = true;
        }

        ~HuffmanNode()
        {
            leftNode = null;
            rightNode = null;
            parentNode = null;
        }

        // Returns left node
        public HuffmanNode<T> GetLeftNode()
        {
            return leftNode;
        }

        // Returns right node
        public HuffmanNode<T> GetRightNode()
        {
            return rightNode;
        }

        // Returns parent node
        public HuffmanNode<T> GetParentNode()
        {
            return parentNode;
        }

        // Returns data stored at the node
        public T GetData()
        {
            return key;
        }

        // Returns true if node is a leaf
        public bool IsLeaf()
        {
            return bIsLeaf;
        }

        // Return true if node is zero
        public bool IsZero()
        {
            return bIsZero;
        }

        // Set is zero to a new value
        public void SetIsZero(bool bNewIsZero)
        {
            bIsZero = bNewIsZero;
        }

        // Retunrs 0 or 1 
        public int IsBit()
        {
           return bIsZero ? 0 : 1;
        }

        // Retunrs true if node is a root
        public bool IsRootNode()
        {
            return parentNode == null;
        }

        // Retunrs the frequency
        public double GetFrequency()
        {
            return frequency;
        }

        // Override CompareTo 
        public Int32 CompareTo(object inObject)
        {
            return -frequency.CompareTo(((HuffmanNode<T>)inObject).frequency);
        }
    }
}
