﻿
// Credit goes to Rosseta Code - https://rosettacode.org/wiki/Huffman_coding#C.23

using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HuffmanCode
{
    public class HuffmanTree<T> where T : IComparable
    {
        private Dictionary<T, HuffmanNode<T>> leafKeys;                                             // Stores the frequency of nodes for each character
        private HuffmanNode<T> rootNode;                                                            // The root of the Huffman tree
        private Dictionary<T, Int32> counts;                                                        // Counts the frequency of each character in the string
        private Heap<HuffmanNode<T>> heap;                                                          // Heap of Huffman nodes 

        // Huffman tree Ctor
        public HuffmanTree()
        {
            // Init variables
            counts  = new Dictionary<T, Int32>();              
            leafKeys = new Dictionary<T, HuffmanNode<T>>();
            rootNode = new HuffmanNode<T>();  
            heap = Heap<HuffmanNode<T>>.CreateHeap();         
        }

        // Builds the Huffman tree when called
        public void CreateHuffmanTree(IEnumerable<T> inData)
        {
            Int32 valueCount = 0;

            // For each character type in the string
            foreach (T data in inData)
            {
                // Check if character if character exists in dictionary
                if (!counts.ContainsKey(data))
                {
                    // If it doesn't already exist set its count to zero
                    counts[data] = 0;
                }
                // Increase the count of the reoccurring characters
                counts[data]++;
                valueCount++;
            }

            // For every character that has a key
            foreach (T data in counts.Keys)
            {
                var node = new HuffmanNode<T>((double)counts[data] / valueCount, data);         // Create a new node
                heap.Insert(node);                                                              // Insert the new node into the heap
                leafKeys[data] = node;                                                          // Insert node into the dictionary for each character
            }

            // Iterate while the heap size is greater than 1
            while (heap.Size() > 1)
            {
                // Extract the two min frequency nodes from the heap
                HuffmanNode<T> leftNode = heap.ExtractMin();
                HuffmanNode<T> rightNode = heap.ExtractMin();
                var parentNode = new HuffmanNode<T>(leftNode, rightNode);   // Create a new parent node which will add the two min frequencies together
                heap.Insert(parentNode);                                    // Insert the new parent node back into the heap
            }

            // ExtractMin the root node and set it's size to zero
            rootNode = heap.ExtractMin();
            rootNode.SetIsZero(false);

            // If Debug, print the tree
            PrintHuffmanTree(rootNode);
        }

        // Encode a character into a list of bits, returns the list of bits
        public List<Int32> Encode(T inData)
        {
            var newValue = new List<Int32>();
            Encode(inData, newValue);
            return newValue;
        }

        // Encode characters into bits
        private void Encode(T inData, List<Int32> inEncoding)
        {
            if(!leafKeys.ContainsKey(inData))
            {
                throw new ArgumentException("Error: Invalid string!");
            }

            HuffmanNode<T> currentNode = leafKeys[inData];
            var codes = new List<Int32>();
            while(!currentNode.IsRootNode())
            {
                codes.Add(currentNode.IsBit());
                currentNode = currentNode.GetParentNode();
            }

            codes.Reverse();
            inEncoding.AddRange(codes);
        }

        // Encode hash table, used to print Huffman codes. Returns the list of bits
        public List<Int32> Encode(IEnumerable<T> inValues)
        {
            var tempValue = new List<Int32>();

            foreach(T value in inValues)
            {
                Encode(value, tempValue);
            }
            return tempValue;
        }

        // Decode a list of bits into a character, returns the character
        private T Decode(List<Int32> inBitStr, ref Int32 inPos)
        {
            HuffmanNode<T> currentNode = rootNode;
            while(!currentNode.IsLeaf())
            {
                if(inPos > inBitStr.Count)
                {
                    throw new ArgumentException("Error: Invalid bits!");
                }

                currentNode = inBitStr[inPos++] == 0 ? currentNode.GetLeftNode() : currentNode.GetRightNode();
            }
            return currentNode.GetData();
        }

        // Decode a list of bits into a list of characters, returns the list of characters
        public List<T> Decode(List<Int32> inBitStr)
        {
            Int32 pos = 0;
            var tempValue = new List<T>();

            while(pos != inBitStr.Count)
            {
                tempValue.Add(Decode(inBitStr, ref pos));
            }
            return tempValue;
        }

        // Print the contents of the Huffman tree, Debug only.
        [Conditional("DEBUG")]
        public void PrintHuffmanTree(HuffmanNode<T> node)
        {
            if (node.GetLeftNode() == null && node.GetRightNode() == null)
            {
                Console.WriteLine(node.GetData());
            }
            else if (node.GetLeftNode() == null)
                PrintHuffmanTree(node.GetRightNode());
            else if (node.GetRightNode() == null)
                PrintHuffmanTree(node.GetLeftNode());
            else
            {
                PrintHuffmanTree(node.GetLeftNode());
                PrintHuffmanTree(node.GetRightNode());
            }
        }

        // Returns leaf keys stored in dictionary
        public Dictionary<T, HuffmanNode<T>> GetLeafKeys()
        {
            return leafKeys;
        }
    }
}
