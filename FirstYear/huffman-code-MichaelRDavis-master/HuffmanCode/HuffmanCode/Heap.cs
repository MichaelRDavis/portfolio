﻿
// Credit goes to Rosseta Code - https://rosettacode.org/wiki/Huffman_coding#C.23

using System;
using System.Collections.Generic;

namespace HuffmanCode
{
    // Heap, a tree based structure (inherits from object comparison interface)
    public class Heap<Key> where Key : IComparable
    { 
        private List<Key> list;   // Generic list is used to add and remove keys from the heap

        // Heap Ctor
        public Heap()
        {
            list = new List<Key>();
        }

        // Heap Dtor
        ~Heap()
        {
            list = null;
        }

        // Create a empty heap
        public static Heap<Key> CreateHeap()
        {
            return new Heap<Key>();
        }

        // Returns the size of the heap
        public Int32 Size()
        {
            return list.Count;
        }

        // Retunrs true if the heap is empty
        public bool IsEmpty()
        {
            return list.Count == 0;
        }

        // Insert a value into the heap
        public void Insert(Key inVal)
        {
            list.Add(inVal);
            InsertAt(list.Count - 1, inVal);
            UpHeap(list.Count - 1);
        }

        // Get the key stored at the top of the heap
        public Key FindMin()
        {
            if(list.Count == 0)
            {
                throw new IndexOutOfRangeException("Error: Peeking at an empty heap!");
            }

            return list[0];
        }

        // Extract an element form the heap
       public Key ExtractMin()
       {
            if(list.Count == 0)
            {
                throw new IndexOutOfRangeException("Error: Removing from an empty Heap!");
            }

            Key topVal = list[0];
            
            InsertAt(0, list[list.Count - 1]);
            list.RemoveAt(list.Count - 1);
            DownHeap(0);
            return topVal;
        }

        // Insert a value into the heap at a specified index
        private void InsertAt(Int32 inIndex, Key inVal)
        {
            list[inIndex] = inVal;
        }

        // Return true if there is a right node
        private bool IsRightNode(Int32 inIndex)
        {
            return GetRightIndex(inIndex) < list.Count;
        }

        // Return true if there is a left branch
        private bool IsLeftNode(Int32 inIndex)
        {
            return GetLeftIndex(inIndex) < list.Count;
        }

        // Returns the parent node index value 
        private Int32 GetParentIndex(Int32 inIndex)
        {
            return (inIndex - 1) / 2;
        }

        // Returns the left branch index
        private Int32 GetLeftIndex(Int32 inIndex)
        {
            return 2 * inIndex + 1;
        }

        // Returns the right branch index
        private Int32 GetRightIndex(Int32 inIndex)
        {
            return 2 * (inIndex + 1);
        }

        // Returns the array key at a specified index
        private Key GetArrayData(Int32 inIndex)
        {
            return list[inIndex];
        }

        // Returns the key stored at the parent node at a specified index
        private Key GetParentKey(Int32 inIndex)
        {
            return list[GetParentIndex(inIndex)];
        }

        // Returns the key stored at the left node at a specified index
        private Key GetLeftKey(Int32 inIndex)
        {
            return list[GetLeftIndex(inIndex)];
        }

        // Returns the key stored at the right node at a specified index
        private Key GetRightKey(Int32 inIndex)
        {
            return list[GetRightIndex(inIndex)];
        }

        // Swap two heap indexes
        private void Swap(Int32 firstIndex, Int32 secondIndex)
        {
            Key value = GetArrayData(firstIndex);
            InsertAt(firstIndex, list[secondIndex]);
            InsertAt(secondIndex, value);
        }

        // Move up the heap
        private void UpHeap(Int32 inIndex)
        {
            while(inIndex > 0 && GetArrayData(inIndex).CompareTo(GetParentKey(inIndex)) > 0)
            {
                Swap(inIndex, GetParentIndex(inIndex));
                inIndex = GetParentIndex(inIndex);
            }
        }

        // Move a down the heap
        private void DownHeap(Int32 index)
        {
            while(index >= 0)
            {
                Int32 count = -1;

                if(IsRightNode(index) && GetRightKey(index).CompareTo(GetArrayData(index)) > 0)
                {
                    count = GetLeftKey(index).CompareTo(GetRightKey(index)) < 0 ? GetRightIndex(index) : GetLeftIndex(index);
                }
                else if(IsLeftNode(index) && GetLeftKey(index).CompareTo(GetArrayData(index)) > 0)
                {
                    count = GetLeftIndex(index);
                }

                if (count >= 0 && count < list.Count)
                {
                    Swap(index, count);
                }

                index = count;
            }
        }

        // Store the heap in an array
        public Key[] ToArray()
        {
            return list.ToArray();
        }

        // Merge two heaps
        public Heap<Key> Merge(Heap<Key> firstHeap, Heap<Key> secondHeap)
        {
            Heap<Key> mergedHeap = new Heap<Key>();
            for (Int32 i = 0; i < firstHeap.Size(); i++)
            {
                
            }

            return null;
        }
    }
}
