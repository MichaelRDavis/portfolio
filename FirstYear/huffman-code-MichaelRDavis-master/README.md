## Bibliography
[Geek for Geeks - Binary Heap](https://www.geeksforgeeks.org/binary-heap/)

[Geeks for Geeks - Huffman Coding](https://www.geeksforgeeks.org/greedy-algorithms-set-3-huffman-coding/)

[Rosetta Code - Huffman Coding](https://rosettacode.org/wiki/Huffman_coding#C.23)

[Wikipedia - Huffman Coding](https://en.wikipedia.org/wiki/Huffman_coding)
