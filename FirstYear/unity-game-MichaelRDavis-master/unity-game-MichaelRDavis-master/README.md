# Ted vs. Zombies
Ted vs. Zombies is a 2D action platformer game where you play has Ted, from the movie Ted! Your goal is to find your way home whilst avoiding the hordes of zombies that want to eat your teddy bear stuffing!

## Introduction
Welcome to the GitHub repository for the Ted vs. Zombies project. Ted vs. Zombies is made in Unity and written in C#.

## Prerequisites
1. Unity 2017.3
2. Visual Studio 2017
3. Git Client

## Controls
* D - Move Right
* A - Move Left
* Space Bar - Jump
* Left Shift - Jetpack
* Ctrl - Baseball Bat
* Alt - Throw Grenade

## Credits
* [2D Platformer](https://assetstore.unity.com/packages/essentials/tutorial-projects/2d-platformer-11228)
* [Ted Assets](https://gameartpartners.com/downloads/teddy-bear-ted/)
* [City Assets](https://gameartpartners.com/downloads/the-city-platform-set/)
* [Zombie Assets](https://gameartpartners.com/downloads/zombie-pack-2d-game-character-sprite/)
* [Zombie Sound](http://soundbible.com/1035-Zombie-Moan.html)
* [Grenade Asset](https://gameartpartners.com/downloads/totally-free-goodies/)
