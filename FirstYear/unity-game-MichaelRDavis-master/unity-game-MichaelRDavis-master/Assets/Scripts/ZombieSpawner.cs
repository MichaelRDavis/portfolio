﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSpawner : MonoBehaviour
{
    public GameObject zombies;            // Array of Zombie prefabs to spawn from.
    public GameObject spawnPoints;        // Array spawn points choose from

    // Called when the game starts
    public void Start()
    {
        Spawn();
    }

    // Spawn zombies 
    public void Spawn()
    {
        GameObject zombie = Instantiate(zombies, spawnPoints.transform);
    }
}
