﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IDamageInterface
{
    [HideInInspector]
    public bool facingRight = true;     // A boolean to check player sprite facing direction
    [HideInInspector]
    public bool jump = false;           // A boolean flag to trigger jump state
    [HideInInspector]
    public bool isFalling = false;

    public float moveForce = 365.0f;    // Player move speed
    public float maxSpeed = 100.0f;     // Max player move speed
    public float jumpForce = 1000.0f;   // Force applied when player jumps

    public Transform groundCheck;       // A transform object to detect ground collision
    private bool grounded = false;      // Flag to see if player is moving along ground
    private Animator anim;              // A reference to the player animation object

    public int grenadeCount = 4;            // Max number of grenades player can carry 
    public GameObject grenade;              // A reference to grenade prefab
    public Transform throwOffset;           // Location of grenade prefab spawn   
    private bool grenadeThrown = false;     // Flag to see if grenade has been thrown

    public int health = 0;              // The player's current health
    public int maxhealth = 100;         // The player's max health
    public int SuperHealth = 200;       // The players super health, activated when power up is collected
    private bool isDead = false;        // Flag to see if layer is dead, i.e. health < 0
    private bool isHit = false;         // Flag to see if player is hit, i.e. has taken damage
    public float deathTimer = 10.0f;    // Clean up timer to remove player from game scene

    public float jetpackForce = 10.0f;      // Force applied when Jetpack is activated 
    private bool jetpackActive = false;     // Flag to see if Jetpack is active

    public int meleeDamage = 25;        // Damage applied on melee collision
    private bool canMelee = true;       // Flag to see if player can currently perform a melee attack
    public bool isMelee = false;        // Flag when player is performing melee
    public GameObject meleeComponent;   // Reference to melee component;

    public bool isPowerupActive = false;    // Flag to see if power up is currently active

    private Rigidbody2D rigidbody2D;            // Cached reference to RigidBody2D Component

    // Called to initialize components and variables when game starts
    private void Awake()
    {
        health = maxhealth;

        groundCheck = transform.Find("groundCheck");
        throwOffset = transform.Find("grenadeOffset");

        anim = GetComponent<Animator>();
        rigidbody2D = GetComponent<Rigidbody2D>();

        meleeComponent = GetComponentInChildren<CircleCollider2D>().gameObject;
        meleeComponent.gameObject.SetActive(false);
    }

    // Called once per a frame
    private void Update()
    {
        HealthManager.playerHealth = health;
        GrenadeManager.grenadeCount = grenadeCount;

        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        if(Input.GetButtonDown("Jump") && grounded)
        {
            jump = true;
        }

        if(Input.GetButtonDown("Fire2") && !grenadeThrown && grenadeCount > 0)
        {
            grenadeCount--;
            Instantiate(grenade, throwOffset.position, throwOffset.rotation);
            anim.SetTrigger("Grenade");
        }

        if(Input.GetButtonDown("Fire1") && canMelee && !isMelee)
        { 
            isMelee = true;
            meleeComponent.gameObject.SetActive(true);
            anim.SetTrigger("Melee");
        }

        if(isPowerupActive)
        {

        }
    }

    // Called every fixed framerate frame
    private void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");

        anim.SetFloat("Speed", Mathf.Abs(h));

        if(h * GetComponent<Rigidbody2D>().velocity.x < maxSpeed)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * moveForce);
        }

        if(Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > maxSpeed)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x) * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }

        if(h > 0 && facingRight)
        {
            Flip();
        }
        else if(h < 0 && !facingRight)
        {
            Flip();
        }

        if(jump)
        {
            anim.SetTrigger("Jump");

            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));

            jump = false;
        }

        jetpackActive = Input.GetButton("Fire3");
        if(jetpackActive)
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jetpackForce));
            anim.SetTrigger("Jetpack");
        }

        //if(rigidbody2D.velocity.y < -0.1)
        //{
        //    isFalling = true;
        //}
        //else
        //{
        //    isFalling = false;
        //}

        //if(isFalling)
        //{
        //    anim.SetTrigger("Parachute");
        //}

        //if(isMelee)
        //{
        //    anim.SetTrigger("Melee");
        //    isMelee = false;
        //    meleeComponent.SetActive(false);
        //}
    }

    public void EndMeleeSwing()
    {
        isMelee = false;
        meleeComponent.SetActive(false);
    }

    // Flip player sprite towards move direction
    private void Flip()
    {
        facingRight = !facingRight;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    // Called when player is taking damage
    public void TakeDamage(int damage)
    {
        if(damage > 0)
        {
            health -= damage;

            if(health <= 0 && isDead)
            {
                isDead = true;
                Die();
            }
            else if(isHit)
            {
                isHit = true;
                Hit();
            }
        }
    }

    // Called when player is dead, i.e. when health < 0
    private void Die()
    {
        if(isDead)
        {
            anim.SetTrigger("Death");

            health = Mathf.Min(0, health);
            DestroyObject(gameObject, deathTimer);
        }
    }

    // Called when player has taken damage
    private void Hit()
    {
        if(isHit)
        {
            anim.SetTrigger("Hit");
        }
    }
}
