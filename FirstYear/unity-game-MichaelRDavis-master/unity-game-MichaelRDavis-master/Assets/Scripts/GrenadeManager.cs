﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GrenadeManager : MonoBehaviour
{
    public static int grenadeCount;

    Text text;

    private void Awake()
    {
        text = GetComponent<Text>();
        grenadeCount = 0;
    }

    private void Update()
    {
        text.text = "Grenade: " + grenadeCount;   
    }
}
