﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour
{
    public static int playerHealth;

    Text text;

    private void Awake()
    {
        text = GetComponent<Text>();
        playerHealth = 0;
    }

    private void Update()
    {
        text.text = "Health: " + playerHealth;
    }
}
