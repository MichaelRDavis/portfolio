﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pauser : MonoBehaviour
{
    private bool isPaused = false;

    public CanvasGroup canvasGroup;

    public void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    private void Update()
    {
        if(Input.GetKeyUp(KeyCode.P))
        {
            isPaused = !isPaused;
        }

        if (isPaused)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;
    }

    void HideButton()
    {
        if(!isPaused)
        {
            canvasGroup.alpha = 0.0f;
            canvasGroup.blocksRaycasts = false;
        }
    }

    void ShowButton()
    {
        if(isPaused)
        {
            canvasGroup.alpha = 1.0f;
            canvasGroup.blocksRaycasts = true;
        }
    }
}
