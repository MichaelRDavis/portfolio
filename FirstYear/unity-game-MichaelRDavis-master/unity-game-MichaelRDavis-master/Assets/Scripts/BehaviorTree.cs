﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TreeNode
{
    public enum EStatus
    {
        INVALID,
        SUCCESS,
        FAILURE,
        RUNNING,
    }

    public TreeNode()
    {
        eStatus = EStatus.INVALID;
    }

    public EStatus Tick()
    {
        if (eStatus != EStatus.RUNNING)
        {
            Init();
        }

        eStatus = Tick();

        if (eStatus != EStatus.RUNNING)
        {
            Terminate(eStatus);
        }

        return eStatus;
    }

    protected EStatus eStatus;

    public abstract EStatus Update();
    public abstract void Init();
    public abstract void Terminate(EStatus eStatus);
    public bool IsSuccess() { return eStatus == EStatus.SUCCESS; }
    public bool IsFailure() { return eStatus == EStatus.FAILURE; }
    public bool IsRunning() { return eStatus == EStatus.RUNNING; }
    public bool IsTerminated() { return IsSuccess() || IsFailure(); }
    public void Reset() { eStatus = EStatus.INVALID; }
}

public abstract class LeafNode : TreeNode
{

}

public class BehaviorTree 
{
    public BehaviorTree()
    {

    }

    protected TreeNode node;
}
