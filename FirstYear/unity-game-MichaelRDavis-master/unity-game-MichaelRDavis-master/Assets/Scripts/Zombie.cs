﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour, IDamageInterface
{
    [HideInInspector]
    private bool bFacingRight = false;      // Flag to check facing direction, used to flip sprite

    public float moveSpeed = 0.01f;    // Zombie move speed
    private Vector3 currentPos;         // Zombie's current position in world space
    private int facingDirection;        // Zombie's facing direction
    public float maxMoveDist = 15.0f;
    public float minMoveDist = 7.5f; 

    public int health = 50;             // Zombie health
    private bool bIsDead = false;       // Flag to see if zombie is dead

    public int damage = 5;              // Zombie collision damage
    public int meleeDamage = 15;        // Zombie melee damage 

    public int killScore = 100;         // Score awarded to player when zombie is killed

    private Animator anim;                  // A cached reference to the zombie animator component 
    public ZombieAI zombieAI;               // A cached reference to the zombie AI script, i.e. the zombies brain
    public BoxCollider2D meleeComponent;    // A reference to the child melee collision component

    public GameObject spawnPickups;         // Pickup game object to spawn on death

    public AudioClip deathSound;            // Sound to play on death

    // Called when zombie is instantiated
    private void Awake()
    {
        anim = GetComponent<Animator>();
        zombieAI = GetComponent<ZombieAI>();
    }

    // Called on begin play
    private void Start()
    {
        // Play zombie spawn animation
        anim.SetTrigger("Spawn");

        // Init movement on game start
        currentPos = transform.position;
        facingDirection = -1;
        maxMoveDist = transform.position.x + 4;
        minMoveDist = transform.position.x - 3.5f;
    }

    // Called every frame
    private void Update()
    {
        if(!bIsDead)
        {
            switch (facingDirection)
            {
                case -1:
                    if (transform.position.x > minMoveDist)
                    {
                       // Debug.Log("x > minMoveDist");
                        GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
                        if(bFacingRight)
                        {
                            Flip();
                        }
                    }
                    else
                    {
                        facingDirection = 1;
                    }
                    break;
                case 1:
                    if (transform.position.x < maxMoveDist)
                    {
                        //Debug.Log("x < maxMoveDist");
                        GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
                        if(bFacingRight)
                        {
                            Flip();
                        }
                    }
                    else
                    {
                        facingDirection = -1;
                    }
                    break;
            }
        }
    }

    private void Flip()
    {
        bFacingRight = !bFacingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }

    // Called when objects overlap with zombie collider
    private void OnCollisionEnter2D(Collision2D collision)
    {
        IDamageInterface damageInterface = collision.gameObject.GetComponent<IDamageInterface>();
        if(damageInterface != null)
        {
            damageInterface.TakeDamage(damage);
        }
    }

    // Called when zombie takes damage 
    public void TakeDamage(int damage)
    {
        if(damage > 0)
        {
            Debug.Log("Ouch");
            health -= damage;
            if(health <= 0 && !bIsDead)
            {
                bIsDead = true;
                Die();
            }
        }
    }

    // Called on death
    public void PlayDeathSound()
    {
        if(deathSound != null)
        {
           AudioSource.PlayClipAtPoint(deathSound, transform.position);
        }
    }

    // Called on death to spawn pickups for player
    public void SpawnPickup()
    {
        if(spawnPickups != null)
        {
            Instantiate(spawnPickups, transform.position, transform.rotation);
        }
    }

    // Called when zombie attacks 
    public void Attack()
    {
        anim.SetTrigger("Attack");

        meleeComponent = GetComponentInChildren<BoxCollider2D>();
        if(meleeComponent.tag == "Player")
        {
            IDamageInterface damageInterface = meleeComponent.gameObject.GetComponent<IDamageInterface>();
            if(damageInterface != null)
            {
                damageInterface.TakeDamage(meleeDamage);
            }
        }
    }

    // Called when zombie dies, i.e. when health < 0
    private void Die()
    {
        if(bIsDead)
        {
            anim.SetTrigger("Death");

            PlayDeathSound();
            SpawnPickup();

            ScoreManager.playerScore += killScore;
        }

        Destroy(gameObject, 1.5f);
    }
}
