﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMovement : MonoBehaviour
{
    public Transform targetLocation;
    public float angle = 45.0f;
    public float gravity = 9.8f;

    public Transform projectileLocation;
    private Transform trajectory;

    private void Awake()
    {
        trajectory = transform;
    }

    private void Start()
    {
        StartCoroutine(MoveProjectie());
    }

    IEnumerator MoveProjectie()
    {
        yield return new WaitForSeconds(1.5f);

        projectileLocation.position = trajectory.position + new Vector3(0, 0, 0);

        float targetDist = Vector3.Distance(projectileLocation.position, targetLocation.position);
        float projVelocity = targetDist / (Mathf.Sin(2 * angle * Mathf.Deg2Rad) / gravity);
        float velocityX = Mathf.Sqrt(projVelocity) * Mathf.Cos(angle * Mathf.Deg2Rad);
        float velocityY = Mathf.Sqrt(projVelocity) * Mathf.Sin(angle * Mathf.Deg2Rad);
        float duration = targetDist / velocityX;

        projectileLocation.rotation = Quaternion.LookRotation(targetLocation.position - projectileLocation.position);

        float elapsedTime = 0;
        while(elapsedTime < duration)
        {
            projectileLocation.Translate(0, (velocityY - (gravity * elapsedTime)) * Time.deltaTime, velocityX * Time.deltaTime);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }
}
