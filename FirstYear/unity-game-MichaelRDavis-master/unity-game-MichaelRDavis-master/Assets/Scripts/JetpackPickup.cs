﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JetpackPickup : Pickup
{
    public int fuelRestoreAmount = 100;

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);
    }
}
