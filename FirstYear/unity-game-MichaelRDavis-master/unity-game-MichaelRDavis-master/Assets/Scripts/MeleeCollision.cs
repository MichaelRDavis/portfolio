﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeCollision : MonoBehaviour
{
    public Player owner;

    public void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("Called");
        IDamageInterface damage = collider.gameObject.GetComponent<Zombie>();
        if(damage != null)
        {
            if (owner.isMelee)
            {
                Debug.Log("Ouch");
                damage.TakeDamage(owner.meleeDamage);
            }
        }
    }
}
