﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static int playerScore;          // The player's current score

    Text text;                               // A reference to the text object

    // Called on initialization
    private void Awake()
    {
        // Get the text component
        text = GetComponent<Text>();

        playerScore = 0;        // Init score to zero 
    }

    private void Update()
    {
        text.text = "Score: " + playerScore;
    }
}
