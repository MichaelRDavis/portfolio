﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraComponent : MonoBehaviour
{
    public GameObject player;
    private Vector3 cameraOffset;

    private void Start()
    {
        if(player != null)
        {
            cameraOffset = transform.position - player.transform.position;
        }
    }

    private void LateUpdate()
    {
        if(player != null)
        {
            transform.position = player.transform.position + cameraOffset;
        }
    }
}