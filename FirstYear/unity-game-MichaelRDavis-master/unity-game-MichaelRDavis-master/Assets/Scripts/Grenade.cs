﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{
    public float fuseTime = 3.0f;           // Time delay that triggers grenade explosion 
    private float count = 0;                // Internal counter for timer
    private bool exploded = false;          // Flag to see if grenade has exploded 
    public float blastRadius = 5.0f;        // Radius of grenade explosion
    public float grenadeForce = 150.0f;     // Force applied on grenade explosion
    public int damage = 75;                 // Damage applied when grenade explodes

    public GameObject explosionFX;          // A reference to the explosion prefab
    public GameObject projMovement;         // Grenade movement trajectory

    // Called on instantiation
    private void Awake()
    {
        count = fuseTime;
    }

    // Called every frame
    private void Update()
    {
        count -= Time.deltaTime;
        if(count <= 0 && !exploded)
        {
            Explode();
            exploded = true;
        }
    }

    // Called when grenade explodes
    private void Explode()
    {
        GameObject localFX;
        localFX = Instantiate(explosionFX, transform.position, transform.rotation);

        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, blastRadius);
        foreach(Collider2D nearbyObject in colliders)
        {
            Rigidbody2D rb2d = nearbyObject.GetComponent<Rigidbody2D>();
            if(rb2d != null)
            {
                Vector3 deltaPos = rb2d.transform.position - transform.position;
                Vector3 force = deltaPos.normalized * grenadeForce;
                rb2d.AddForce(force, ForceMode2D.Impulse);
                IDamageInterface damageInterface = nearbyObject.gameObject.GetComponent<IDamageInterface>();
                if(damageInterface != null)
                {
                    damageInterface.TakeDamage(damage);
                }
            }
        }

        Destroy(gameObject);
    }
}
