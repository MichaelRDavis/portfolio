﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    public GameObject owner;

    public GUIStyle empty;
    public GUIStyle full;

    private int currentStatus;

    Vector2 pos = new Vector2(10, 50);
    Vector2 size = new Vector2(250, 50);

    public Texture2D emptyTex;
    public Texture2D fullTex;

    private void OnGUI()
    {
        GUI.BeginGroup(new Rect(pos.x, pos.y, size.x, size.y), emptyTex, empty);
        GUI.Box(new Rect(pos.x, pos.y, size.x, size.y), fullTex, full);

        GUI.BeginGroup(new Rect(0, 0, size.x * currentStatus, size.y));
        GUI.Box(new Rect(0, 0, size.x, size.y), fullTex, full);

        GUI.EndGroup();
        GUI.EndGroup();
    }

    private void Update()
    {
        currentStatus = owner.GetComponent<Player>().health / owner.GetComponent<Player>().maxhealth;
    }
};
