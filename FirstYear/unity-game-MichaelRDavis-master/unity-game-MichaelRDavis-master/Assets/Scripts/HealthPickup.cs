﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : Pickup
{
    public int restoredHeath = 25;          // Amount of health restored when picked up
    public AudioClip pickupSound;           // Sound to play on pickup

    // Called on begin play
    public void Start()
    {
        Destroy(gameObject, 3.5f);
    }

    // Called on collision
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Player player = collision.gameObject.GetComponent<Player>();
            if (player)
            {
                if (player.health < player.maxhealth)
                {
                    Debug.Log("Player can pickup health pack");
                    player.health += restoredHeath;
                    player.health = Mathf.Clamp(player.health, 0, player.maxhealth);
                    PlayPickupSound();

                    Destroy(gameObject);
                }
            }
        }
    }

    // Play sound on pickup 
    private void PlayPickupSound()
    {
        if(pickupSound != null)
        {
            AudioSource.PlayClipAtPoint(pickupSound, transform.position);
        }
    }

    // Called on collision overlap
    protected override void OnTriggerEnter2D(Collider2D collision)
    {

    }
}
