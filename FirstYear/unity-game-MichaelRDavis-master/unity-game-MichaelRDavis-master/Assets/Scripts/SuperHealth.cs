﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperHealth : Powerup
{
    private Player instigator;

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            instigator = collision.gameObject.GetComponent<Player>();
            if(instigator != null)
            {
                instigator.isPowerupActive = true;
            }
        }
    }
};
