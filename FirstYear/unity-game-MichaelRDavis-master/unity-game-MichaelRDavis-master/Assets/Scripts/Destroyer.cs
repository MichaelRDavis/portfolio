﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    public bool destroyOnAwake;
    public float destroyOnAwakeDelay;
    public bool findChild = false;
    public string strChildName;

    private void Awake()
    {
        if(destroyOnAwake)
        {
            if(findChild)
            {
                Destroy(transform.Find(strChildName).gameObject);
            }
            else
            {
                Destroy(gameObject, destroyOnAwakeDelay);
            }
        }
    }

    void DestroyChildGameObject()
    {
        if (transform.Find(strChildName).gameObject != null)
            Destroy(transform.Find(strChildName).gameObject);
    }

    void DisableChildGameObject()
    {
        if (transform.Find(strChildName).gameObject.activeSelf == true)
            transform.Find(strChildName).gameObject.SetActive(false);
    }

    void DestroyGameObject()
    {
        Destroy(gameObject);
    }
}
