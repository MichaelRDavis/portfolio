﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour, IDamageInterface
{
    public int health;
    public int maxHealth;

    public virtual void TakeDamage(int damage)
    {
        if(damage > 0)
        {
            health -= damage;
        }
    }

    public virtual void Die() { }
    public virtual void Hit() { }
}
