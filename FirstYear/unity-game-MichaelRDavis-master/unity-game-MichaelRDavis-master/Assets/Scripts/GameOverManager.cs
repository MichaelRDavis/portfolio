﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    public Player player;                   // Reference to the player game object
    public float gameOverDelay = 5.0f;      // Game Over time delay
    private float gameOverTimer;            // Internal counter to keep track of time

    // Called every frame
    private void Update()
    {
        if(player.health <= 0)
        {
            gameOverTimer += Time.deltaTime;
            if(gameOverTimer >= gameOverDelay)
            {
                SceneManager.LoadScene("GameOverScreen");
            }
        }
    }
};
